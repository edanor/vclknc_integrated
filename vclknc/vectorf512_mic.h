/****************************  vectorf512_mic.h   *******************************
* Author:        Przemyslaw Karpinski
* Date created:  2014-05-30
* Last modified: 2014-06-06
* Version:       0.01
* Project:       vector classes
* Description:
* Header file defining 512-bit floating point vector classes as interface
* to intrinsic functions in xeon phi - Knights Corner with KNCNI instruction set.
*
* Instructions:
* Use Gnu, Intel compiler. Compiles for KNCNI instruction set.
*
* The following vector classes are defined here:
* Vec16f    Vector of 16 single precision floating point numbers
* Vec8d     Vector of 8 double precision floating point numbers
*
* Each vector object is represented internally in the CPU as a 512-bit register.
* This header file defines operators and functions for these vectors.
*
* For example:
* Vec8d a(1., 2., 3., 4., 5., 6., 7., 8.), b(9., 10., 11., 12., 13., 14., 15.), c;
* c = a + b;     // now c contains (10., 12., 14., 16., 18., 20., 22., 24.)
*
* For detailed instructions, see VectorClass.pdf
*
* (c) Copyright 2014 - 2014 GNU General Public License http://www.gnu.org/licenses
*
* This piece of code was developed as part of ICE-DIP project at CERN.
* “ICE-DIP is a European Industrial Doctorate project funded by the European Community’s 
* 7th Framework programme Marie Curie Actions under grant PITN-GA-2012-316596”.
*
*****************************************************************************/

// check combination of header files
#if defined (VECTORF256_H)
#if    VECTORF256_H != 2
#error Two different versions of vectorf256.h included
#endif
#else
#define VECTORF256_H  2

#if INSTRSET != 10   // Knight'c Corner required
#error Please compile for the AVX instruction set or higher
#endif

#include "vectori512_mic.h"

// TODO: remove this after implementing all functions/functors
#include <assert.h>

// TODO: this should be moved to a separate file (shared content with vectorf128.h)
// Control word manipulaton
// ------------------------
// The MXCSR control word has the following bits:
//  0:    Invalid Operation Flag
//  1:    Denormal Flag
//  2:    Divide-by-Zero Flag
//  3:    Overflow Flag
//  4:    Underflow Flag
//  5:    Precision Flag
//  6:    Denormals Are Zeros
//  7:    Invalid Operation Mask
//  8:    Denormal Operation Mask
//  9:    Divide-by-Zero Mask
// 10:    Overflow Mask
// 11:    Underflow Mask
// 12:    Precision Mask
// 13-14: Rounding control
//        00: round to nearest or even
//        01: round down towards -infinity
//        10: round up   towards +infinity
//        11: round towards zero (truncate)
// 15: Flush to Zero

// Function get_control_word:
// Read the MXCSR control word
static inline uint32_t get_control_word() {
    return _mm_getcsr();
}

// Function set_control_word:
// Write the MXCSR control word
static inline void set_control_word(uint32_t w) {
    _mm_setcsr(w);
}

// Function no_denormals:
// Set "Denormals Are Zeros" and "Flush to Zero" mode to avoid the extremely
// time-consuming denormals in case of underflow
static inline void no_denormals() {
    uint32_t t1 = get_control_word();
    t1 |= (1 << 6) | (1 << 15);     // set bit 6 and 15 in MXCSR
    set_control_word(t1);
}

// Function reset_control_word:
// Set the MXCSR control word to the default value 0x1F80.
// This will mask floating point exceptions, set rounding mode to nearest (or even),
// and allow denormals.
static inline void reset_control_word() {
    set_control_word(0x1F80);
}

/*****************************************************************************
*
*          select functions
*
*****************************************************************************/
// Select between two __m512 sources, element by element. Used in various functions
// and operators. Corresponds to this pseudocode:
// for (int i = 0; i < 16; i++) result[i] = s[i] ? a[i] : b[i];
// Each element in s must be either 0 (false) or 0xFFFFFFFF (true).
static inline __m512 selectf (__mmask16 const & s, __m512 const & a, __m512 const & b) {
    return _mm512_mask_blend_ps(s, b, a); // The mask vector and operands are switched in comparison to _mm256 version
}

// Same, with two __m512d sources.
// and operators. Corresponds to this pseudocode:
// for (int i = 0; i < 8; i++) result[i] = s[i] ? a[i] : b[i];
// Each element in s must be either 0 (false) or 0xFFFFFFFFFFFFFFFF (true). No other
// values are allowed.
static inline __m512d selectd (__mmask16 const & s, __m512d const & a, __m512d const & b) {
    return _mm512_mask_blend_pd(s, b, a); // The mask vector and operands are switched in comparison to _mm512 version
}

/*****************************************************************************
*
*          Generate compile-time constant vector
*
*****************************************************************************/

// Generate a constant vector of 16 integers stored in memory,
// load as _m512
template <int i0, int i1, int i2,  int i3,  int i4,  int i5,  int i6,  int i7,
          int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15>
static inline __m512 constant16f() {
    __declspec(align(64)) static const union {
        int     i[16];
        __m512   zmm;
    } u = {{i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15}};
    return u.zmm;
}

// Generate a constant vector of 8 integers stored in memory,
// load as _m512
template <uint64_t i0, uint64_t i1, uint64_t i2,  uint64_t i3,  uint64_t i4,  uint64_t i5,  uint64_t i6,  uint64_t i7>
static inline __m512d constant8d() {
    __declspec(align(64)) static const union {
        uint64_t    i[8];
        __m512d      zmm;
    } u = {{i0,i1,i2,i3,i4,i5,i6,i7}};
    return u.zmm;
}

/*****************************************************************************
*
*          Vec16f: Vector of 16 single precision floating point values
*
*****************************************************************************/

class Vec16f {
protected:
    __m512 zmm; // Float vector
public:
    // Default constructor:
    Vec16f() {
        zmm = _mm512_set1_ps(0.0);
    }
    // Constructor to broadcast the same value into all elements:
    Vec16f(float f) {
        zmm = _mm512_set1_ps(f);
    }
    // Constructor to build from all elements:
    Vec16f(float f0, float f1, float f2,  float f3,  float f4,  float f5,  float f6,  float f7,
          float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15) {
        zmm = _mm512_setr_ps(f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15);
    }
    // Constructor to convert from type __m512 used in intrinsics:
    Vec16f(__m512 const & x) {
        zmm = x;
    }
    // Assignment operator to convert from type __m512 used in intrinsics:
    Vec16f & operator = (__m512 const & x) {
        zmm = x;
        return *this;
    }
    // Type cast operator to convert to __m512 used in intrinsics
    operator __m512() const {
        return zmm;
    }
    // Member function to load from array (unaligned)
    Vec16f & load(float const * p) {
        if((((uint64_t)p) & 0x3F) != 0)assert(0); // TODO: this should be removed
        zmm = _mm512_load_ps(p);
        return *this;
    }
    // Member function to load from array, aligned by 32
    // You may use load_a instead of load if you are certain that p points to an address
    // divisible by 32.
    Vec16f & load_a(float const * p) {
        zmm = _mm512_load_ps(p);
        return *this;
    }
    // Member function to store into array (unaligned)
    void store(float * p) const {
        // TODO: some sort of allignment check should be done here
        if((((uint64_t)p) & 0x3F) != 0)assert(0); // TODO: this should be removed
        _mm512_store_ps(p, zmm);
    }
    // Member function to store into array, aligned by 32
    // You may use store_a instead of store if you are certain that p points to an address
    // divisible by 32.
    void store_a(float * p) const {
        _mm512_store_ps(p, zmm);
    }
    // Partial load. Load n elements and set the rest to 0
    Vec16f & load_partial(int n, float const * p) {
        zmm = _mm512_mask_load_ps(_mm512_setzero_ps(),  _mm512_int2mask((1 << n)-1), p);
        return *this;
    }

    // Partial store. Store n elements
    void store_partial(int n, float * p) const {
        _mm512_mask_store_ps(p, _mm512_int2mask((1 << n) - 1), zmm);
    }

    // cut off vector to n elements. The last 16-n elements are set to zero
    Vec16f & cutoff(int n) {
        zmm = _mm512_mask_mov_ps(_mm512_set1_ps(0.0),  _mm512_int2mask((1 << n) - 1), zmm);
        return *this;
    }

    // Member function to change a single element in vector
    // Note: This function is inefficient. Use load function if changing more than one element
    Vec16f const & insert(uint32_t index, float value) {
                zmm = _mm512_mask_mov_ps(zmm,
                                 _mm512_int2mask(1 << index),
                                 _mm512_set1_ps(value));
        return *this;
    }
    // Member function extract a single element from vector
    float extract(uint32_t index) const {
        __declspec(align(64)) float x[16];
        store_a(x);

        unsigned int uint = *((unsigned int  *)&x[index & 0xF]);
        // printf("extract[%d]: %f %08X\n", index & 0xF, x[index & 0xF], uint);
        return x[index & 0xF];
    }

    // Extract a single element. Use store function if extracting more than one element.
    // Operator [] can only read an element, not write.
    float operator [] (uint32_t index) const {
        return extract(index);
    }
};

/*****************************************************************************
*
*          Operators for Vec16f
*
*****************************************************************************/

// vector operator + : add element by element
static inline Vec16f operator + (Vec16f const & a, Vec16f const & b) {
    return _mm512_add_ps(a,b);
}

// vector operator + : add vector and scalar
static inline Vec16f operator + (Vec16f const & a, float b) {
    return a + Vec16f(b);
}
static inline Vec16f operator + (float a, Vec16f const & b) {
    return Vec16f(a) + b;
}

// vector operator += : add
static inline Vec16f & operator += (Vec16f & a, Vec16f const & b) {
    a = a + b;
    return a;
}

// postfix operator ++
static inline Vec16f operator ++ (Vec16f & a, int) {
    Vec16f a0 = a;
    a = a + 1.0f;
    return a0;
}

// prefix operator ++
static inline Vec16f & operator ++ (Vec16f & a) {
    a = a + 1.0f;
    return a;
}

// vector operator - : subtract element by element
static inline Vec16f operator - (Vec16f const & a, Vec16f const & b) {
    return _mm512_sub_ps(a,b);
}

// vector operator - : subtract vector and scalar
static inline Vec16f operator - (Vec16f const & a, float b) {
    return a - Vec16f(b);
}
static inline Vec16f operator - (float a, Vec16f const & b) {
    return Vec16f(a) - b;
}

// vector operator - : unary minus
// Change sign bit, even for 0, INF and NAN
static inline Vec16f operator - (Vec16f const & a) {
    return _mm512_castsi512_ps(_mm512_xor_epi32(_mm512_castps_si512(a), _mm512_set1_epi32(0x80000000)));
}

// vector operator -= : subtract
static inline Vec16f & operator -= (Vec16f & a, Vec16f const & b) {
    a = a - b;
    return a;
}

// postfix operator --
static inline Vec16f operator -- (Vec16f & a, int) {
    Vec16f a0 = a;
    a = a - 1.0f;
    return a0;
}

// prefix operator --
static inline Vec16f & operator -- (Vec16f & a) {
    a = a - 1.0f;
    return a;
}

// vector operator * : multiply element by element
static inline Vec16f operator * (Vec16f const & a, Vec16f const & b) {
    return _mm512_mul_ps(a,b);
}

// vector operator * : multiply vector and scalar
static inline Vec16f operator * (Vec16f const & a, float b) {
    return a * Vec16f(b);
}

static inline Vec16f operator * (float a, Vec16f const & b) {
    return Vec16f(a) * b;
}

// vector operator *= : multiply
static inline Vec16f & operator *= (Vec16f & a, Vec16f const & b) {
    a = a * b;
    return a;
}

// vector operator / : divide all elements by same integer
static inline Vec16f operator / (Vec16f const & a, Vec16f const & b) {
    return _mm512_div_ps(a,b);
}

// vector operator / : divide vector and scalar
static inline Vec16f operator / (Vec16f const & a, float b) {
    return a / Vec16f(b);
}

static inline Vec16f operator / (float a, Vec16f const & b) {
    return Vec16f(a) / b;
}

// vector operator /= : divide
static inline Vec16f & operator /= (Vec16f & a, Vec16f const & b) {
    a = a / b;
    return a;
}

// vector operator == : returns true for elements for which a == b
static inline Vec16b operator == (Vec16f const & a, Vec16f const & b) {
    return _mm512_cmp_ps_mask(a, b, _MM_CMPINT_EQ);
}

// vector operator != : returns true for elements for which a != b
static inline Vec16b operator != (Vec16f const & a, Vec16f const & b) {
    return _mm512_cmp_ps_mask(a, b, _MM_CMPINT_NE);
}

// vector operator < : returns true for elements for which a < b
static inline Vec16b operator < (Vec16f const & a, Vec16f const & b) {
    return _mm512_cmp_ps_mask(a, b, _MM_CMPINT_LT);
}

// vector operator <= : returns true for elements for which a <= b
static inline Vec16b operator <= (Vec16f const & a, Vec16f const & b) {
    return _mm512_cmp_ps_mask(a, b, _MM_CMPINT_LE);
}

// vector operator > : returns true for elements for which a > b
static inline Vec16b operator > (Vec16f const & a, Vec16f const & b) {
    return _mm512_cmp_ps_mask(a, b, _MM_CMPINT_GT);
}

// vector operator >= : returns true for elements for which a >= b
static inline Vec16b operator >= (Vec16f const & a, Vec16f const & b) {
    return _mm512_cmp_ps_mask(a, b, _MM_CMPINT_GE);
}

// Bitwise logical operators

// vector operator & : bitwise and
static inline Vec16f operator & (Vec16f const & a, Vec16f const & b) {
    return _mm512_castsi512_ps(_mm512_and_epi32(_mm512_castps_si512(a), _mm512_castps_si512(b)));
}

// vector operator &= : bitwise and
static inline Vec16f & operator &= (Vec16f & a, Vec16f const & b) {
    a = a & b;
    return a;
}

// vector operator & : bitwise and of Vec8f and Vec8fb
static inline Vec16f operator & (Vec16f const & a, Vec16b const & b) {
    return _mm512_mask_mov_ps(_mm512_setzero_ps(), b, a);
}

static inline Vec16f operator & (Vec16b const & a, Vec16f const & b) {
    return _mm512_mask_mov_ps(_mm512_setzero_ps(), a, b);
}

// vector operator | : bitwise or
static inline Vec16f operator | (Vec16f const & a, Vec16f const & b) {
    return _mm512_castsi512_ps(_mm512_or_epi32(_mm512_castps_si512(a), _mm512_castps_si512(b)));
}

// vector operator |= : bitwise or
static inline Vec16f & operator |= (Vec16f & a, Vec16f const & b) {
    a = a | b;
    return a;
}

// vector operator ^ : bitwise xor
static inline Vec16f operator ^ (Vec16f const & a, Vec16f const & b) {
    return Vec16f(_mm512_castsi512_ps(_mm512_xor_epi32(_mm512_castps_si512(a), _mm512_castps_si512(b))));
}

// vector operator ^= : bitwise xor
static inline Vec16f & operator ^= (Vec16f & a, Vec16f const & b) {
    a = a ^ b;
    return a;
}

// vector operator ! : logical not. Returns Boolean vector
static inline Vec16b operator ! (Vec16f const & a) {
    return a == Vec16f(0.0f);
}

/*****************************************************************************
*
*          Functions for Vec16f
*
*****************************************************************************/

// Select between two operands. Corresponds to this pseudocode:
// for (int i = 0; i < 16; i++) result[i] = s[i] ? a[i] : b[i];
static inline Vec16f select (Vec16b const & s, Vec16f const & a, Vec16f const & b) {
    return _mm512_mask_mov_ps(b, s, a);
}

// Conditional add: For all vector elements i: result[i] = f[i] ? (a[i] + b[i]) : a[i]
static inline Vec16f if_add (Vec16b const & f, Vec16f const & a, Vec16f const & b) {
    return a + (f & b);
}

// Conditional multiply: For all vector elements i: result[i] = f[i] ? (a[i] * b[i]) : a[i]
static inline Vec16f if_mul (Vec16b const & f, Vec16f const & a, Vec16f const & b) {
    return a * select(f, b, 1.f);
}

// TODO: multiply and add; multiply and add 233; multiply add and negate; multiply and subtract; multiply negate and subtract; multiply and round; scale; scale and round

// General arithmetic functions, etc.

// Horizontal add: Calculates the sum of all vector elements.
static inline float horizontal_add (Vec16f const & a) {
    return _mm512_reduce_add_ps(a);
}

// function max: a > b ? a : b
static inline Vec16f max(Vec16f const & a, Vec16f const & b) {
    return _mm512_max_ps(a,b);
}

// function min: a < b ? a : b
static inline Vec16f min(Vec16f const & a, Vec16f const & b) {
    return _mm512_min_ps(a,b);
}

// function abs: absolute value
// Removes sign bit, even for -0.0f, -INF and -NAN
static inline Vec16f abs(Vec16f const & a) {
    __m512i mask = _mm512_set1_epi32(0x7FFFFFFF);
    __m512i ai = _mm512_castps_si512(a);
    __m512i t1 = _mm512_and_epi32(ai, mask);
    return _mm512_castsi512_ps(t1);
}

// function sqrt: square root
static inline Vec16f sqrt(Vec16f const & a) {
    return _mm512_sqrt_ps(a);
}

// function square: a * a
static inline Vec16f square(Vec16f const & a) {
    return a * a;
}

// Raise floating point numbers to integer power n
static inline Vec16f pow(Vec16f const & a, int n) {
    Vec16f x = a;                       // a^(2^i)
    Vec16f y(1.0f);                     // accumulator
    if (n >= 0) {                      // make sure n is not negative
        while (true) {                 // loop for each bit in n
            if (n & 1) y *= x;         // multiply if bit = 1
            n >>= 1;                   // get next bit of n
            if (n == 0) return y;      // finished
            x *= x;                    // x = a^2, a^4, a^8, etc.
        }
    }
    else {                             // n < 0
        return Vec16f(1.0f)/pow(x,-n); // reciprocal
    }
}
// prevent implicit conversion of exponent to int
static Vec16f pow(Vec16f const & x, float y);

// Raise floating point numbers to integer power n, where n is a compile-time constant
template <int n>
static inline Vec16f pow_n(Vec16f const & a) {
    if (n < 0)    return Vec16f(1.0f) / pow_n<-n>(a);
    if (n == 0)   return Vec16f(1.0f);
    if (n >= 256) return pow(a, n);
    Vec16f x = a;                       // a^(2^i)
    Vec16f y;                           // accumulator
    const int lowest = n - (n & (n-1));// lowest set bit in n
    if (n & 1) y = x;
    if (n < 2) return y;
    x = x*x;                           // x^2
    if (n & 2) {
        if (lowest == 2) y = x; else y *= x;
    }
    if (n < 4) return y;
    x = x*x;                           // x^4
    if (n & 4) {
        if (lowest == 4) y = x; else y *= x;
    }
    if (n < 8) return y;
    x = x*x;                           // x^8
    if (n & 8) {
        if (lowest == 8) y = x; else y *= x;
    }
    if (n < 16) return y;
    x = x*x;                           // x^16
    if (n & 16) {
        if (lowest == 16) y = x; else y *= x;
    }
    if (n < 32) return y;
    x = x*x;                           // x^32
    if (n & 32) {
        if (lowest == 32) y = x; else y *= x;
    }
    if (n < 64) return y;
    x = x*x;                           // x^64
    if (n & 64) {
        if (lowest == 64) y = x; else y *= x;
    }
    if (n < 128) return y;
    x = x*x;                           // x^128
    if (n & 128) {
        if (lowest == 128) y = x; else y *= x;
    }
    return y;
}

template <int n>
static inline Vec16f pow(Vec16f const & a, Const_int_t<n>) {
    return pow_n<n>(a);
}

// function round: round to nearest integer (even). (result as float vector)
static inline Vec16f round(Vec16f const & a) {
    return _mm512_round_ps(a, _MM_FROUND_TO_NEAREST_INT, _MM_EXPADJ_NONE);
}

// function truncate: round towards zero. (result as float vector)
static inline Vec16f truncate(Vec16f const & a) {
    return _mm512_round_ps(a, _MM_FROUND_TO_ZERO, _MM_EXPADJ_NONE);
}

// function floor: round towards minus infinity. (result as float vector)
static inline Vec16f floor(Vec16f const & a) {
    return _mm512_round_ps(a, _MM_FROUND_TO_NEG_INF, _MM_EXPADJ_NONE);
}

// function ceil: round towards plus infinity. (result as float vector)
static inline Vec16f ceil(Vec16f const & a) {
    return _mm512_round_ps(a, _MM_FROUND_TO_POS_INF, _MM_EXPADJ_NONE);
}

static inline Vec16i round_to_int(Vec16f const & a) {
    return _mm512_cvtfxpnt_round_adjustps_epi32(a, _MM_ROUND_MODE_NEAREST, _MM_EXPADJ_NONE);
}

// function truncate_to_int: round towards zero. (result as integer vector)
static inline Vec16i truncate_to_int(Vec16f const & a) {
    return _mm512_cvtfxpnt_round_adjustps_epi32(a, _MM_ROUND_MODE_TOWARD_ZERO, _MM_EXPADJ_NONE);
}

// function to_float: convert integer vector to float vector
static inline Vec16f to_float(Vec16i const & a) {
    return _mm512_cvtfxpnt_round_adjustepi32_ps(a, _MM_FROUND_TO_NEAREST_INT, _MM_EXPADJ_NONE);
}

// Approximate math functions

// approximate reciprocal (Faster than 1.f / a. relative accuracy better than 2^-11)
static inline Vec16f approx_recipr(Vec16f const & a) {
    return _mm512_rcp23_ps(a);
}

// approximate reciprocal squareroot (Faster than 1.f / sqrt(a). Relative accuracy better than 2^-11)
static inline Vec16f approx_rsqrt(Vec16f const & a) {
    return _mm512_rsqrt23_ps(a);
}

// Math functions using fast bit manipulation

// Extract the exponent as an integer
// exponent(a) = floor(log2(abs(a)));
// exponent(1.0f) = 0, exponent(0.0f) = -127, exponent(INF) = +128, exponent(NAN) = +128
static inline Vec16i exponent(Vec16f const & a) {
    return _mm512_cvtfxpnt_round_adjustps_epi32(_mm512_getexp_ps(a), _MM_FROUND_TO_NEAREST_INT, _MM_EXPADJ_NONE);
}

// Extract the fraction part of a floating point number
// a = 2^exponent(a) * fraction(a), except for a = 0
// fraction(1.0f) = 1.0f, fraction(5.0f) = 1.25f
static inline Vec16f fraction(Vec16f const & a) {
    return _mm512_getmant_ps(a, _MM_MANT_NORM_1_2, _MM_MANT_SIGN_zero); // getmant gives more flexibility in terms of normalization
    // TODO: remove this commented code when solving 0.0 issue (see vectorf512_mic.h: static inline Vec16f fraction(Vec16f const & a) )
    /*Vec16ui t1 = _mm512_castps_si512(a);   // reinterpret as 32-bit integer
    Vec16ui t2 = (t1 & 0x007FFFFF) | 0x3F800000; // set exponent to 0 + bias
    return _mm512_castsi512_ps(t2);*/
}

// Fast calculation of pow(2,n) with n integer
// n  =    0 gives 1.0f
// n >=  128 gives +INF
// n <= -127 gives 0.0f
// This function will never produce denormals, and never raise exceptions
static inline Vec16f exp2(Vec16i const & n) {
    // return _mm512_exp223_ps(n);      // NOTE: this gives only an approximate result
    Vec16i t1 = max(n,  -0x7F);         // limit to allowed range
    Vec16i t2 = min(t1,  0x80);
    Vec16i t3 = t2 + 0x7F;              // add bias
    Vec16i t4 = t3 << 23;               // put exponent into position 23
    return _mm512_castsi512_ps(t4);     // reinterpret as float
}

// Categorization functions

// Function sign_bit: gives true for elements that have the sign bit set
// even for -0.0f, -INF and -NAN
// Note that sign_bit(Vec8f(-0.0f)) gives true, while Vec8f(-0.0f) < Vec8f(0.0f) gives false
// (the underscore in the name avoids a conflict with a macro in Intel's mathimf.h)
static inline Vec16b sign_bit(Vec16f const & a) {
    Vec16i t1 = _mm512_castps_si512(a);
    Vec16i t2 = t1 >> 31;
    return _mm512_cmp_epi32_mask(t2, _mm512_set1_epi32(0xFFFFFFFF), _MM_CMPINT_EQ);
}

// Function sign_combine: changes the sign of a when b has the sign bit set
// same as select(sign_bit(b), -a, a)
static inline Vec16f sign_combine(Vec16f const & a, Vec16f const & b) {
    Vec16f signmask = _mm512_castsi512_ps(_mm512_set1_epi32(0x80000000));
    return a ^ (b & signmask);
}

// Function is_finite: gives true for elements that are normal, denormal or zero,
// false for INF and NAN
// (the underscore in the name avoids a conflict with a macro in Intel's mathimf.h)
static inline Vec16b is_finite(Vec16f const & a) {
    Vec16i v1 = _mm512_castps_si512(a); // reinterpret as 32-bit integer
    Vec16i v2 = v1 << 1;                // shift out sign bit
    Vec16b v3 = Vec16i(v2 & 0xFF000000) != 0xFF000000; // exponent field is not all 1s
    return Vec16b(v3);
}

// Function is_inf: gives true for elements that are +INF or -INF
// false for finite numbers and NAN
// (the underscore in the name avoids a conflict with a macro in Intel's mathimf.h)
static inline Vec16b is_inf(Vec16f const & a) {
    Vec16i t1 = _mm512_castps_si512(a);  // reinterpret as 32-bit integer
    Vec16i t2 = t1 << 1;                 // shift out sign bit
    return Vec16b(t2 == 0xFF000000);    // exponent is all 1s, fraction is 0*/
}

// Function is_nan: gives true for elements that are +NAN or -NAN
// false for finite numbers and +/-INF
// (the underscore in the name avoids a conflict with a macro in Intel's mathimf.h)
static inline Vec16b is_nan(Vec16f const & a) {
    Vec16i t1 = _mm512_castps_si512(a); // reinterpret as 32-bit integer
    Vec16i t2 = t1 << 1;                // shift out sign bit
    Vec16i t3 = 0xFF000000;             // exponent mask
    Vec16i t4 = t2 & t3;                // exponent
    Vec16i t5 = andnot(t2, t3);         // fraction
    return Vec16b(t4 == t3 && t5 != 0);// exponent = all 1s and fraction != 0
}

// Function is_subnormal: gives true for elements that are denormal (subnormal)
// false for finite numbers, zero, NAN and INF
static inline Vec16b is_subnormal(Vec16f const & a) {
    Vec16i v1 = _mm512_castps_si512(a);
    Vec16i v2 = v1 << 1;
    Vec16i v3 = 0xFF000000;
    Vec16i v4 = v2 & v3;
    Vec16i v5 = _mm512_and_epi32(~v3, v2);
    return Vec16b(v4 == 0 && v5 != 0);
}

static inline Vec16b is_zero_or_subnormal(Vec16f const & a)
{
    /*Vec16i t1 = _mm512_castps_si512(a); // reinterpret as 64-bit integer
    Vec16i t2 = t1 << 1;                // shift out sign bit
    Vec16i t3 = 0xFF000000;             // exponent mask
    Vec16i t4 = t2 & t3;                // exponent
    Vec16i t5 = andnot(t2, t3);        // fraction
    Vec16b t6 = t1 != 0x00000000;       // is_zero
    return Vec16b(t6 || (t4 == 0 && t5 != 0));  // exponent = 0 and fraction != 0 */

    Vec16i t = _mm512_castps_si512(a);    // reinterpret as 32-bit integer
    t &= 0x7FF000ll;          // isolate exponent
    return t == 0;                      // exponent = 0
}

// Function infinite16f: returns a vector where all elements are +INF
static inline Vec16f infinite16f() {
    return _mm512_castsi512_ps(_mm512_set1_epi32(0x7F800000));
}

// Function nan16f: returns a vector where all elements are +NAN (quiet)
static inline Vec16f nan16f(int n = 0x10) {
    return _mm512_castsi512_ps(_mm512_set1_epi32(0x7FC00000 + n));
}

// change signs on vectors Vec16f
// Each index i0 - i7 is 1 for changing sign on the corresponding element, 0 for no change
template <int i0, int i1, int i2,  int i3,  int i4,  int i5,  int i6,  int i7,
          int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15>
static inline Vec16f change_sign(Vec16f const & a) {
    if ((i0 | i1 | i2 | i3 | i4 | i5 | i6 | i7 | i8 | i9 | i10 | i11 | i12 | i13 | i14 | i15) == 0) return a;
    __mmask16 mask =    i0      | i1 << 1 | i2  << 2  | i3  << 3  | i4  << 4  | i5  << 5  | i6  << 6  | i7  << 7 |
                        i8 << 8 | i9 << 9 | i10 << 10 | i11 << 11 | i12 << 12 | i13 << 13 | i14 << 14 | i15 << 15;
    __m512i t0 = _mm512_castps_si512(a);
    __m512i t1 = _mm512_set1_epi32(0x80000000);
    return _mm512_castsi512_ps(_mm512_mask_xor_epi32(t0, mask, t0, t1));
}

/*****************************************************************************
*
*          Vec8d: Vector of 8 double precision floating point values
*
*****************************************************************************/

class Vec8d {
protected:
    __m512d zmm; // double vector
public:
    // Default constructor:
    Vec8d() {
        zmm = _mm512_setzero_pd();
    }
    // Constructor to broadcast the same value into all elements:
    Vec8d(double d) {
        zmm = _mm512_set1_pd(d);
    }
    // Constructor to build from all elements:
    Vec8d(double d0, double d1, double d2, double d3, double d4, double d5, double d6, double d7) {
        zmm = _mm512_setr_pd(d0, d1, d2, d3, d4, d5, d6, d7);
    }
    // Constructor to convert from type __m512d used in intrinsics:
    Vec8d(__m512d const & x) {
        zmm = x;
    }
    // Assignment operator to convert from type __m512d used in intrinsics:
    Vec8d & operator = (__m512d const & x) {
        zmm = x;
        return *this;
    }

    // Type cast operator to convert to __m512d used in intrinsics
    operator __m512d() const {
        return zmm;
    }

    // Type cast operator to convert to __m512 used in intrinsics
    operator Vec8uq() const {
        // TODO: find proper intrinsic...
        //return _mm512_cvt_roundpd_epi64(zmm);
       // assert(0);
        return Vec8uq(0);
    }

    // Member function to load from array (unaligned)
    Vec8d & load(double const * p) {
        zmm = _mm512_load_pd(p);
        return *this;
    }
    // Member function to load from array, aligned by 64
    // You may use load_a instead of load if you are certain that p points to an address
    // divisible by 64
    Vec8d & load_a(double const * p) {
        zmm =  _mm512_load_pd(p);
    }
    // Member function to store into array (unaligned)
    void store(double * p) const {
        // TODO: some sort of allignment check or partial load for two cachelines should be done
        _mm512_store_pd(p, zmm);
    }

    // Member function to store into array, aligned by 64
    // You may use store_a instead of store if you are certain that p points to an address
    // divisible by 64
    void store_a(double * p) const {
        _mm512_store_pd(p, zmm);
    }

    // Partial load. Load n elements and set the rest to 0
    Vec8d & load_partial(int n, double const * p) {
        __m512d t0 = _mm512_set1_pd(0);
        zmm = _mm512_mask_load_pd(t0,
                                  (1 << n)-1,
                                  p);
    }

    // Partial store. Store n elements
    void store_partial(int n, double * p) const {
        _mm512_mask_store_pd(p,
                             (1 << n) - 1,
                             zmm);
    }

    // cut off vector to n elements. The last 8-n elements are set to zero
    Vec8d & cutoff(int n) {
        zmm = _mm512_mask_mov_pd(_mm512_setzero_pd(), _mm512_int2mask((1 << n) - 1), zmm);
        return *this;
    }

    // Member function to change a single element in vector
    // Note: This function is inefficient. Use load function if changing more than one element
    Vec8d const & insert(uint32_t index, double value) {
        zmm = _mm512_mask_mov_pd(zmm,
                                 1 << index,
                                 _mm512_set1_pd(value));
        return *this;
    }

    // Member function extract a single element from vector
    double extract(uint32_t index) const {
        __declspec(align(64)) double x[8]; // double should be already naturally alligned, placing this clause for readibility
        store(x);
        return x[index & 7];
    }

    // Extract a single element. Use store function if extracting more than one element.
    // Operator [] can only read an element, not write.
    double operator [] (uint32_t index) const {
        return extract(index);
    }
};

/*****************************************************************************
*
*          Operators for Vec8d
*
*****************************************************************************/

// vector operator + : add element by element
static inline Vec8d operator + (Vec8d const & a, Vec8d const & b) {
    return _mm512_add_pd(a,b);
}

// vector operator + : add vector and scalar
static inline Vec8d operator + (Vec8d const & a, double b) {
    return a + Vec8d(b);
}
static inline Vec8d operator + (double a, Vec8d const & b) {
    return Vec8d(a) + b;
}

// vector operator += : add
static inline Vec8d & operator += (Vec8d & a, Vec8d const & b) {
    a = a + b;
    return a;
}

// postfix operator ++
static inline Vec8d operator ++ (Vec8d & a, int) {
    Vec8d a0 = a;
    a = a + 1.0;
    return a0;
}

// prefix operator ++
static inline Vec8d & operator ++ (Vec8d & a) {
    a = a + 1.0;
    return a;
}

// vector operator - : subtract element by element
static inline Vec8d operator - (Vec8d const & a, Vec8d const & b) {
    return _mm512_sub_pd(a,b);
}

// vector operator - : subtract vector and scalar
static inline Vec8d operator - (Vec8d const & a, double b) {
    return a - Vec8d(b);
}
static inline Vec8d operator - (double a, Vec8d const & b) {
    return Vec8d(a) - b;
}

// vector operator - : unary minus
// Change sign bit, even for 0, INF and NAN
static inline Vec8d operator - (Vec8d const & a) {
    return _mm512_castsi512_pd(_mm512_xor_epi64(_mm512_castpd_si512(a), _mm512_set1_epi64(0x8000000000000000)));
}

// vector operator -= : subtract
static inline Vec8d & operator -= (Vec8d & a, Vec8d const & b) {
    a = a - b;
    return a;
}

// postfix operator --
static inline Vec8d operator -- (Vec8d & a, int) {
    Vec8d a0 = a;
    a = a - 1.0;
    return a0;
}

// prefix operator --
static inline Vec8d & operator -- (Vec8d & a) {
    a = a - 1.0;
    return a;
}

// vector operator * : multiply element by element
static inline Vec8d operator * (Vec8d const & a, Vec8d const & b) {
    return _mm512_mul_pd(a,b);
}

// vector operator * : multiply vector and scalar
static inline Vec8d operator * (Vec8d const & a, double b) {
    return a * Vec8d(b);
}
static inline Vec8d operator * (double a, Vec8d const & b) {
    return Vec8d(a) * b;
}

// vector operator *= : multiply
static inline Vec8d & operator *= (Vec8d & a, Vec8d const & b) {
    a = a * b;
    return a;
}

// vector operator / : divide all elements by same integer
static inline Vec8d operator / (Vec8d const & a, Vec8d const & b) {
    return _mm512_div_pd(a, b);
}

// vector operator / : divide vector and scalar
static inline Vec8d operator / (Vec8d const & a, double b) {
    return a / Vec8d(b);
}
static inline Vec8d operator / (double a, Vec8d const & b) {
    return Vec8d(a) / b;
}

// vector operator /= : divide
static inline Vec8d & operator /= (Vec8d & a, Vec8d const & b) {
    a = a / b;
    return a;
}

// vector operator == : returns true for elements for which a == b
static inline Vec8b operator == (Vec8d const & a, Vec8d const & b) {
    return _mm512_cmp_pd_mask(a, b, _MM_CMPINT_EQ);
}

// vector operator != : returns true for elements for which a != b
static inline Vec8b operator != (Vec8d const & a, Vec8d const & b) {
    return _mm512_cmp_pd_mask(a, b, _MM_CMPINT_NE);
}

// vector operator < : returns true for elements for which a < b
static inline Vec8b operator < (Vec8d const & a, Vec8d const & b) {
    return _mm512_cmp_pd_mask(a, b, _MM_CMPINT_LT);
}

// vector operator <= : returns true for elements for which a <= b
static inline Vec8b operator <= (Vec8d const & a, Vec8d const & b) {
    return _mm512_cmp_pd_mask(a, b, _MM_CMPINT_LE);
}

// vector operator > : returns true for elements for which a > b
static inline Vec8b operator > (Vec8d const & a, Vec8d const & b) {
    return _mm512_cmp_pd_mask(a, b, _MM_CMPINT_GT);
}

// vector operator >= : returns true for elements for which a >= b
static inline Vec8b operator >= (Vec8d const & a, Vec8d const & b) {
    return _mm512_cmp_pd_mask(a, b, _MM_CMPINT_GE);
}

// Bitwise logical operators

// vector operator & : bitwise and
static inline Vec8d operator & (Vec8d const & a, Vec8d const & b) {
    return _mm512_castsi512_pd(_mm512_and_epi64(_mm512_castpd_si512(a), _mm512_castpd_si512(b)));
}

// vector operator &= : bitwise and
static inline Vec8d & operator &= (Vec8d & a, Vec8d const & b) {
    a = a & b;
    return a;
}

// vector operator & : bitwise and of Vec8d and Vec8b
static inline Vec8d operator & (Vec8d const & a, Vec8b const & b) {
    return _mm512_mask_mov_pd(_mm512_setzero_pd(), b, a);
}

static inline Vec8d operator & (Vec8b const & a, Vec8d const & b) {
    return _mm512_mask_mov_pd(_mm512_setzero_pd(), a, b);
}

// vector operator | : bitwise or
static inline Vec8d operator | (Vec8d const & a, Vec8d const & b) {
    return _mm512_castsi512_pd(_mm512_or_epi64(_mm512_castpd_si512(a), _mm512_castpd_si512(b)));
}

// vector operator |= : bitwise or
static inline Vec8d & operator |= (Vec8d & a, Vec8d const & b) {
    a = a | b;
    return a;
}

// vector operator ^ : bitwise xor
static inline Vec8d operator ^ (Vec8d const & a, Vec8d const & b) {
    return _mm512_castsi512_pd(_mm512_xor_epi64(_mm512_castpd_si512(a), _mm512_castpd_si512(b)));
}

// vector operator ^= : bitwise xor
static inline Vec8d & operator ^= (Vec8d & a, Vec8d const & b) {
    a = a ^ b;
    return a;
}

// vector operator ! : logical not. Returns Boolean vector
static inline Vec8b operator ! (Vec8d const & a) {
    return a == Vec8d(0.0);
}

/*****************************************************************************
*
*          Functions for Vec8d
*
*****************************************************************************/

// Select between two operands. Corresponds to this pseudocode:
// for (int i = 0; i < 8; i++) result[i] = s[i] ? a[i] : b[i];
// Each byte in s must be either 0 (false) or 0xFFFFFFFFFFFFFFFF (true).
// No other values are allowed.
static inline Vec8d select (Vec8b const & s, Vec8d const & a, Vec8d const & b) {
    return _mm512_mask_mov_pd(b, s, a);
}

// Conditional add: For all vector elements i: result[i] = f[i] ? (a[i] + b[i]) : a[i]
static inline Vec8d if_add (Vec8b const & f, Vec8d const & a, Vec8d const & b) {
    return a + (f & b);
}

// Conditional multiply: For all vector elements i: result[i] = f[i] ? (a[i] * b[i]) : a[i]
static inline Vec8d if_mul (Vec8b const & f, Vec8d const & a, Vec8d const & b) {
    return a * select(f, b, 1.);
}

// General arithmetic functions, etc.

// Horizontal add: Calculates the sum of all vector elements.
static inline double horizontal_add (Vec8d const & a) {
    return _mm512_reduce_add_pd(a);
}

// function max: a > b ? a : b
static inline Vec8d max(Vec8d const & a, Vec8d const & b) {
    return _mm512_max_pd(a,b);
}

// function min: a < b ? a : b
static inline Vec8d min(Vec8d const & a, Vec8d const & b) {
    return _mm512_min_pd(a,b);
}

// function abs: absolute value
// Removes sign bit, even for -0.0f, -INF and -NAN
static inline Vec8d abs(Vec8d const & a) {
    __m512i mask = constant16i<0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF,
                               0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF>();
    //__m512i mask = _mm512_set1_epi64(0x7FFFFFFFFFFFFFFF); // <-- this is another option though i'm not sure if it is faster...
    __m512i ai = _mm512_castpd_si512(a);
    return _mm512_castsi512_pd(_mm512_and_epi64(ai, mask));
}

// function sqrt: square root
static inline Vec8d sqrt(Vec8d const & a) {
    return _mm512_sqrt_pd(a);
}

// function square: a * a
static inline Vec8d square(Vec8d const & a) {
    return a * a;
}

// pow(Vec4d, int):
// Raise floating point numbers to integer power n
static inline Vec8d pow(Vec8d const & a, int n) {
    Vec8d x = a;                       // a^(2^i)
    Vec8d y(1.0);                      // accumulator
    if (n >= 0) {                      // make sure n is not negative
        while (true) {                 // loop for each bit in n
            if (n & 1) y *= x;         // multiply if bit = 1
            n >>= 1;                   // get next bit of n
            if (n == 0) return y;      // finished
            x *= x;                    // x = a^2, a^4, a^8, etc.
        }
    }
    else {                             // n < 0
        return Vec8d(1.0)/pow(x,-n);   // reciprocal
    }
}
// prevent implicit conversion of exponent to int
static Vec8d pow(Vec8d const & x, double y);

// Raise floating point numbers to integer power n, where n is a compile-time constant
template <int n>
static inline Vec8d pow_n(Vec8d const & a) {
    if (n < 0)    return Vec8d(1.0) / pow_n<-n>(a);
    if (n == 0)   return Vec8d(1.0);
    if (n >= 256) return pow(a, n);
    Vec8d x = a;                       // a^(2^i)
    Vec8d y;                           // accumulator
    const int lowest = n - (n & (n-1));// lowest set bit in n
    if (n & 1) y = x;
    if (n < 2) return y;
    x = x*x;                           // x^2
    if (n & 2) {
        if (lowest == 2) y = x; else y *= x;
    }
    if (n < 4) return y;
    x = x*x;                           // x^4
    if (n & 4) {
        if (lowest == 4) y = x; else y *= x;
    }
    if (n < 8) return y;
    x = x*x;                           // x^8
    if (n & 8) {
        if (lowest == 8) y = x; else y *= x;
    }
    if (n < 16) return y;
    x = x*x;                           // x^16
    if (n & 16) {
        if (lowest == 16) y = x; else y *= x;
    }
    if (n < 32) return y;
    x = x*x;                           // x^32
    if (n & 32) {
        if (lowest == 32) y = x; else y *= x;
    }
    if (n < 64) return y;
    x = x*x;                           // x^64
    if (n & 64) {
        if (lowest == 64) y = x; else y *= x;
    }
    if (n < 128) return y;
    x = x*x;                           // x^128
    if (n & 128) {
        if (lowest == 128) y = x; else y *= x;
    }
    return y;
}

template <int n>
static inline Vec8d pow(Vec8d const & a, Const_int_t<n>) {
    return pow_n<n>(a);
}

// function round: round to nearest integer (even). (result as double vector)
static inline Vec8d round(Vec8d const & a) {
    return _mm512_roundfxpnt_adjust_pd(a, _MM_FROUND_TO_NEAREST_INT, _MM_EXPADJ_NONE);
}

// function truncate: round towards zero. (result as double vector)
static inline Vec8d truncate(Vec8d const & a) {
    return _mm512_roundfxpnt_adjust_pd(a, _MM_FROUND_TO_ZERO, _MM_EXPADJ_NONE);
}

// function floor: round towards minus infinity. (result as double vector)
static inline Vec8d floor(Vec8d const & a) {
    return _mm512_roundfxpnt_adjust_pd(a, _MM_FROUND_TO_NEG_INF, _MM_EXPADJ_NONE);
}

// function ceil: round towards plus infinity. (result as double vector)
static inline Vec8d ceil(Vec8d const & a) {
    return _mm512_roundfxpnt_adjust_pd(a, _MM_FROUND_TO_POS_INF, _MM_EXPADJ_NONE);
}

// function round_to_int: round to nearest integer (even). (result as integer vector)
// The only way to represent Vec8i is to use Vec16i
static inline Vec16i round_to_int(Vec8d const & a) {
    // TODO: replace Vec16i with Vec8q
    return _mm512_cvtfxpnt_roundpd_epi32lo(a, _MM_FROUND_TO_NEAREST_INT);
}

static inline Vec16i truncate_to_int(Vec8d const & a, Vec8d const & b) {
    Vec16i t0 = _mm512_cvtfxpnt_roundpd_epi32lo(a, _MM_FROUND_TO_ZERO);
    Vec16i t1 = _mm512_cvtfxpnt_roundpd_epi32lo(b, _MM_FROUND_TO_ZERO);
    Vec16i t2 = _mm512_permutevar_epi32(constant16i<8,9,10,11,12,13,14,15, 0,1,2,3,4,5,6,7>(), t0);
    Vec16i t3 = _mm512_or_epi32(t1,t2);
    return t3;
}

// function to_double: convert low part of integer vector to double vector
static inline Vec8d to_double_low(Vec16i const & a) {
    return _mm512_cvtepi32lo_pd(a);
}

// function round_to_int: round to nearest integer (even)
// result as 64-bit integer vector, but with limited range
static inline Vec8q round_to_int64_limited(Vec8d const & a) {
    Vec16i t1 = round_to_int(a);
    return extend_low(t1);
}

// function compress: convert two Vec8d to one Vec16f
static inline Vec16f compress (Vec8d const & low, Vec8d const & high) {
    __m512i t0 = _mm512_castps_si512(_mm512_cvtpd_pslo(high));
    __m512i t1 = _mm512_castps_si512(_mm512_cvtpd_pslo(low)) ;

    return _mm512_castsi512_ps(_mm512_mask_permutevar_epi32(t1, 0xFF00, constant16i<8,9,10,11,12,13,14,15, 0, 1, 2, 3, 4, 5, 6, 7>(), t0));
}

// Function extend_low : convert Vec16f vector elements 0 - 7 to Vec8d
static inline Vec8d extend_low(Vec16f const & a) {
    return _mm512_cvtpslo_pd(a);
}

// Function extend_high : convert Vec8f vector elements 8 - 15 to Vec8d
static inline Vec8d extend_high (Vec16f const & a) {
    __m512i t0 = _mm512_castps_si512(a);
    __m512i t1 = _mm512_mask_permutevar_epi32(t0, 0xFFFF, constant16i<8,9,10,11,12,13,14,15, 0, 1, 2, 3, 4, 5, 6, 7>(), t0);
    __m512 t2 = _mm512_castsi512_ps(t1);
    return _mm512_cvtpslo_pd(t2);
}

// Math functions using fast bit manipulation

// TODO: is this useful?
#ifdef VECTORI256_H  // 256 bit integer vectors are available
// Extract the exponent as an integer
// exponent(a) = floor(log2(abs(a)));
// exponent(1.0) = 0, exponent(0.0) = -1023, exponent(INF) = +1024, exponent(NAN) = +1024
/* TODO:
static inline Vec4q exponent(Vec4d const & a) {
    assert(0);
#if VECTORI256_H > 1  // AVX2
    Vec4uq t1 = _mm256_castpd_si256(a);// reinterpret as 64-bit integer
    Vec4uq t2 = t1 << 1;               // shift out sign bit
    Vec4uq t3 = t2 >> 53;              // shift down logical to position 0
    Vec4q  t4 = Vec4q(t3) - 0x3FF;     // subtract bias from exponent
    return t4;
#else
    return Vec4q(exponent(a.get_low()), exponent(a.get_high()));
#endif
}

// Extract the fraction part of a floating point number
// a = 2^exponent(a) * fraction(a), except for a = 0
// fraction(1.0) = 1.0, fraction(5.0) = 1.25
static inline Vec8d fraction(Vec8d const & a) {
    assert(0);
    /* TODO:
    Vec4uq t1 = _mm256_castpd_si256(a);   // reinterpret as 64-bit integer
    Vec4uq t2 = Vec4uq((t1 & 0x000FFFFFFFFFFFFF) | 0x3FF0000000000000); // set exponent to 0 + bias
    return _mm256_castsi256_pd(t2);
}

// Fast calculation of pow(2,n) with n integer
// n  =     0 gives 1.0
// n >=  1024 gives +INF
// n <= -1023 gives 0.0
// This function will never produce denormals, and never raise exceptions
static inline Vec8d exp2(Vec8q const & n) {
    assert(0);
     TODO:
    Vec4q t1 = max(n,  -0x3FF);        // limit to allowed range
    Vec4q t2 = min(t1,  0x400);
    Vec4q t3 = t2 + 0x3FF;             // add bias
    Vec4q t4 = t3 << 52;               // put exponent into position 52
    return _mm256_castsi256_pd(t4);       // reinterpret as double
}
static inline Vec8d exp2(Vec8d const & x); // defined in vectormath_exp.h
*/
#endif
// Categorization functions

// Function sign_bit: gives true for elements that have the sign bit set
// even for -0.0, -INF and -NAN
// Note that sign_bit(Vec8d(-0.0)) gives true, while Vec8d(-0.0) < Vec8d(0.0) gives false
static inline Vec8b sign_bit(Vec8d const & a) {
    Vec8q t1 = _mm512_castpd_si512(a);    // reinterpret as 64-bit integer
    Vec8q t2 = t1 >> 63;                  // extend sign bit
    return _mm512_cmp_epi64_mask(t2, _mm512_set1_epi64(0xFFFFFFFFFFFFFFFF), _MM_CMPINT_EQ);
}

// Function sign_combine: changes the sign of a when b has the sign bit set
// same as select(sign_bit(b), -a, a)
static inline Vec8d sign_combine(Vec8d const & a, Vec8d const & b) {
    Vec8d signmask = constant8d<0x8000000000000000, 0x8000000000000000, 0x8000000000000000, 0x8000000000000000,
                                0x8000000000000000, 0x8000000000000000, 0x8000000000000000, 0x8000000000000000>();
    return a^ (b & signmask);
}

// Function is_finite: gives true for elements that are normal, denormal or zero,
// false for INF and NAN
static inline Vec8b is_finite(Vec8d const & a) {
    Vec8q  t1 = _mm512_castpd_si512(a); // reinterpret as 64-bit integer
    Vec8q  t2 = t1 << 1;                // shift out sign bit
    return Vec8b(t2 != 0xFFE0000000000000);// exponent field is not all 1s
}

// Function is_inf: gives true for elements that are +INF or -INF
// false for finite numbers and NAN
static inline Vec8b is_inf(Vec8d const & a) {
    Vec8q t1 = _mm512_castpd_si512(a); // reinterpret as 64-bit integer
    Vec8q t2 = t1 << 1;                // shift out sign bit
    return Vec8b(t2 == 0xFFE0000000000000);   // exponent is all 1s, fraction is 0 */
}

// Function is_nan: gives true for elements that are +NAN or -NAN
// false for finite numbers and +/-INF
static inline Vec8b is_nan(Vec8d const & a) {
    Vec8q t1 = _mm512_castpd_si512(a); // reinterpret as 32-bit integer
    Vec8q t2 = t1 << 1;                // shift out sign bit
    Vec8q t3 = 0xFFE0000000000000;     // exponent mask
    Vec8q t4 = t2 & t3;                // exponent
    Vec8q t5 = andnot(t2, t3);         // fraction
    return Vec8b(t4 == t3 && t5 != 0);// exponent = all 1s and fraction != 0
}

// Function is_subnormal: gives true for elements that are denormal (subnormal)
// false for finite numbers, zero, NAN and INF
static inline Vec8b is_subnormal(Vec8d const & a) {
    Vec8q t1 = _mm512_castpd_si512(a); // reinterpret as 64-bit integer
    Vec8q t2 = t1 << 1;                // shift out sign bit
    Vec8q t3 = 0xFFE0000000000000;     // exponent mask
    Vec8q t4 = t2 & t3;                // exponent
    Vec8q t5 = andnot(t2, t3);         // fraction
    return Vec8b(t4 == 0 && t5 != 0); // exponent = 0 and fraction != 0 */
}

static inline Vec8b is_zero_or_subnormal(Vec8d const & a)
{
    Vec8q t = _mm512_castpd_si512(a);    // reinterpret as 32-bit integer
    t &= 0x7FF0000000000000ll;          // isolate exponent
    return t == 0;                      // exponent = 0
}

// Function infinite2d: returns a vector where all elements are +INF
static inline Vec8d infinite8d() {
    return _mm512_set1_pd(0x7F80000000000000);
}

// Function nan2d: returns a vector where all elements are +NAN (quiet)
static inline Vec8d nan8d(int n = 0x10) {
    return _mm512_castsi512_pd(Vec8q(0x7FF8000000000000 + n));
}

// change signs on vectors Vec4d
// Each index i0 - i3 is 1 for changing sign on the corresponding element, 0 for no change
template <int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8>
static inline Vec8d change_sign(Vec8d const & a) {
    if ((i0 | i1 | i2 | i3 | i4 | i5 | i6 | i7 ) == 0) return a;
    __m512i mask = _mm512_setr_epi64(i0 ? 0x8000000000000000 : 0, i1 ? 0x8000000000000000 : 0, i2 ? 0x8000000000000000 : 0, i3 ? 0x8000000000000000 : 0,
                                     i4 ? 0x8000000000000000 : 0, i5 ? 0x8000000000000000 : 0, i6 ? 0x8000000000000000 : 0, i7 ? 0x8000000000000000 : 0);
    return _mm512_xor_pd(a, mask);

}

/*****************************************************************************
*
*          Functions for reinterpretation between vector types
*
*****************************************************************************/

static inline __m512i reinterpret_i (__m512i const & x) {
    return x;
}

static inline __m512i reinterpret_i (__m512  const & x) {
    return _mm512_castps_si512(x);
}

static inline __m512i reinterpret_i (__m512d const & x) {
    return _mm512_castpd_si512(x);
}

static inline __m512  reinterpret_f (__m512i const & x) {
    return _mm512_castsi512_ps(x);
}

static inline __m512  reinterpret_f (__m512  const & x) {
    return x;
}

static inline __m512  reinterpret_f (__m512d const & x) {
    return _mm512_castpd_ps(x);
}

static inline __m512d reinterpret_d (__m512i const & x) {
    return _mm512_castsi512_pd(x);
}

static inline __m512d reinterpret_d (__m512  const & x) {
    return _mm512_castps_pd(x);
}

static inline __m512d reinterpret_d (__m512d const & x) {
    return x;
}

// TODO: fix this comment
/*****************************************************************************
*
*          Vector permute and blend functions
*
******************************************************************************
*
* The permute function can reorder the elements of a vector and optionally
* set some elements to zero.
*
* The indexes are inserted as template parameters in <>. These indexes must be
* constants. Each template parameter is an index to the element you want to
* select. An index of -1 will generate zero. An index of -256 means don't care.
*
* Example:
* Vec4d a(10., 11., 12., 13.);    // a is (10, 11, 12, 13)
* Vec4d b;
* b = permute4d<1,0,-1,3>(a);     // b is (11, 10,  0, 13)
*
*
* The blend function can mix elements from two different vectors and
* optionally set some elements to zero.
*
* The indexes are inserted as template parameters in <>. These indexes must be
* constants. Each template parameter is an index to the element you want to
* select, where indexes 0 - 3 indicate an element from the first source
* vector and indexes 4 - 7 indicate an element from the second source vector.
* A negative index will generate zero.
*
*
* Example:
* Vec4d a(10., 11., 12., 13.);    // a is (10, 11, 12, 13)
* Vec4d b(20., 21., 22., 23.);    // a is (20, 21, 22, 23)
* Vec4d c;
* c = blend4d<4,3,7,-1> (a,b);    // c is (20, 13, 23,  0)
*
* A lot of the code here is metaprogramming aiming to find the instructions
* that best fit the template parameters and instruction set. The metacode
* will be reduced out to leave only a few vector instructions in release
* mode with optimization on.
*****************************************************************************/

// permute vector Vec8d
template <int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8>
static inline Vec8d permute4d(Vec8d const & a) {
    assert(0);
    /* TODO: use permut intrinsics
    const int ior = i0 | i1 | i2 | i3 | i4 | i5 | i6 | i7;  // OR indexes

    // is zeroing needed
    const bool do_zero    = ior < 0 && (ior & 0x80); // at least one index is negative, and not -0x100

    // is shuffling needed
    const bool do_shuffle = (i0>0) || (i1!=1 && i1>=0) || (i2!=2 && i2>=0) || (i3!=3 && i3>=0);

    if (!do_shuffle) {       // no shuffling needed
        if (do_zero) {       // zeroing
            if ((i0 & i1 & i2 & i3) < 0) {
                return _mm256_setzero_pd(); // zero everything
            }
            // zero some elements
            __m256d const mask = _mm256_castps_pd (
                constant8f< -int(i0>=0), -int(i0>=0), -int(i1>=0), -int(i1>=0), -int(i2>=0), -int(i2>=0), -int(i3>=0), -int(i3>=0) > ());
            return _mm256_and_pd(a, mask);     // zero with AND mask
        }
        else {
            return a;  // do nothing
        }
    }
#if INSTRSET >= 8  // AVX2: use VPERMPD
    __m256d x = _mm256_permute4x64_pd(a, (i0&3) | (i1&3)<<2 | (i2&3)<<4 | (i3&3)<<6);
    if (do_zero) {       // zeroing
        // zero some elements
        __m256d const mask2 = _mm256_castps_pd (
            constant8f< -int(i0>=0), -int(i0>=0), -int(i1>=0), -int(i1>=0), -int(i2>=0), -int(i2>=0), -int(i3>=0), -int(i3>=0) > ());
        x = _mm256_and_pd(x, mask2);     // zero with AND mask
    }
    return x;
#else   // AVX

    // Needed contents of low/high part of each source register in VSHUFPD
    // 0: a.low, 1: a.high, 3: zero
    const int s1 = (i0 < 0 ? 3 : (i0 & 2) >> 1) | (i2 < 0 ? 0x30 : (i2 & 2) << 3);
    const int s2 = (i1 < 0 ? 3 : (i1 & 2) >> 1) | (i3 < 0 ? 0x30 : (i3 & 2) << 3);
    // permute mask
    const int sm = (i0 < 0 ? 0 : (i0 & 1)) | (i1 < 0 ? 1 : (i1 & 1)) << 1 | (i2 < 0 ? 0 : (i2 & 1)) << 2 | (i3 < 0 ? 1 : (i3 & 1)) << 3;

    if (s1 == 0x01 || s1 == 0x11 || s2 == 0x01 || s2 == 0x11) {
        // too expensive to use 256 bit permute, split into two 128 bit permutes
        Vec2d alo = a.get_low();
        Vec2d ahi = a.get_high();
        Vec2d rlo = blend2d<i0, i1> (alo, ahi);
        Vec2d rhi = blend2d<i2, i3> (alo, ahi);
        return Vec4d(rlo, rhi);
    }

    // make operands for VSHUFPD
    __m256d r1, r2;

    switch (s1) {
    case 0x00:  // LL
        r1 = _mm256_insertf128_pd(a,_mm256_castpd256_pd128(a),1);  break;
    case 0x03:  // LZ
        r1 = _mm256_insertf128_pd(do_zero ? _mm256_setzero_pd() : __m256d(a), _mm256_castpd256_pd128(a), 1);
        break;
    case 0x10:  // LH
        r1 = a;  break;
    case 0x13:  // ZH
        r1 = do_zero ? _mm256_and_pd(a, _mm256_castps_pd(constant8f<0,0,0,0,-1,-1,-1,-1>())) : __m256d(a);  break;
    case 0x30:  // LZ
        if (do_zero) {
            __m128d t  = _mm256_castpd256_pd128(a);
            t  = _mm_and_pd(t,t);
            r1 = _mm256_castpd128_pd256(t);
        }
        else r1 = a;
        break;
    case 0x31:  // HZ
        r1 = _mm256_castpd128_pd256(_mm256_extractf128_pd(a,1));  break;
    case 0x33:  // ZZ
        r1 = do_zero ? _mm256_setzero_pd() : __m256d(a);  break;
    }

    if (s2 == s1) {
        if (sm == 0x0A) return r1;
        r2 = r1;
    }
    else {
        switch (s2) {
        case 0x00:  // LL
            r2 = _mm256_insertf128_pd(a,_mm256_castpd256_pd128(a),1);  break;
        case 0x03:  // ZL
            r2 = _mm256_insertf128_pd(do_zero ? _mm256_setzero_pd() : __m256d(a), _mm256_castpd256_pd128(a), 1);
            break;
        case 0x10:  // LH
            r2 = a;  break;
        case 0x13:  // ZH
            r2 = do_zero ? _mm256_and_pd(a,_mm256_castps_pd(constant8f<0,0,0,0,-1,-1,-1,-1>())) : __m256d(a);  break;
        case 0x30:  // LZ
            if (do_zero) {
                __m128d t  = _mm256_castpd256_pd128(a);
                t  = _mm_and_pd(t,t);
                r2 = _mm256_castpd128_pd256(t);
            }
            else r2 = a;
            break;
        case 0x31:  // HZ
            r2 = _mm256_castpd128_pd256(_mm256_extractf128_pd(a,1));  break;
        case 0x33:  // ZZ
            r2 = do_zero ? _mm256_setzero_pd() : __m256d(a);  break;
        }
    }
    return  _mm256_shuffle_pd(r1, r2, sm);

#endif  // INSTRSET >= 8*/
}


// blend vectors Vec8d
template <int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8>
static inline Vec8d blend8d(Vec8d const & a, Vec8d const & b) {
    assert(0);
    /* TODO: use blend intrinsics?
    // Combine all the indexes into a single bitfield, with 8 bits for each
    const int m1 = (i0 & 7) | (i1 & 7) << 8 | (i2 & 7) << 16 | (i3 & 7) << 24;

    // Mask to zero out negative indexes
    const uint32_t mz = (i0 < 0 ? 0 : 0xFF) | (i1 < 0 ? 0 : 0xFF) << 8 | (i2 < 0 ? 0 : 0xFF) << 16 | (i3 < 0 ? 0 : 0xFF) << 24;

    if (mz == 0) return _mm256_setzero_pd();  // all zero

    __m256d t1;
    if ((((m1 & 0xFEFEFEFE) ^ 0x06020400) & mz) == 0) {
        // fits VSHUFPD(a,b)
        t1 = _mm256_shuffle_pd(a, b, (i0 & 1) | (i1 & 1) << 1 | (i2 & 1) << 2 | (i3 & 1) << 3);
        if (mz == 0xFFFFFFFF) return t1;
        return permute4d<i0 < 0 ? -1 : 0, i1 < 0 ? -1 : 1, i2 < 0 ? -1 : 2, i3 < 0 ? -1 : 3> (t1);
    }
    if ((((m1 & 0xFEFEFEFE) ^0x02060004) & mz) == 0) {
        // fits VSHUFPD(b,a)
        t1 = _mm256_shuffle_pd(b, a, (i0 & 1) | (i1 & 1) << 1 | (i2 & 1) << 2 | (i3 & 1) << 3);
        if (mz == 0xFFFFFFFF) return t1;
        return permute4d<i0 < 0 ? -1 : 0, i1 < 0 ? -1 : 1, i2 < 0 ? -1 : 2, i3 < 0 ? -1 : 3> (t1);
    }
    if ((((m1 & 0x03030303) ^ 0x03020100) & mz) == 0) {
        // blend and zero, no permute
        if ((m1 & 0x04040404 & mz) == 0) {
            t1 = a;
        }
        else if (((m1 ^ 0x04040404) & 0x04040404 & mz) == 0) {
            t1 = b;
        }
        else {
            t1 = _mm256_blend_pd(a, b, (i0&4)>>2 | (i1&4)>>1 | (i2&4) | (i3&4) << 1);
        }
        if (mz == 0xFFFFFFFF) return t1;
        return permute4d<i0 < 0 ? -1 : 0, i1 < 0 ? -1 : 1, i2 < 0 ? -1 : 2, i3 < 0 ? -1 : 3> (t1);
    }
    if ((m1 & 0x04040404 & mz) == 0) {
        // all from a
        return permute4d<i0, i1, i2, i3> (a);
    }
    if (((m1 ^ 0x04040404) & 0x04040404 & mz) == 0) {
        // all from b
        return permute4d<i0 ^ 4, i1 ^ 4, i2 ^ 4, i3 ^ 4> (b);
    }
    // check if we can do 128-bit blend/permute
    if (((m1 ^ 0x01000100) & 0x01010101 & mz) == 0) {
        const uint32_t j0 = uint32_t((i0 >= 0 ? i0 : i1 >= 0 ? i1 : -1) >> 1);
        const uint32_t j1 = uint32_t((i2 >= 0 ? i2 : i3 >= 0 ? i3 : -1) >> 1);
        if (((m1 ^ ((j0 & 3) * 0x00000202 | (j1 & 3) * 0x02020000)) & 0x06060606 & mz) == 0) {
            t1 = _mm256_permute2f128_pd(a, b, (j0 & 0x0F) | (j1 & 0x0F) << 4);
            const bool partialzero = (((i0 | i1) ^ j0) & 0x80) != 0 || (((i2 | i3) ^ j1) & 0x80) != 0;
            if (partialzero) {
                // zero some elements
                __m256d mask = _mm256_castps_pd (constant8f <
                    i0 < 0 ? 0 : -1, i0 < 0 ? 0 : -1, i1 < 0 ? 0 : -1, i1 < 0 ? 0 : -1,
                    i2 < 0 ? 0 : -1, i2 < 0 ? 0 : -1, i3 < 0 ? 0 : -1, i3 < 0 ? 0 : -1 > ());
                return _mm256_and_pd(t1, mask);
            }
            else return t1;
        }
    }
    // general case. combine two permutes
    Vec4d a1 = permute4d <
        (uint32_t)i0 < 4 ? i0 : -0x100,
        (uint32_t)i1 < 4 ? i1 : -0x100,
        (uint32_t)i2 < 4 ? i2 : -0x100,
        (uint32_t)i3 < 4 ? i3 : -0x100 > (a);
    Vec4d b1 = permute4d <
        (uint32_t)(i0^4) < 4 ? (i0^4) : -0x100,
        (uint32_t)(i1^4) < 4 ? (i1^4) : -0x100,
        (uint32_t)(i2^4) < 4 ? (i2^4) : -0x100,
        (uint32_t)(i3^4) < 4 ? (i3^4) : -0x100 > (b);
    t1 = _mm256_blend_pd(a1, b1, (i0&4)>>2 | (i1&4)>>1 | (i2&4) | (i3&4) << 1);
    if (mz == 0xFFFFFFFF) return t1;
    return permute4d<i0 < 0 ? -1 : 0, i1 < 0 ? -1 : 1, i2 < 0 ? -1 : 2, i3 < 0 ? -1 : 3> (t1);*/
}

/*****************************************************************************
*
*          Vector Vec16f permute and blend functions
*
*****************************************************************************/

// permute vector Vec8f
template <int i0, int i1, int i2,  int i3,  int i4,  int i5,  int i6,  int i7,
          int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15>
static inline Vec16f permute16f(Vec16f const & a) {
    assert(0);
    /* TODO:
    __m256 t1, mask;

    const int ior = i0 | i1 | i2 | i3 | i4 | i5 | i6 | i7;  // OR indexes

    // is zeroing needed
    const bool do_zero    = ior < 0 && (ior & 0x80); // at least one index is negative, and not -0x100

    // is shuffling needed
    const bool do_shuffle = (i0>0) || (i1!=1 && i1>=0) || (i2!=2 && i2>=0) || (i3!=3 && i3>=0) ||
        (i4!=4 && i4>=0) || (i5!=5 && i5>=0) || (i6!=6 && i6>=0) || (i7!=7 && i7>=0);

    if (!do_shuffle) {       // no shuffling needed
        if (do_zero) {       // zeroing
            if ((i0 & i1 & i2 & i3 & i4 & i5 & i6 & i7) < 0) {
                return _mm256_setzero_ps(); // zero everything
            }
            // zero some elements
            mask = constant8f< -int(i0>=0), -int(i1>=0), -int(i2>=0), -int(i3>=0), -int(i4>=0), -int(i5>=0), -int(i6>=0), -int(i7>=0) > ();
            return _mm256_and_ps(a, mask);     // zero with AND mask
        }
        else {
            return a;  // do nothing
        }
    }

#if INSTRSET >= 8  // AVX2: use VPERMPS
    if (do_shuffle) {    // shuffling
        mask = constant8f< i0 & 7, i1 & 7, i2 & 7, i3 & 7, i4 & 7, i5 & 7, i6 & 7, i7 & 7 > ();
#if defined (_MSC_VER) && _MSC_VER < 1700 && ! defined(__INTEL_COMPILER)
        // bug in MS VS 11 beta: operands in wrong order. fixed in 11.0
        t1 = _mm256_permutevar8x32_ps(mask, _mm256_castps_si256(a));      //  problem in immintrin.h
#elif defined (GCC_VERSION) && GCC_VERSION <= 40700 && !defined(__INTEL_COMPILER) && !defined(__clang__)
        // Gcc 4.7.0 has wrong parameter type and operands in wrong order. fixed in version 4.7.1
        t1 = _mm256_permutevar8x32_ps(mask, a);
#else   // no bug version
        t1 = _mm256_permutevar8x32_ps(a, _mm256_castps_si256(mask));
#endif
    }
    else {
        t1 = a;          // no shuffling
    }
    if (do_zero) {       // zeroing
        if ((i0 & i1 & i2 & i3 & i4 & i5 & i6 & i7) < 0) {
            return _mm256_setzero_ps(); // zero everything
        }
        // zero some elements
        mask = constant8f< -int(i0>=0), -int(i1>=0), -int(i2>=0), -int(i3>=0), -int(i4>=0), -int(i5>=0), -int(i6>=0), -int(i7>=0) > ();
        t1 = _mm256_and_ps(t1, mask);     // zero with AND mask
    }
    return t1;
#else   // AVX

    // Combine all the indexes into a single bitfield, with 4 bits for each
    const int m1 = (i0&7) | (i1&7)<<4 | (i2&7)<<8 | (i3&7)<<12 | (i4&7)<<16 | (i5&7)<<20 | (i6&7)<<24 | (i7&7)<<28;

    // Mask to zero out negative indexes
    const int m2 = (i0<0?0:0xF) | (i1<0?0:0xF)<<4 | (i2<0?0:0xF)<<8 | (i3<0?0:0xF)<<12 | (i4<0?0:0xF)<<16 | (i5<0?0:0xF)<<20 | (i6<0?0:0xF)<<24 | (i7<0?0:0xF)<<28;

    // Check if it is possible to use VSHUFPS. Index n must match index n+4 on bit 0-1, and even index n must match odd index n+1 on bit 2
    const bool sps = ((m1 ^ (m1 >> 16)) & 0x3333 & m2 & (m2 >> 16)) == 0  &&  ((m1 ^ (m1 >> 4)) & 0x04040404 & m2 & m2 >> 4) == 0;

    if (sps) {   // can use VSHUFPS

        // Index of each pair (i[n],i[n+1])
        const int j0 = i0 >= 0 ? i0 : i1;
        const int j1 = i2 >= 0 ? i2 : i3;
        const int j2 = i4 >= 0 ? i4 : i5;
        const int j3 = i6 >= 0 ? i6 : i7;

        // Index of each pair (i[n],i[n+4])
        const int k0 = i0 >= 0 ? i0 : i4;
        const int k1 = i1 >= 0 ? i1 : i5;
        const int k2 = i2 >= 0 ? i2 : i6;
        const int k3 = i3 >= 0 ? i3 : i7;

        // Needed contents of low/high part of each source register in VSHUFPS
        // 0: a.low, 1: a.high, 3: zero or don't care
        const int s1 = (j0 < 0 ? 3 : (j0 & 4) >> 2) | (j2 < 0 ? 0x30 : (j2 & 4) << 2);
        const int s2 = (j1 < 0 ? 3 : (j1 & 4) >> 2) | (j3 < 0 ? 0x30 : (j3 & 4) << 2);

        // calculate cost of using VSHUFPS
        const int cost1 = (s1 == 0x01 || s1 == 0x11) ? 2 : (s1 == 0x00 || s1 == 0x03 || s1 == 0x31) ? 1 : 0;
        const int cost2 = (s2 == s1) ? 0 : (s2 == 0x01 || s2 == 0x11) ? 2 : (s2 == 0x00 || (s2 == 0x03 && (s1 & 0xF0) != 0x00) || (s2 == 0x31 && (s1 & 0x0F) != 0x01)) ? 1 : 0;

        if (cost1 + cost2 <= 3) {

            // permute mask
            const int sm = (k0 < 0 ? 0 : (k0 & 3)) | (k1 < 0 ? 1 : (k1 & 3)) << 2 | (k2 < 0 ? 2 : (k2 & 3)) << 4 | (k3 < 0 ? 3 : (k3 & 3)) << 6;

            // make operands for VSHUFPS
            __m256 r1, r2;

            switch (s1) {
            case 0x00:  // LL
            case 0x03:  // ZL
                r1 = _mm256_insertf128_ps(a,_mm256_castps256_ps128(a),1);  break;
            case 0x01:  // HL
                r1 = _mm256_castps128_ps256(_mm256_extractf128_ps(a,1));
                r1 = _mm256_insertf128_ps(r1,_mm256_castps256_ps128(a),1);  break;
            case 0x10:  // LH
            case 0x13:  // ZH
            case 0x30:  // LZ
            case 0x33:  // ZZ
                r1 = a;  break;
            case 0x11:  // HH
                r1 = _mm256_castps128_ps256(_mm256_extractf128_ps(a,1));
                r1 = _mm256_insertf128_ps(r1,_mm256_castps256_ps128(r1),1);  break;
            case 0x31:  // HZ
                r1 = _mm256_castps128_ps256(_mm256_extractf128_ps(a,1));  break;
            }

            if (s2 == s1) {
                if (sm == 0xE4) return r1;
                r2 = r1;
            }
            else {
                switch (s2) {
                case 0x00:  // LL
                    r2 = _mm256_insertf128_ps(a,_mm256_castps256_ps128(a),1);  break;
                case 0x03:  // ZL
                    if ((s1 & 0xF0) == 0x00) r2 = r1;
                    else {
                        r2 = _mm256_insertf128_ps(a,_mm256_castps256_ps128(a),1);
                    }
                    break;
                case 0x01:  // HL
                    r2 = _mm256_castps128_ps256(_mm256_extractf128_ps(a,1));
                    r2 = _mm256_insertf128_ps(r1,_mm256_castps256_ps128(a),1);  break;
                case 0x10:  // LH
                case 0x13:  // ZH
                case 0x30:  // LZ
                case 0x33:  // ZZ
                    r2 = a;  break;
                case 0x11:  // HH
                    r2 = _mm256_castps128_ps256(_mm256_extractf128_ps(a,1));
                    r2 = _mm256_insertf128_ps(r2,_mm256_castps256_ps128(r2),1);  break;
                case 0x31:  // HZ
                    if ((s1 & 0x0F) == 0x01) r2 = r1;
                    else {
                        r2 = _mm256_castps128_ps256(_mm256_extractf128_ps(a,1));
                    }
                    break;
                }
            }

            // now the permute instruction
            t1 = _mm256_shuffle_ps(r1, r2, sm);

            if (do_zero) {
                // zero some elements
                mask = constant8f< -int(i0>=0), -int(i1>=0), -int(i2>=0), -int(i3>=0), -int(i4>=0), -int(i5>=0), -int(i6>=0), -int(i7>=0) > ();
                t1 = _mm256_and_ps(t1, mask);     // zero with AND mask
            }
            return t1;
        }
    }
    // not using VSHUFPS. Split into low and high part
    Vec4f alo = a.get_low();
    Vec4f ahi = a.get_high();
    Vec4f rlo = blend4f<i0, i1, i2, i3> (alo, ahi);
    Vec4f rhi = blend4f<i4, i5, i6, i7> (alo, ahi);
    return Vec8f(rlo, rhi);
#endif*/
}


// blend vectors Vec8f
template <int i0, int i1, int i2,  int i3,  int i4,  int i5,  int i6,  int i7,
          int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15>
static inline Vec16f blend16f(Vec16f const & a, Vec16f const & b) {
    assert(0);
    /* TODO:

    const int ior = i0 | i1 | i2 | i3 | i4 | i5 | i6 | i7;  // OR indexes

    // is zeroing needed
    const bool do_zero  = ior < 0 && (ior & 0x80); // at least one index is negative, and not -0x100

    // Combine all the indexes into a single bitfield, with 4 bits for each
    const int m1 = (i0&0xF) | (i1&0xF)<<4 | (i2&0xF)<<8 | (i3&0xF)<<12 | (i4&0xF)<<16 | (i5&0xF)<<20 | (i6&0xF)<<24 | (i7&0xF)<<28;

    // Mask to zero out negative indexes
    const int mz = (i0<0?0:0xF) | (i1<0?0:0xF)<<4 | (i2<0?0:0xF)<<8 | (i3<0?0:0xF)<<12 | (i4<0?0:0xF)<<16 | (i5<0?0:0xF)<<20 | (i6<0?0:0xF)<<24 | (i7<0?0:0xF)<<28;

    __m256 t1, mask;

    if (mz == 0) return _mm256_setzero_ps();  // all zero

    if ((m1 & 0x88888888 & mz) == 0) {
        // all from a
        return permute8f<i0, i1, i2, i3, i4, i5, i6, i7> (a);
    }

    if (((m1 ^ 0x88888888) & 0x88888888 & mz) == 0) {
        // all from b
        return permute8f<i0&~8, i1&~8, i2&~8, i3&~8, i4&~8, i5&~8, i6&~8, i7&~8> (b);
    }

    if ((((m1 & 0x77777777) ^ 0x76543210) & mz) == 0) {
        // blend and zero, no permute
        mask = constant8f<(i0&8)?0:-1, (i1&8)?0:-1, (i2&8)?0:-1, (i3&8)?0:-1, (i4&8)?0:-1, (i5&8)?0:-1, (i6&8)?0:-1, (i7&8)?0:-1> ();
        t1   = select(mask, a, b);
        if (!do_zero) return t1;
        // zero some elements
        mask = constant8f< (i0<0&&(i0&8)) ? 0 : -1, (i1<0&&(i1&8)) ? 0 : -1, (i2<0&&(i2&8)) ? 0 : -1, (i3<0&&(i3&8)) ? 0 : -1,
            (i4<0&&(i4&8)) ? 0 : -1, (i5<0&&(i5&8)) ? 0 : -1, (i6<0&&(i6&8)) ? 0 : -1, (i7<0&&(i7&8)) ? 0 : -1 > ();
        return _mm256_and_ps(t1, mask);
    }

    // check if we can do 128-bit blend/permute
    if (((m1 ^ 0x32103210) & 0x33333333 & mz) == 0) {
        const uint32_t j0 = (i0 >= 0 ? i0 : i1 >= 0 ? i1 : i2 >= 0 ? i2 : i3 >= 0 ? i3 : -1) >> 2;
        const uint32_t j1 = (i4 >= 0 ? i4 : i5 >= 0 ? i5 : i6 >= 0 ? i6 : i7 >= 0 ? i7 : -1) >> 2;
        if (((m1 ^ ((j0 & 3) * 0x00004444 | (j1 & 3) * 0x44440000)) & 0xCCCCCCCC & mz) == 0) {
            t1 = _mm256_permute2f128_ps(a, b, (j0 & 0x0F) | (j1 & 0x0F) << 4);
            const bool partialzero = (((i0 | i1 | i2 | i3) ^ j0) & 0x80) != 0 || (((i4 | i5 | i6 | i7) ^ j1) & 0x80) != 0;
            if (partialzero) {
                // zero some elements
                mask = constant8f< i0 < 0 ? 0 : -1, i1 < 0 ? 0 : -1, i2 < 0 ? 0 : -1, i3 < 0 ? 0 : -1,
                    i4 < 0 ? 0 : -1, i5 < 0 ? 0 : -1, i6 < 0 ? 0 : -1, i7 < 0 ? 0 : -1 > ();
                return _mm256_and_ps(t1, mask);
            }
            else return t1;
        }
    }
    // Not checking special cases for vunpckhps, vunpcklps: they are too rare

    // Check if it is possible to use VSHUFPS.
    // Index n must match index n+4 on bit 0-1, and even index n must match odd index n+1 on bit 2-3
    const bool sps = ((m1 ^ (m1 >> 16)) & 0x3333 & mz & (mz >> 16)) == 0  &&  ((m1 ^ (m1 >> 4)) & 0x0C0C0C0C & mz & mz >> 4) == 0;

    if (sps) {   // can use VSHUFPS

        // Index of each pair (i[n],i[n+1])
        const int j0 = i0 >= 0 ? i0 : i1;
        const int j1 = i2 >= 0 ? i2 : i3;
        const int j2 = i4 >= 0 ? i4 : i5;
        const int j3 = i6 >= 0 ? i6 : i7;

        // Index of each pair (i[n],i[n+4])
        const int k0 = i0 >= 0 ? i0 : i4;
        const int k1 = i1 >= 0 ? i1 : i5;
        const int k2 = i2 >= 0 ? i2 : i6;
        const int k3 = i3 >= 0 ? i3 : i7;

        // Needed contents of low/high part of each source register in VSHUFPS
        // 0: a.low, 1: a.high, 2: b.low, 3: b.high, 4: zero or don't care
        const int s1 = (j0 < 0 ? 4 : (j0 & 0xC) >> 2) | (j2 < 0 ? 0x30 : (j2 & 0xC) << 2);
        const int s2 = (j1 < 0 ? 3 : (j1 & 0xC) >> 2) | (j3 < 0 ? 0x30 : (j3 & 0xC) << 2);

        // permute mask
        const int sm = (k0 < 0 ? 0 : (k0 & 3)) | (k1 < 0 ? 1 : (k1 & 3)) << 2 | (k2 < 0 ? 2 : (k2 & 3)) << 4 | (k3 < 0 ? 3 : (k3 & 3)) << 6;

        __m256 r1, r2;
        __m128 ahi = _mm256_extractf128_ps(a,1);    // 1
        __m128 bhi = _mm256_extractf128_ps(b,1);    // 3

        switch (s1) {
        case 0x00:  case 0x04:
            r1 = _mm256_insertf128_ps(a,_mm256_castps256_ps128(a),1);  break;
        case 0x01:  case 0x41:
            r1 = _mm256_insertf128_ps(_mm256_castps128_ps256(ahi),_mm256_castps256_ps128(a),1);  break;
        case 0x02:
            r1 = _mm256_insertf128_ps(b,_mm256_castps256_ps128(a),1);  break;
        case 0x03:
            r1 = _mm256_insertf128_ps(_mm256_castps128_ps256(bhi),_mm256_castps256_ps128(a),1);  break;
        case 0x10:  case 0x14:  case 0x40:  case 0x44:
            r1 = a;  break;
        case 0x11:
            r1 = _mm256_insertf128_ps(_mm256_castps128_ps256(ahi),ahi,1);  break;
        case 0x12:
            r1 = _mm256_insertf128_ps(b,ahi,1);  break;
        case 0x13:
            r1 = _mm256_insertf128_ps(_mm256_castps128_ps256(bhi),ahi,1);  break;
        case 0x20:
            r1 = _mm256_insertf128_ps(a,_mm256_castps256_ps128(b),1);  break;
        case 0x21:
            r1 = _mm256_insertf128_ps(_mm256_castps128_ps256(ahi),_mm256_castps256_ps128(b),1);  break;
        case 0x22:  case 0x24:  case 0x42:
            r1 = _mm256_insertf128_ps(b,_mm256_castps256_ps128(b),1);  break;
        case 0x23:  case 0x43:
            r1 = _mm256_insertf128_ps(_mm256_castps128_ps256(bhi),_mm256_castps256_ps128(b),1);  break;
        case 0x30:
            r1 = _mm256_insertf128_ps(a,bhi,1);  break;
        case 0x31:
            r1 = _mm256_insertf128_ps(_mm256_castps128_ps256(ahi),bhi,1);  break;
        case 0x32:  case 0x34:
            r1 = b;  break;
        case 0x33:
            r1 = _mm256_insertf128_ps(_mm256_castps128_ps256(bhi),bhi,1);  break;
        }
        if (s2 == s1 || ((s2 & 0x04) && ((s1 ^ s2) & 0xF0) == 0) || ((s2 & 0x40) && ((s1 ^ s2) & 0x0F) == 0)) {
            // can use r2 = r1
            if (sm == 0xE4) return r1;  // no shuffling needed
            r2 = r1;
        }
        else {
            switch (s2) {
            case 0x00:  case 0x04:
                r2 = _mm256_insertf128_ps(a,_mm256_castps256_ps128(a),1);  break;
            case 0x01:  case 0x41:
                r2 = _mm256_insertf128_ps(_mm256_castps128_ps256(ahi),_mm256_castps256_ps128(a),1);  break;
            case 0x02:
                r2 = _mm256_insertf128_ps(b,_mm256_castps256_ps128(a),1);  break;
            case 0x03:
                r2 = _mm256_insertf128_ps(_mm256_castps128_ps256(bhi),_mm256_castps256_ps128(a),1);  break;
            case 0x10:  case 0x14:  case 0x40:  case 0x44:
                r2 = a;  break;
            case 0x11:
                r2 = _mm256_insertf128_ps(_mm256_castps128_ps256(ahi),ahi,1);  break;
            case 0x12:
                r2 = _mm256_insertf128_ps(b,ahi,1);  break;
            case 0x13:
                r2 = _mm256_insertf128_ps(_mm256_castps128_ps256(bhi),ahi,1);  break;
            case 0x20:
                r2 = _mm256_insertf128_ps(a,_mm256_castps256_ps128(b),1);  break;
            case 0x21:
                r2 = _mm256_insertf128_ps(_mm256_castps128_ps256(ahi),_mm256_castps256_ps128(b),1);  break;
            case 0x22:  case 0x24:  case 0x42:
                r2 = _mm256_insertf128_ps(b,_mm256_castps256_ps128(b),1);  break;
            case 0x23:  case 0x43:
                r2 = _mm256_insertf128_ps(_mm256_castps128_ps256(bhi),_mm256_castps256_ps128(b),1);  break;
            case 0x30:
                r2 = _mm256_insertf128_ps(a,bhi,1);  break;
            case 0x31:
                r2 = _mm256_insertf128_ps(_mm256_castps128_ps256(ahi),bhi,1);  break;
            case 0x32:  case 0x34:
                r2 = b;  break;
            case 0x33:
                r2 = _mm256_insertf128_ps(_mm256_castps128_ps256(bhi),bhi,1);  break;
            }
        }

        // now the shuffle instruction
        t1 = _mm256_shuffle_ps(r1, r2, sm);

        if (do_zero) {
            // zero some elements
            mask = constant8f< -int(i0>=0), -int(i1>=0), -int(i2>=0), -int(i3>=0), -int(i4>=0), -int(i5>=0), -int(i6>=0), -int(i7>=0) > ();
            t1 = _mm256_and_ps(t1, mask);     // zero with AND mask
        }
        return t1;
    }

    // Check if we can use 64-bit blend. Even numbered indexes must be even and odd numbered
    // indexes must be equal to the preceding index + 1, except for negative indexes.
    if (((m1 ^ 0x10101010) & 0x11111111 & mz) == 0 && ((m1 ^ m1 >> 4) & 0x0E0E0E0E & mz & mz >> 4) == 0) {

        const bool partialzero = int((i0 ^ i1) | (i2 ^ i3) | (i4 ^ i5) | (i6 ^ i7)) < 0; // part of a 64-bit block is zeroed
        const int blank1 = partialzero ? -0x100 : -1;  // ignore or zero
        const int n0 = i0 > 0 ? i0/2 : i1 > 0 ? i1/2 : blank1;  // indexes for 64 bit blend
        const int n1 = i2 > 0 ? i2/2 : i3 > 0 ? i3/2 : blank1;
        const int n2 = i4 > 0 ? i4/2 : i5 > 0 ? i5/2 : blank1;
        const int n3 = i6 > 0 ? i6/2 : i7 > 0 ? i7/2 : blank1;
        t1 = _mm256_castpd_ps (blend4d<n0,n1,n2,n3> (_mm256_castps_pd(a), _mm256_castps_pd(b)));
        if (blank1 == -1 || !do_zero) {
            return  t1;
        }
        // need more zeroing
        mask = constant8f< -int(i0>=0), -int(i1>=0), -int(i2>=0), -int(i3>=0), -int(i4>=0), -int(i5>=0), -int(i6>=0), -int(i7>=0) > ();
        return _mm256_and_ps(t1, mask);     // zero with AND mask
    }

    // general case: permute and blend and possible zero
    const int blank2 = do_zero ? -1 : -0x100;  // ignore or zero

    Vec8f ta = permute8f <
        (uint32_t)i0 < 8 ? i0 : blank2,
        (uint32_t)i1 < 8 ? i1 : blank2,
        (uint32_t)i2 < 8 ? i2 : blank2,
        (uint32_t)i3 < 8 ? i3 : blank2,
        (uint32_t)i4 < 8 ? i4 : blank2,
        (uint32_t)i5 < 8 ? i5 : blank2,
        (uint32_t)i6 < 8 ? i6 : blank2,
        (uint32_t)i7 < 8 ? i7 : blank2 > (a);
    Vec8f tb = permute8f <
        (uint32_t)(i0^8) < 8 ? (i0^8) : blank2,
        (uint32_t)(i1^8) < 8 ? (i1^8) : blank2,
        (uint32_t)(i2^8) < 8 ? (i2^8) : blank2,
        (uint32_t)(i3^8) < 8 ? (i3^8) : blank2,
        (uint32_t)(i4^8) < 8 ? (i4^8) : blank2,
        (uint32_t)(i5^8) < 8 ? (i5^8) : blank2,
        (uint32_t)(i6^8) < 8 ? (i6^8) : blank2,
        (uint32_t)(i7^8) < 8 ? (i7^8) : blank2 > (b);

    if (blank2 == -1) {
        return  _mm256_or_ps(ta, tb);
    }
    // no zeroing, need to blend
    const int maskb = ((i0 >> 3) & 1) | ((i1 >> 2) & 2) | ((i2 >> 1) & 4) | (i3 & 8) |
        ((i4 << 1) & 0x10) | ((i5 << 2) & 0x20) | ((i6 << 3) & 0x40) | ((i7 << 4) & 0x80);
    return _mm256_blend_ps(ta, tb, maskb);  // blend */
}


/*****************************************************************************
*
*          Vector lookup functions
*
******************************************************************************
*
* These functions use vector elements as indexes into a table.
* The table is given as one or more vectors or as an array.
*
* This can be used for several purposes:
*  - table lookup
*  - permute or blend with variable indexes
*  - blend from more than two sources
*  - gather non-contiguous data
*
* An index out of range may produce any value - the actual value produced is
* implementation dependent and may be different for different instruction
* sets. An index out of range does not produce an error message or exception.
*
* Example:
* Vec4i a(2,0,0,3);               // index  a is (  2,   0,   0,   3)
* Vec4f b(1.0f,1.1f,1.2f,1.3f);   // table  b is (1.0, 1.1, 1.2, 1.3)
* Vec4f c;
* c = lookup4 (a,b);              // result c is (1.2, 1.0, 1.0, 1.3)
*
*****************************************************************************/

#ifdef VECTORI256_H  // Vec8i and Vec4q must be defined

static inline Vec8f lookup8(Vec8i const & index, Vec8f const & table) {
#if INSTRSET >= 8 && VECTORI256_H > 1 // AVX2
#if defined (_MSC_VER) && _MSC_VER < 1700 && ! defined(__INTEL_COMPILER)
    // bug in MS VS 11 beta: operands in wrong order. fixed in 11.0
    return _mm256_permutevar8x32_ps(_mm256_castsi256_ps(index), _mm256_castps_si256(table));
#elif defined (GCC_VERSION) && GCC_VERSION <= 40700 && !defined(__INTEL_COMPILER) && !defined(__clang__)
        // Gcc 4.7.0 has wrong parameter type and operands in wrong order. fixed in version 4.7.1
    return _mm256_permutevar8x32_ps(_mm256_castsi256_ps(index), table);
#else
    // no bug version
    return _mm256_permutevar8x32_ps(table, index);
#endif

#else // AVX
    // swap low and high part of table
    __m256  t1 = _mm256_castps128_ps256(_mm256_extractf128_ps(table, 1));
    __m256  t2 = _mm256_insertf128_ps(t1, _mm256_castps256_ps128(table), 1);
    // join index parts
    __m256i index2 = _mm256_insertf128_si256(_mm256_castsi128_si256(index.get_low()), index.get_high(), 1);
    // permute within each 128-bit part
    __m256  r0 = _mm256_permutevar_ps(table, index2);
    __m256  r1 = _mm256_permutevar_ps(t2,    index2);
    // high index bit for blend
    __m128i k1 = _mm_slli_epi32(index.get_high() ^ 4, 29);
    __m128i k0 = _mm_slli_epi32(index.get_low(),      29);
    __m256  kk = _mm256_insertf128_ps(_mm256_castps128_ps256(_mm_castsi128_ps(k0)), _mm_castsi128_ps(k1), 1);
    // blend the two permutes
    return _mm256_blendv_ps(r0, r1, kk);
#endif
}

template <int n>
static inline Vec8f lookup(Vec8i const & index, float const * table) {
    if (n <= 0) return 0;
    if (n <= 4) {
        Vec4f table1 = Vec4f().load(table);
        return Vec8f(
            lookup4 (index.get_low(),  table1),
            lookup4 (index.get_high(), table1));
    }
#if INSTRSET < 8  // not AVX2
    if (n <= 8) {
        return lookup8(index, Vec8f().load(table));
    }
#endif
    // Limit index
    Vec8ui index1;
    if ((n & (n-1)) == 0) {
        // n is a power of 2, make index modulo n
        index1 = Vec8ui(index) & (n-1);
    }
    else {
        // n is not a power of 2, limit to n-1
        index1 = min(Vec8ui(index), n-1);
    }
#if INSTRSET >= 8 && VECTORI256_H > 1 // AVX2
    return _mm256_i32gather_ps(table, index1, 4);
#else // AVX
    return Vec8f(table[index1[0]],table[index1[1]],table[index1[2]],table[index1[3]],
    table[index1[4]],table[index1[5]],table[index1[6]],table[index1[7]]);
#endif
}

static inline Vec4d lookup4(Vec4q const & index, Vec4d const & table) {
#if INSTRSET >= 8 && VECTORI256_H > 1 // AVX2
    // We can't use VPERMPD because it has constant indexes.
    // Convert the index to fit VPERMPS
    Vec8i index1 = permute8i<0,0,2,2,4,4,6,6> (Vec8i(index+index));
    Vec8i index2 = index1 + Vec8i(constant8i<0,1,0,1,0,1,0,1>());
#if defined (_MSC_VER) && _MSC_VER < 1700 && ! defined(__INTEL_COMPILER)
    // bug in MS VS 11 beta: operands in wrong order. fixed in 11.0
    return _mm256_castps_pd(_mm256_permutevar8x32_ps(_mm256_castsi256_ps(index2), _mm256_castpd_si256(table)));
#elif defined (GCC_VERSION) && GCC_VERSION <= 40700 && !defined(__INTEL_COMPILER) && !defined(__clang__)
        // Gcc 4.7.0 has wrong parameter type and operands in wrong order
    return _mm256_castps_pd(_mm256_permutevar8x32_ps(_mm256_castsi256_ps(index2), _mm256_castpd_ps(table)));
#else
    // no bug version
    return _mm256_castps_pd(_mm256_permutevar8x32_ps(_mm256_castpd_ps(table), index2));
#endif

#else // AVX
    // swap low and high part of table
    __m256d t1 = _mm256_castpd128_pd256(_mm256_extractf128_pd(table, 1));
    __m256d t2 = _mm256_insertf128_pd(t1, _mm256_castpd256_pd128(table), 1);
    // index << 1
    __m128i index2lo = index.get_low()  + index.get_low();
    __m128i index2hi = index.get_high() + index.get_high();
    // join index parts
    __m256i index3 = _mm256_insertf128_si256(_mm256_castsi128_si256(index2lo), index2hi, 1);
    // permute within each 128-bit part
    __m256d r0 = _mm256_permutevar_pd(table, index3);
    __m256d r1 = _mm256_permutevar_pd(t2,    index3);
    // high index bit for blend
    __m128i k1 = _mm_slli_epi64(index.get_high() ^ 2, 62);
    __m128i k0 = _mm_slli_epi64(index.get_low(),      62);
    __m256d kk = _mm256_insertf128_pd(_mm256_castpd128_pd256(_mm_castsi128_pd(k0)), _mm_castsi128_pd(k1), 1);
    // blend the two permutes
    return _mm256_blendv_pd(r0, r1, kk);
#endif
}

template <int n>
static inline Vec8d lookup(Vec8q const & index, double const * table) {
    if (n <= 0) return 0;
    if (n <= 2) {
        Vec2d table1 = Vec2d().load(table);
        return Vec4d(
            lookup2 (index.get_low(),  table1),
            lookup2 (index.get_high(), table1));
    }
#if INSTRSET < 8  // not AVX2
    if (n <= 4) {
        return lookup4(index, Vec4d().load(table));
    }
#endif
    // Limit index
    Vec8ui index1;
    if ((n & (n-1)) == 0) {
        // n is a power of 2, make index modulo n
        index1 = Vec8ui(index) & constant8i<n-1, 0, n-1, 0, n-1, 0, n-1, 0>();
    }
    else {
        // n is not a power of 2, limit to n-1
        index1 = min(Vec8ui(index), constant8i<n-1, 0, n-1, 0, n-1, 0, n-1, 0>() );
    }
#if INSTRSET >= 8 && VECTORI256_H > 1 // AVX2
    return _mm256_i64gather_pd(table, index1, 8);
#else // AVX
    Vec4q index2 = Vec4q(index1);
    return Vec4d(table[index2[0]],table[index2[1]],table[index2[2]],table[index2[3]]);
#endif
}
#endif  // VECTORI256_H

/*****************************************************************************
*
*          Gather functions with fixed indexes
*
*****************************************************************************/
// Load elements from array a with indices i0, i1, i2, i3, ..
template <int i0, int i1, int i2,  int i3,  int i4,  int i5,  int i6,  int i7,
          int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15>
static inline Vec16f gather16f(void const * a) {
    return reinterpret_f(gather16i<i0, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15>(a));
}

// Load elements from array a with indices i0, i1, i2, i3
template <int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7>
static inline Vec8d gather8d(void const * a) {
    assert(0);
    // TODO:
    //return reinterpret_d(gather4q<i0, i1, i2, i3>(a));
}


#endif // VECTORF256_H
