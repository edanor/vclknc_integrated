/****************************  vectori256.h   *******************************
* Author:        Przemyslaw Karpinski
* Date created:  2014-05-14
* Last modified: 2014-06-06
* Version:       0.01
* Project:       vector classes
* Description:
* Header file defining integer vector classes as interface to intrinsic 
* functions in x86 microprocessors with KNCNI (Intel Xeon Phi: Knights Landing) instruction sets.
*
* Instructions:
* Use Gnu, Intel or Microsoft C++ compiler. Compile for the desired 
* instruction set, which must be MIC. 
*
* The following vector classes are defined here:
* Vec512b   Vector of 512  1-bit unsigned  integers or Booleans
* Vec16i    Vector of  16  32-bit signed integers
* Vec16ui   Vector of  16  32-bit unsigned integers
* Vec16b    Vector of  16  Booleans for use with Vec16xxx types 
* Vec8q	    Vector of   8  64-bit signed integers
* Vec8uq	Vector of   8  64-bit unsigned integers
* Vec8b	    Vector of   8  Booleans for use with Vec8xxx types
*
* Each vector object is represented internally in the CPU as a 512-bit register.
* This header file defines operators and functions for these vectors.
*
* For example:
* Vec16i a(1,2,3,4,5,6,7,8, 9, 10, 11, 12, 13, 14, 15), b(16,17,18,19,20,21,22,23), c;
* c = a + b;     // now c contains (17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47)
*
* For detailed instructions, see VectorClass.pdf
*
* (c) Copyright 2014 GNU General Public License http://www.gnu.org/licenses
*
* This piece of code was developed as part of ICE-DIP project at CERN.
* �ICE-DIP is a European Industrial Doctorate project funded by the European Community�s 
* 7th Framework programme Marie Curie Actions under grant PITN-GA-2012-316596�.
*
*****************************************************************************/

// check combination of header files
#if defined (VECTORI512_H)
#if    VECTORI512_H != 2
#error Two different versions of vectori256.h included
#endif
#else
#define VECTORI512_H 2

#ifdef VECTORF512_H
#error Please put header file vectori256.h before vectorf256.h
#endif

#if INSTRSET != 10   // MIC required
#error Wrong instruction set for vectori512.h, MIC required or use vectori512e.h
#endif

#include "../instrset.h"

// TODO: remove this after clean-up
#include <assert.h>
#include <stdio.h>

/*****************************************************************************
*
*          Generate compile-time constant vector
*
*****************************************************************************/
// Generate a constant vector of 16 integers stored in memory.
// Can be converted to any integer vector type
template <int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15>
static inline __m512i constant16i() {
    __declspec(align(64)) static const union {
        int32_t i[16];
        __m512i zmm;
    } u = {{i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15}};
    return u.zmm;
}

// Generate a constant vector of 16 unsigned integers stored in memory.
// Can be converted to any integer vector type
template <int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15>
static inline __m512 constant16u() {
    __declspec(align(64)) static const union {
        uint32_t i[16];
        __m512 zmm;
    } u = {{i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15}};
    return u.zmm;
}

// Generate a constant vector of 8 quads stored in memory.
// Can be converted to any integer vector type
template <uint64_t i0, uint64_t i1, uint64_t i2, uint64_t i3, uint64_t i4, uint64_t i5, uint64_t i6, uint64_t i7>
static inline __m512i constant8q() {
    __declspec(align(64)) static const union {
        uint64_t i[8];
        __m512i zmm;
    } u = {{i0,i1,i2,i3,i4,i5,i6,i7}};
    return u.zmm;
}

// Generate a constant vector of 8 unsigned integers stored in memory.
// Can be converted to any integer vector type
template <uint64_t i0, uint64_t i1, uint64_t i2, uint64_t i3, uint64_t i4, uint64_t i5, uint64_t i6, uint64_t i7>
static inline __m512 constant8uq() {
    __declspec(align(64)) static const union {
        uint32_t i[8];
        __m512 zmm;
    } u = {{i0,i1,i2,i3,i4,i5,i6,i7}};
    return u.zmm;
}

/*****************************************************************************
*
*          Vector of 512 1-bit unsigned integers or Booleans
*
*****************************************************************************/
class Vec512b {
protected:
    __m512i zmm; // Integer vector
public:
    // Default constructor:
    Vec512b() {
        zmm = _mm512_setzero_epi32();
    };
    
    Vec512b(int i)
    {
        zmm = _mm512_set1_epi32(-(i & 1));
    }
    
    // Constructor to convert from type __m512i used in intrinsics:
    Vec512b(__m512i const & x) {
        zmm = x;
    };
    
    Vec512b(Vec512b const & a0)
    {
        zmm = a0;
    }

    // Assignment operator to convert from type __m512i used in intrinsics:
    Vec512b & operator = (__m512i const & x) {
        zmm = x;
        return *this;
    };

    // Type cast operator to convert to __m512i used in intrinsics
    operator __m512i() const {
        return zmm;
    }

    // Member function to load from array (unaligned)
    Vec512b & load(void const * p) {
        // TODO: protection from unaligned access
        zmm = _mm512_load_si512(p);
        return *this;
    }

    // You may use load_a instead of load if you are certain that p points to an address
    // divisible by 64, but there is hardly any speed advantage of load_a on modern processors
    Vec512b & load_a(void const * p) {
        zmm = _mm512_load_si512(p);
        return *this;
    }

    // Member function to store into array (unaligned)
    void store(void * p) const {
        // TODO: protection from unaligned access 
        // if((((uint64_t)p) & 0x3F) != 0)assert(0);
        _mm512_store_si512(p, zmm);
    }

    // You may use store_a instead of store if you are certain that p points to an address
    // divisible by 64, but there is hardly any speed advantage of load_a on modern processors
    void store_a(void * p) const {
        _mm512_store_si512(p, zmm);
    }

    // Member function to change a single bit
    // Note: This function is inefficient. Use load function if changing more than one bit
    Vec512b const & set_bit(uint32_t index, int value) {
        __m512i   t0;
        int       bm = 1 << (index & 0x1F);     // bit 
        __mmask16 qm = _mm512_int2mask(1 << ((index >> 5) & 0xF)); // qword index mask

        if(value & 1) {
            t0 = _mm512_set1_epi32(bm);
            zmm = _mm512_mask_or_epi32(zmm, qm, zmm, t0);
        }
        else
        {
            t0 = _mm512_set1_epi32(~bm);
            zmm = _mm512_mask_and_epi32(zmm, qm, zmm, t0);
        }
        return *this;
    }
    // Member function to get a single bit
    // Note: This function is inefficient. Use store function if reading more than one bit
    int get_bit(uint32_t index) const {
        union {
            __m512i x;
            uint8_t i[64];
        } u;
        u.x = zmm; 
        int wi = (index >> 3) & 0x3F;            // byte index
        int bi = index & 7;                      // bit index within byte w
        return (u.i[wi] >> bi) & 1;
    }

    // Extract a single element. Use store function if extracting more than one element.
    // Operator [] can only read an element, not write.
    int operator [] (uint32_t index) const {
        return get_bit(index);
    }
};


// Define operators for this class

// vector operator & : bitwise and
static inline Vec512b operator & (Vec512b const & a, Vec512b const & b) {
    return _mm512_and_si512(a, b);
}
static inline Vec512b operator && (Vec512b const & a, Vec512b const & b) {
    return a & b;
}

// vector operator | : bitwise or
static inline Vec512b operator | (Vec512b const & a, Vec512b const & b) {
    return _mm512_or_si512(a, b);
}
static inline Vec512b operator || (Vec512b const & a, Vec512b const & b) {
    return a | b;
}

// vector operator ^ : bitwise xor
static inline Vec512b operator ^ (Vec512b const & a, Vec512b const & b) {
    return Vec512b(_mm512_xor_epi32(a, b));
}

// vector operator ~ : bitwise not
static inline Vec512b operator ~ (Vec512b const & a) {
    return _mm512_xor_epi32(a, _mm512_set1_epi32(0xFFFFFFFF));
}

// vector operator &= : bitwise and
static inline Vec512b & operator &= (Vec512b & a, Vec512b const & b) {
    a = a & b;
    return a;
}

// vector operator |= : bitwise or
static inline Vec512b & operator |= (Vec512b & a, Vec512b const & b) {
    a = a | b;
    return a;
}

// vector operator ^= : bitwise xor
static inline Vec512b & operator ^= (Vec512b & a, Vec512b const & b) {
    a = a ^ b;
    return a;
}

// Define functions for this class
// function andnot: a & ~ b
static inline Vec512b andnot (Vec512b const & a, Vec512b const & b) {
    return _mm512_andnot_epi32(b, a);
}

/*****************************************************************************
*
*          Vec16b: A base class for Vector of 16 Booleans 
*
*****************************************************************************/
class Vec16b {
protected:
    __mmask16 m16; // Integer vector
public:
    // Default constructor:
    Vec16b() {
        m16 = _mm512_int2mask(0);
    };
    
    // Using _mmask16 as an argument would require explicit usage of _mm512_int2mask() when calling this constructor.
    Vec16b(uint16_t mask)
    {
        m16 = _mm512_int2mask(mask);
    }

    // Assignment operator to convert from type __mmask16 used in intrinsics:
    Vec16b & operator = (__mmask16 const & x)
    {
        m16 = x;
        return *this;
    }

    // Type cast operator to convert to __mmask16 used in intrinsics
    operator __mmask16() const {
        return m16;
    }

    // All other bool vectors should have the same interface (for the sake of templatization) 
    Vec16b & load(void const *p) {
        m16 = _mm512_int2mask( *((int *)p) );
        return *this;
    }

    // Member function to load from array (unaligned)
    Vec16b & load_a(void const *p) {
        m16 = *((uint16_t *)p);
        return *this;
    }
    
    // Keeping this for compatibility with other bool types.
    // There is no address sanity check
    void store(void * p) const {
        int *ptr = (int *)p;
        *ptr = _mm512_mask2int(m16);
    }
    
    // Keeping this for compatibility with other bool types.
    // There is no address sanity check
    void store_a(void * p) const {
        int *ptr = (int *)p;
        *ptr = _mm512_mask2int(m16);
    }
    
    // Member function to change a single bit
    // Note: This function is inefficient. Use load function if changing more than one bit
    Vec16b const & set_bit(uint32_t index, int value) {
        m16 = _mm512_kor(m16, _mm512_int2mask(value << index));
        return *this;
    }

    // Member function to get a single bit
    // Note: This function is inefficient. Use store function if reading more than one bit
    int get_bit(uint32_t index) const {
        return _mm512_mask2int(_mm512_kand(m16, _mm512_int2mask( 1 << index))) >> index;
    }
    // Extract a single element. Use store function if extracting more than one element.
    // Operator [] can only read an element, not write.
    int operator [] (uint32_t index) const {
        return get_bit(index);
    }
};

/*****************************************************************************
*
*          Operators for Vec16b
*
*****************************************************************************/
// vector operator & : bitwise and
static inline Vec16b operator & (Vec16b const & a, Vec16b const & b) {
    return _mm512_kand(a,b);
}

static inline Vec16b operator && (Vec16b const & a, Vec16b const & b) {
    return a & b;
}

// vector operator &= : bitwise and
static inline Vec16b & operator &= (Vec16b & a, Vec16b const & b) {
    a = a & b;
    return a;
}

// vector operator | : bitwise or
static inline Vec16b operator | (Vec16b const & a, Vec16b const & b) {
    return _mm512_kor(a,b);
}

static inline Vec16b operator || (Vec16b const & a, Vec16b const & b) {
    return a | b;
}

// vector operator |= : bitwise or
static inline Vec16b & operator |= (Vec16b & a, Vec16b const & b) {
    a = a | b;
    return a;
}

// vector operator ^ : bitwise xor
static inline Vec16b operator ^ (Vec16b const & a, Vec16b const & b) {
    return _mm512_kxor(a,b);
}

// vector operator ^= : bitwise xor
static inline Vec16b & operator ^= (Vec16b & a, Vec16b const & b) {
    a = a ^ b;
    return a;
}

// vector operator ~ : bitwise not
static inline Vec16b operator ~ (Vec16b const & a) {
    return _mm512_knot(a);
}

// vector operator ! : logical not
// (operator ! is less efficient than operator ~. Use only where not
// all bits in an element are the same)
static inline Vec16b operator ! (Vec16b const & a) {
    return _mm512_knot(a);
}

// Functions for Vec16b
// andnot: a & ~ b
static inline Vec16b andnot(Vec16b const & a, Vec16b const & b) {
    return _mm512_kandn(b, a);
}

/*****************************************************************************
*
*          Vec8b: A base class for Vector of 8 Booleans 
*
*****************************************************************************/
// Definition will be different for the AVX512 instruction set
class Vec8b {
protected:
    //__mmask8 m8; // Integer vector
public:
    __mmask8 m8; // Integer vector
    // Default constructor:
    Vec8b() {
        m8 = _mm512_int2mask(0x00);
    };
    
    Vec8b(int mask)
    {
        m8 = _mm512_int2mask(mask);
    }

    // Assignment operator to convert from type __mmask16 used in intrinsics:
    Vec8b & operator = (__mmask8 const & x)
    {
        m8 = x;
        return *this;
    }

    // Type cast operator to convert to __mmask16 used in intrinsics
    operator __mmask8() const {
        return m8;
    }

    // All other bool vectors should have the same interface (for the sake of templatization) 
    Vec8b & load(void const *p) {
        m8 = _mm512_int2mask( *((int *)p) );
        return *this;
    }

    // Member function to load from array (unaligned)
    Vec8b & load_a(void const *p) {
        m8 = *((uint8_t *)p);
        return *this;
    }
    
    // Keeping this for compatibility with other bool types.
    // There is no address sanity check
    void store(void * p) const {
        int *ptr = (int*)p;
        *ptr = _mm512_mask2int(m8);
    }
    
    // Keeping this for compatibility with other bool types.
    // There is no address sanity check
    void store_a(void * p) const {
        int *ptr = (int*)p;
        *ptr = _mm512_mask2int(m8);
    }
    
    // Member function to change a single bit
    // Note: This function is inefficient. Use load function if changing more than one bit
    Vec8b const & set_bit(uint32_t index, int value) {
        m8 = _mm512_kor(m8, _mm512_int2mask(value << index));
        return *this;
    }

    // Member function to get a single bit
    // Note: This function is inefficient. Use store function if reading more than one bit
    int get_bit(uint32_t index) const {
        return _mm512_mask2int(_mm512_kand(m8, _mm512_int2mask(1 << index))) >> index;
    }
    // Extract a single element. Use store function if extracting more than one element.
    // Operator [] can only read an element, not write.
    int operator [] (uint32_t index) const {
        return get_bit(index);
    }
};

/*****************************************************************************
*
*          Operators for Vec8b
*
*****************************************************************************/
// vector operator & : bitwise and
static inline Vec8b operator & (Vec8b const & a, Vec8b const & b) {
    return _mm512_kand(a,b);
}

static inline Vec8b operator && (Vec8b const & a, Vec8b const & b) {
    return a & b;
}

// vector operator &= : bitwise and
static inline Vec8b & operator &= (Vec8b & a, Vec8b const & b) {
    a = a & b;
    return a;
}

// vector operator | : bitwise or
static inline Vec8b operator | (Vec8b const & a, Vec8b const & b) {
    return _mm512_kor(a,b);
}

static inline Vec8b operator || (Vec8b const & a, Vec8b const & b) {
    return a | b;
}

// vector operator |= : bitwise or
static inline Vec8b & operator |= (Vec8b & a, Vec8b const & b) {
    a = a | b;
    return a;
}

// vector operator ^ : bitwise xor
static inline Vec8b operator ^ (Vec8b const & a, Vec8b const & b) {
    return _mm512_kxor(a,b);
}

// vector operator ^= : bitwise xor
static inline Vec8b & operator ^= (Vec8b & a, Vec8b const & b) {
    a = a ^ b;
    return a;
}

// vector operator ~ : bitwise not
static inline Vec8b operator ~ (Vec8b const & a) {
    return _mm512_knot(a);
}

// vector operator ! : logical not
// since the vector is now represented by binary mask type, use bitwise not
static inline Vec8b operator ! (Vec8b const & a) {
    return _mm512_knot(a);
}

// Functions for Vec16fb

// andnot: a & ~ b
static inline Vec8b andnot(Vec8b const & a, Vec8b const & b) {
    return _mm512_kandn(b, a);
}

/*****************************************************************************
*
*          selectb function
*
*****************************************************************************/
// Select between two sources, byte by byte. Used in various functions and operators
// Corresponds to this pseudocode:
// for (int i = 0; i < 64; i++) result[i] = s[i] ? a[i] : b[i];
// Each byte in s must be either 0 (false) or 0xFF (true). No other values are allowed.
// Only bit 7 in each byte of s is checked, 
static inline __m512i selectb (__m512i const & s, __m512i const & a, __m512i const & b) {
    assert(0);
    // TODO:
    // return _mm512_blendv_epi8 (b, a, s);
}

/*****************************************************************************
*
*          Horizontal Boolean functions
*
*****************************************************************************/

// horizontal_and. Returns true if all bits are 1
static inline bool horizontal_and (Vec16b const & a) {
    return _mm512_mask2int(a) == 0xFFFF;
}

// horizontal_and. Returns true if all bits are 1
static inline bool horizontal_and (Vec8b const & a) {
    return _mm512_mask2int(a) == 0xFF;
}

// horizontal_and. Returns true if all bits are 1
static inline bool horizontal_and (Vec512b const & a) {
    return _mm512_reduce_and_epi32(a) != 0;
}

// horizontal_or. Returns true if at least one bit is 1
static inline bool horizontal_or (Vec16b const & a) {
    return _mm512_mask2int(a) != 0;
}

// horizontal_or. Returns true if at least one bit is 1
static inline bool horizontal_or (Vec8b const & a) {
    return _mm512_mask2int(a) != 0;
}

// horizontal_or. Returns true if at least one bit is 1
static inline bool horizontal_or (Vec512b const & a) {
    return _mm512_reduce_or_epi32(a) != 0;
}

/*****************************************************************************
*
*          Vector of 16 32-bit signed integers
*
*****************************************************************************/

class Vec16i : public Vec512b {
public:
    // Default constructor:
    Vec16i() {
    };

    // Constructor to broadcast the same value into all elements:
    Vec16i(int i) {
        zmm = _mm512_set1_epi32(i);
    };

    // Constructor to build from all elements:
    Vec16i(int32_t i0, int32_t i1, int32_t i2,  int32_t i3,  int32_t i4,  int32_t i5,  int32_t i6,  int32_t i7,
           int32_t i8, int32_t i9, int32_t i10, int32_t i11, int32_t i12, int32_t i13, int32_t i14, int32_t i15)
    {
        zmm = _mm512_setr_epi32(i0, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15);
    };

    /* __mmask16 cannot be used in a constructor because it will casue constructor ambiguity with 'int i'...;
    Vec16i(__mmask16 mask)
    {
        zmm = _mm512_setr_epi32(
                   mask & 0x0001,      mask << 1 & 0x0002, mask << 1 & 0x0004, mask << 1 & 0x0008, 
                   mask << 1 & 0x0010, mask << 1 & 0x0020, mask << 1 & 0x0040, mask << 1 & 0x0080,
                   mask << 1 & 0x0100, mask << 1 & 0x0200, mask << 1 & 0x0400, mask << 1 & 0x0800,
                   mask << 1 & 0x1000, mask << 1 & 0x2000, mask << 1 & 0x4000, mask << 1 & 0x8000
            );
    }*/

    Vec16i(Vec512b const & a)
    {
        zmm = a;
    };

    // Constructor to convert from type __m512i used in intrinsics:
    // NOT SUPPORTED BY HW
    Vec16i(__m512i const & x) {
        zmm = x;
    };

    // Assignment operator to convert from type __m512i used in intrinsics:
    Vec16i & operator = (__m512i const & x) {
        zmm = x;
        return *this;
    };

    // Type cast operator to convert to __m512i used in intrinsics
    operator __m512i() const {
        return zmm;
    };

    // Member function to load from array (unaligned)
    Vec16i & load(void const * p) {
        // TODO: guard from unaligned access or remove this
        zmm = _mm512_load_epi32(p);
        return *this;
    }

    // Member function to load from array, aligned by 64
    Vec16i & load_a(void const * p) {
        zmm = _mm512_load_epi32(p);
        return *this;
    }

    // Partial load. Load n elements and set the rest to 0
    Vec16i & load_partial(int n, void const * p) {
        zmm = _mm512_mask_load_epi32(_mm512_setzero_epi32(), ((1 << n) - 1), p);
        return *this;
    }

    // Partial store. Store n elements
    void store_partial(int n, void * p) const {
        _mm512_mask_store_epi32(p, ((1 << n) - 1), zmm);
    }

    // cut off vector to n elements. The last 16-n elements are set to zero
    Vec16i & cutoff(int n) {
        zmm = _mm512_mask_mov_epi32(_mm512_setzero_epi32(), (((1 << n) - 1)), zmm);
        return *this;
    }

    // Member function to change a single element in vector
    // Note: This function is inefficient. Use load function if changing more than one element
    Vec16i const & insert(uint32_t index, int32_t value) {
        zmm = _mm512_mask_mov_epi32(zmm, 
                                 1 << index, 
                                 _mm512_set1_epi32(value));
        return *this;
    };

    // Member function extract a single element from vector
    int32_t extract(uint32_t index) const {
        __declspec(align(64)) int32_t x[16];
        store(x);
        return x[index & 15];
    }

    // Extract a single element. Use store function if extracting more than one element.
    // Operator [] can only read an element, not write.
    int32_t operator [] (uint32_t index) const {
        return extract(index);
    }
};

// Define operators for Vec16i

// vector operator + : add element by element
static inline Vec16i operator + (Vec16i const & a, Vec16i const & b) {
    return _mm512_add_epi32(a,b);
}

// vector operator += : add
static inline Vec16i & operator += (Vec16i & a, Vec16i const & b) {
    a = a + b;
    return a;
}

// postfix operator ++
static inline Vec16i operator ++ (Vec16i & a, int) {
    Vec16i a0 = a;
    a = a + 1;
    return a0;
}

// prefix operator ++
static inline Vec16i & operator ++ (Vec16i & a) {
    a = a + 1;
    return a;
}

// vector operator - : subtract element by element
static inline Vec16i operator - (Vec16i const & a, Vec16i const & b) {
    return _mm512_sub_epi32(a,b);
}

// vector operator - : unary minus
static inline Vec16i operator - (Vec16i const & a) {
    return _mm512_sub_epi32(_mm512_setzero_epi32(), a);
}

// vector operator -= : subtract
static inline Vec16i & operator -= (Vec16i & a, Vec16i const & b) {
    a = a - b;
    return a;
}

// postfix operator --
static inline Vec16i operator -- (Vec16i & a, int) {
    Vec16i a0 = a;
    a = a - 1;
    return a0;
}

// prefix operator --
static inline Vec16i & operator -- (Vec16i & a) {
    a = a - 1;
    return a;
}

// vector operator * : multiply element by element
static inline Vec16i operator * (Vec16i const & a, Vec16i const & b) {
    return _mm512_mullo_epi32(a, b);
}

// vector operator *= : multiply
static inline Vec16i & operator *= (Vec16i & a, Vec16i const & b) {
    a = a * b;
    return a;
}

// vector operator / : divide all elements by same integer
// See bottom of file

// vector operator << : shift left
static inline Vec16i operator << (Vec16i const & a, int32_t b) {
    return Vec16i(_mm512_sllv_epi32(a, _mm512_set1_epi32(b)));
}

// vector operator <<= : shift left
static inline Vec16i & operator <<= (Vec16i & a, int32_t b) {
    a = a << b;
    return a;
}

// vector operator >> : shift right arithmetic
static inline Vec16i operator >> (Vec16i const & a, int32_t b) {
    return _mm512_srav_epi32(a, _mm512_set1_epi32(b));
}

// vector operator >>= : shift right arithmetic
static inline Vec16i & operator >>= (Vec16i & a, int32_t b) {
    a = a >> b;
    return a;
}

// vector operator == : returns true for elements for which a == b
static inline Vec16b operator == (Vec16i const & a, Vec16i const & b) {
    return _mm512_cmp_epi32_mask(a, b, _MM_CMPINT_EQ);
}

// vector operator != : returns true for elements for which a != b
static inline Vec16b operator != (Vec16i const & a, Vec16i const & b) {
    return _mm512_cmp_epi32_mask(a, b, _MM_CMPINT_NE);
}
  
// vector operator > : returns true for elements for which a > b
static inline Vec16b operator > (Vec16i const & a, Vec16i const & b) {
    return _mm512_cmp_epi32_mask(a,b, _MM_CMPINT_GT);
}

// vector operator < : returns true for elements for which a < b
static inline Vec16b operator < (Vec16i const & a, Vec16i const & b) {
    return _mm512_cmp_epi32_mask(a,b, _MM_CMPINT_LT);
}

// vector operator >= : returns true for elements for which a >= b (signed)
static inline Vec16b operator >= (Vec16i const & a, Vec16i const & b) {
    return _mm512_cmp_epi32_mask(a,b, _MM_CMPINT_GE);
}

// vector operator <= : returns true for elements for which a <= b (signed)
static inline Vec16b operator <= (Vec16i const & a, Vec16i const & b) {
    return _mm512_cmp_epi32_mask(a,b, _MM_CMPINT_LE);
}

// vector operator & : bitwise and
static inline Vec16i operator & (Vec16i const & a, Vec16i const & b) {
    return Vec16i(Vec512b(a) & Vec512b(b));
}
static inline Vec16i operator && (Vec16i const & a, Vec16i const & b) {
    return a & b;
}
// vector operator &= : bitwise and
static inline Vec16i & operator &= (Vec16i & a, Vec16i const & b) {
    a = a & b;
    return a;
}

// vector operator | : bitwise or
static inline Vec16i operator | (Vec16i const & a, Vec16i const & b) {
    return Vec16i(Vec512b(a) | Vec512b(b));
}
static inline Vec16i operator || (Vec16i const & a, Vec16i const & b) {
    return a | b;
}
// vector operator |= : bitwise or
static inline Vec16i & operator |= (Vec16i & a, Vec16i const & b) {
    a = a | b;
    return a;
}

// vector operator ^ : bitwise xor
static inline Vec16i operator ^ (Vec16i const & a, Vec16i const & b) {
    return Vec16i(Vec512b(a) ^ Vec512b(b));
}
// vector operator ^= : bitwise xor
static inline Vec16i & operator ^= (Vec16i & a, Vec16i const & b) {
    a = a ^ b;
    return a;
}

// vector operator ~ : bitwise not
static inline Vec16i operator ~ (Vec16i const & a) {
    return Vec16i( ~ Vec512b(a));
}

// vector operator ! : returns true for elements == 0
static inline Vec16b operator ! (Vec16i const & a) { 
    return _mm512_cmp_epi32_mask(a,_mm512_setzero_epi32(), _MM_CMPINT_EQ);
}

// Functions for this class

// Select between two operands. Corresponds to this pseudocode:
// for (int i = 0; i < 8; i++) result[i] = s[i] ? a[i] : b[i];
// Each byte in s must be either 0 (false) or -1 (true). No other values are allowed.
// (s is signed)
static inline Vec16i select (Vec16b const & s, Vec16i const & a, Vec16i const & b) {
    // TODO:
    assert(0);
    return 0;
}

// Conditional add: For all vector elements i: result[i] = f[i] ? (a[i] + b[i]) : a[i]
static inline Vec16i if_add (Vec16b const & f, Vec16i const & a, Vec16i const & b) {
    return a + (Vec16i(f) & b);
}

// Horizontal add: Calculates the sum of all vector elements.
// Overflow will wrap around
static inline int32_t horizontal_add (Vec16i const & a) {
    return _mm512_reduce_add_epi32(a);
}

// Horizontal add extended: Calculates the sum of all vector elements.
// Elements are sign extended before adding to avoid overflow
static inline int64_t horizontal_add_x (Vec16i const & a) {
    // TODO: 64b Int's are only emulated on MIC. Find an efficient way to implement this operation
    assert(0);
//(#ifdef __XOP2__  // Possible future 256-bit XOP extension?)
    /*
    __m256i signs = _mm256_srai_epi32(a,31);                          // sign of all elements
    __m256i a01   = _mm256_unpacklo_epi32(a,signs);                   // sign-extended a0, a1, a4, a5
    __m256i a23   = _mm256_unpackhi_epi32(a,signs);                   // sign-extended a2, a3, a6, a7
    __m256i sum1  = _mm256_add_epi64(a01,a23);                        // add
    __m256i sum2  = _mm256_unpackhi_epi64(sum1,sum1);                 // odd qwords
    __m256i sum3  = _mm256_add_epi64(sum1,sum2);                      // add qwords
#if defined (_MSC_VER) && _MSC_VER <= 1700 && ! defined(__INTEL_COMPILER)
    __m128i sum4  = _mm256_extractf128_si256(sum3,1);                 // bug in MS compiler VS 11
#else
    __m128i sum4  = _mm256_extracti128_si256(sum3,1);                 // get high part
#endif
    __m128i sum5  = _mm_add_epi64(_mm256_castsi256_si128(sum3),sum4); // add low and high parts
#if defined (__x86_64__)
    return          _mm_cvtsi128_si64(sum5);                          // 64 bit mode
#else
    union {
        __m128i x;    // silly definition of _mm256_storel_epi64 requires __m128i
        int64_t i;
    } u;
    _mm_storel_epi64(&u.x,sum5);
    return u.i;
#endif*/
}

// function add_saturated: add element by element, signed with saturation
static inline Vec16i add_saturated(Vec16i const & a, Vec16i const & b) {
    __m512i sum    = _mm512_add_epi32(a, b);                  // a + b
    __m512i axb    = _mm512_xor_epi32(a, b);                  // check if a and b have different sign
    __m512i axs    = _mm512_xor_epi32(a, sum);                // check if a and sum have different sign
    __m512i overf1 = andnot(axb, axs);                        // check if sum has wrong sign
    __m512i overf2 = _mm512_srai_epi32(overf1, 31);           // -1 if overflow
    __m512i asign  = _mm512_srli_epi32(a, 31);                // 1  if a < 0
    __m512i sat1   = _mm512_srli_epi32(overf2, 1);            // 7FFFFFFF if overflow
    __m512i sat2   = _mm512_add_epi32(sat1, asign);           // 7FFFFFFF if positive overflow 80000000 if negative overflow
    return selectb(overf2, sat2, sum);                        // sum if not overflow, else sat2*/
}

// function sub_saturated: subtract element by element, signed with saturation
static inline Vec16i sub_saturated(Vec16i const & a, Vec16i const & b) {
    __m512i diff   = _mm512_sub_epi32(a, b);                  // a + b
    __m512i axb    = _mm512_xor_epi32(a, b);                  // check if a and b have different sign
    __m512i axs    = _mm512_xor_epi32(a, diff);               // check if a and sum have different sign
    __m512i overf1 = _mm512_and_epi32(axb,axs);               // check if sum has wrong sign
    __m512i overf2 = _mm512_srai_epi32(overf1,31);            // -1 if overflow
    __m512i asign  = _mm512_srli_epi32(a,31);                 // 1  if a < 0
    __m512i sat1   = _mm512_srli_epi32(overf2,1);             // 7FFFFFFF if overflow
    __m512i sat2   = _mm512_add_epi32(sat1,asign);            // 7FFFFFFF if positive overflow 80000000 if negative overflow
    return  selectb(overf2,sat2,diff);                        // diff if not overflow, else sat2 
}

// function max: a > b ? a : b
static inline Vec16i max(Vec16i const & a, Vec16i const & b) {
    return _mm512_max_epi32(a,b);
}

// function min: a < b ? a : b
static inline Vec16i min(Vec16i const & a, Vec16i const & b) {
    return _mm512_min_epi32(a, b);
}

// function abs: a >= 0 ? a : -a
static inline Vec16i abs(Vec16i const & a) {
    __m512i mask = constant16i<0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, \
                               0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF>();
    return _mm512_and_epi32(a, mask);
}

// function abs_saturated: same as abs, saturate if overflow
static inline Vec16i abs_saturated(Vec16i const & a) {
    __m512i absa   = abs(a);                        // abs(a)
    __m512i overfl = _mm512_srai_epi32(absa, 31);   // sign
    return _mm512_add_epi32(absa, overfl);          // subtract 1 if 0x80000000 
}

// function rotate_left all elements
// Use negative count to rotate right
static inline Vec16i rotate_left(Vec16i const & a, uint32_t b) {
    __m512i left  = _mm512_slli_epi32(a, (b & 0x1F));        // a << b
    __m512i right = _mm512_srli_epi32(a, ((32-b) & 0x1F));   // a >> (32 - b)
    __m512i rot   = _mm512_or_epi32(left, right);            // or
}

/*****************************************************************************
*
*          Vector of 16 32-bit unsigned integers
*
*****************************************************************************/
class Vec16ui : public Vec16i {
public:
    // Default constructor:
    Vec16ui() {
    };

    // Constructor to broadcast the same value into all elements:
    Vec16ui(uint32_t i) {
        zmm = _mm512_set1_epi32(i);
    };

    // Constructor to build from all elements:
    Vec16ui(uint32_t i0, uint32_t i1, uint32_t i2,  uint32_t i3,  uint32_t i4,  uint32_t i5,  uint32_t i6,  uint32_t i7,
            uint32_t i8, uint32_t i9, uint32_t i10, uint32_t i11, uint32_t i12, uint32_t i13, uint32_t i14, uint32_t i15) {
        zmm = _mm512_setr_epi32(i0, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15);
    };

    // Constructor to build from two Vec8ui:
    // UNSUPPORTED BY HW
    /*Vec16ui(Vec8ui const & a0, Vec8ui const & a1) {
        zmm = set_m128ir(a0, a1);
    }*/

    // Constructor to convert from type __m512i used in intrinsics:
    Vec16ui(__m512i const & x) {
        zmm = x;
    };

    // Assignment operator to convert from type __m256i used in intrinsics:
    Vec16ui & operator = (__m512i const & x) {
        zmm = x;
        return *this;
    };

    // Member function to load from array (unaligned)
    Vec16ui & load(void const * p) {
        zmm = _mm512_load_epi32(p);
        return *this;
    }

    // Member function to load from array, aligned by 32
    Vec16ui & load_a(void const * p) {
        // TODO: allignment check
        zmm = _mm512_load_epi32(p);
        return *this;
    }

    // Member function to change a single element in vector
    // Note: This function is inefficient. Use load function if changing more than one element
    Vec16ui const & insert(uint32_t index, uint32_t value) {
        zmm = _mm512_mask_mov_epi32(zmm, 
                                 1 << index, 
                                 _mm512_set1_epi32(value));
        return *this;
    }

    // Member function extract a single element from vector
    uint32_t extract(uint32_t index) const {
        __declspec(align(64)) uint32_t x[16];
        store(x);
        return x[index & 15];
    }

    // Extract a single element. Use store function if extracting more than one element.
    // Operator [] can only read an element, not write.
    uint32_t operator [] (uint32_t index) const {
        return extract(index);
    }
    // Member functions to split into two Vec4ui:
    // UNSUPPORTED BY HW
    /*Vec4ui get_low() const {
        return _mm256_castsi256_si128(zmm);
    }
    Vec4ui get_high() const {
        return _mm256_extractf128_si256(zmm,1);
    }*/
};

// Define operators for Vec16ui

// vector operator + : add
static inline Vec16ui operator + (Vec16ui const & a, Vec16ui const & b) {
    return _mm512_add_epi32(a,b);
}


// vector operator += : add
static inline Vec16ui & operator += (Vec16ui & a, Vec16ui const & b) {
    a = a + b;
    return a;
}

// postfix operator ++
static inline Vec16ui operator ++ (Vec16ui & a, int) {
    Vec16ui a0 = a;
    a = a + 1;
    return a0;
}

// prefix operator ++
static inline Vec16ui & operator ++ (Vec16ui & a) {
    a = a + 1;
    return a;
}

// vector operator - : subtract
static inline Vec16ui operator - (Vec16ui const & a, Vec16ui const & b) {
    return _mm512_sub_epi32(a,b);
}

// vector operator -= : subtract
static inline Vec16ui & operator -= (Vec16ui & a, Vec16ui const & b) {
    a = a - b;
    return a;
}

// postfix operator --
static inline Vec16ui operator -- (Vec16ui & a, int) {
    Vec16ui a0 = a;
    a = a - 1;
    return a0;
}

// prefix operator --
static inline Vec16ui & operator -- (Vec16ui & a) {
    a = a - 1;
    return a;
}

// vector operator * : multiply
static inline Vec16ui operator * (Vec16ui const & a, Vec16ui const & b) {
    return Vec16ui (Vec16i(a) * Vec16i(b));
}

// vector operator *= : multiply
static inline Vec16ui & operator *= (Vec16ui & a, Vec16ui const & b) {
    a = a * b;
    return a;
}

// vector operator / : divide
// See bottom of file

// vector operator << : shift left all elements
static inline Vec16ui operator << (Vec16ui const & a, int32_t b) {
    return Vec16ui(_mm512_sllv_epi32(a, _mm512_set1_epi32(b)));
}

// vector operator <<= : shift left
static inline Vec16ui & operator <<= (Vec16ui & a, int32_t b) {
    a = a << b;
    return a;
}

// vector operator >> : shift right logical all elements
static inline Vec16ui operator >> (Vec16ui const & a, int32_t b) {
    return _mm512_srav_epi32(a, _mm512_set1_epi32(b));
}

// vector operator >>= : shift right logical
static inline Vec16ui & operator >>= (Vec16ui & a, int32_t b) {
    a = a >> b;
    return a;
} 

// vector operator == : returns true for elements for which a == b (unsigned)
static inline Vec16b operator == (Vec16ui const & a, Vec16ui const & b) {
    return _mm512_cmp_epu32_mask(a, b, _MM_CMPINT_EQ);
}

// vector operator != : returns true for elements for which a != b (unsigned)
static inline Vec16b operator != (Vec16ui const & a, Vec16ui const & b) {
    return _mm512_cmp_epu32_mask(a, b, _MM_CMPINT_NE);
}

// vector operator > : returns true for elements for which a > b (unsigned)
static inline Vec16b operator > (Vec16ui const & a, Vec16ui const & b) {
    return _mm512_cmp_epu32_mask(a, b, _MM_CMPINT_GT);
}

// vector operator < : returns true for elements for which a < b (unsigned)
static inline Vec16b operator < (Vec16ui const & a, Vec16ui const & b) {
    return _mm512_cmp_epu32_mask(a, b, _MM_CMPINT_LT);
}

// vector operator >= : returns true for elements for which a >= b (unsigned)
static inline Vec16b operator >= (Vec16ui const & a, Vec16ui const & b) {
    return _mm512_cmp_epu32_mask(a, b, _MM_CMPINT_GE);
}

// vector operator <= : returns true for elements for which a <= b (unsigned)
static inline Vec16b operator <= (Vec16ui const & a, Vec16ui const & b) {
    return _mm512_cmp_epu32_mask(a, b, _MM_CMPINT_LE);
}

// vector operator & : bitwise and
static inline Vec16ui operator & (Vec16ui const & a, Vec16ui const & b) {
    return Vec16ui(Vec512b(a) & Vec512b(b));
}

static inline Vec16ui operator && (Vec16ui const & a, Vec16ui const & b) {
    return a & b;
}

// vector operator &= : bitwise and
static inline Vec16ui & operator &= (Vec16ui & a, Vec16ui const & b) {
    a = a & b;
    return a;
}

// vector operator | : bitwise or
static inline Vec16ui operator | (Vec16ui const & a, Vec16ui const & b) {
    return Vec16ui(Vec512b(a) | Vec512b(b));
}

static inline Vec16ui operator || (Vec16ui const & a, Vec16ui const & b) {
    return a | b;
}

// vector operator |= : bitwise or
static inline Vec16ui & operator |= (Vec16ui & a, Vec16ui const & b) {
    a = a | b;
    return a;
}

// vector operator ^ : bitwise xor
static inline Vec16ui operator ^ (Vec16ui const & a, Vec16ui const & b) {
    return Vec16ui(Vec512b(a) ^ Vec512b(b));
}

// vector operator ^= : bitwise xor
static inline Vec16ui & operator ^= (Vec16ui & a, Vec16ui const & b) {
    a = a ^ b;
    return a;
}

// vector operator ~ : bitwise not
static inline Vec16ui operator ~ (Vec16ui const & a) {
    return Vec16ui( ~ Vec512b(a));
}

// vector operator ! : returns true for elements == 0
static inline Vec16b operator ! (Vec16ui const & a) { 
    return _mm512_cmp_epu32_mask(a,_mm512_setzero_epi32(), _MM_CMPINT_EQ);
}

// Functions for this class

// Select between two operands. Corresponds to this pseudocode:
// for (int i = 0; i < 16; i++) result[i] = s[i] ? a[i] : b[i];
// Each word in s must be either 0 (false) or -1 (true). No other values are allowed.
// (s is signed)
static inline Vec16ui select (Vec16b const & s, Vec16ui const & a, Vec16ui const & b) {
    // TODO:
    assert(0);
    return 0;
}

// Conditional add: For all vector elements i: result[i] = f[i] ? (a[i] + b[i]) : a[i]
static inline Vec16ui if_add (Vec16b const & f, Vec16ui const & a, Vec16ui const & b) {
    return a + (Vec16ui(f) & b);
}

// Horizontal add: Calculates the sum of all vector elements.
// Overflow will wrap around
static inline uint32_t horizontal_add (Vec16ui const & a) {
    return horizontal_add((Vec16i)a);
}

// Horizontal add extended: Calculates the sum of all vector elements.
// Elements are zero extended before adding to avoid overflow
static inline uint64_t horizontal_add_x (Vec16ui const & a) {
    assert(0);
//#ifdef __XOP2__  // Possible future 256-bit XOP extension ?
/* TODO:
    __m256i zero  = _mm256_setzero_si256();                           // 0
    __m256i a01   = _mm256_unpacklo_epi32(a,zero);                    // zero-extended a0, a1
    __m256i a23   = _mm256_unpackhi_epi32(a,zero);                    // zero-extended a2, a3
    __m256i sum1  = _mm256_add_epi64(a01,a23);                        // add
    __m256i sum2  = _mm256_unpackhi_epi64(sum1,sum1);                 // high qword
    __m256i sum3  = _mm256_add_epi64(sum1,sum2);                      // add
#if defined (_MSC_VER) && _MSC_VER <= 1700 && ! defined(__INTEL_COMPILER)
    __m128i sum4  = _mm256_extractf128_si256(sum3, 1);                // bug in MS compiler VS 11
#else
    __m128i sum4  = _mm256_extracti128_si256(sum3, 1);                // get high part
#endif
    __m128i sum5  = _mm_add_epi64(_mm256_castsi256_si128(sum3),sum4); // add low and high parts
#ifdef __x86_64__
    return          _mm_cvtsi128_si64(sum5);                          // 64 bit mode
#else
    union {
        __m128i x;  // silly definition of _mm256_storel_epi64 requires __m256i
        uint64_t i;
    } u;
    _mm_storel_epi64(&u.x,sum5);
    return u.i;
#endif
    */
}

// function add_saturated: add element by element, unsigned with saturation
static inline Vec16ui add_saturated(Vec16ui const & a, Vec16ui const & b) {
    Vec16ui sum      = a + b;
    Vec16ui aorb     = Vec16ui(a | b);
    Vec16ui overflow = Vec16ui(sum < aorb); // overflow if a + b < (a | b)
    return Vec16ui (sum | overflow);         // return 0xFFFFFFFF if overflow
}

// function sub_saturated: subtract element by element, unsigned with saturation
static inline Vec16ui sub_saturated(Vec16ui const & a, Vec16ui const & b) {
    Vec16ui diff      = a - b;
    Vec16ui underflow = Vec16ui(diff > a);    // underflow if a - b > a
    return Vec16ui(andnot(underflow,diff));            // return 0 if underflow
}

// function max: a > b ? a : b
static inline Vec16ui max(Vec16ui const & a, Vec16ui const & b) {
    return _mm512_max_epu32(a, b);
}

// function min: a < b ? a : b
static inline Vec16ui min(Vec16ui const & a, Vec16ui const & b) {
    return _mm512_min_epu32(a, b);
}

/*****************************************************************************
*
*          Vector of 8 64-bit signed integers
*
*****************************************************************************/

class Vec8q : public Vec512b {
public:
    // Default constructor:
    Vec8q() {
    };

    // Constructor to broadcast the same value into all elements:
    Vec8q(uint64_t i) {
        zmm = _mm512_set1_epi64(i);
    };

    // Constructor to build from all elements:
    Vec8q(int64_t i0, int64_t i1, int64_t i2,  int64_t i3,  int64_t i4,  int64_t i5,  int64_t i6,  int64_t i7)
    {
        // TODO: verify that _mm512_setr_epi64 and _mm512_set_epi64 work correctly
        zmm = _mm512_setr_epi64(i0, i1, i2, i3, i4, i5, i6, i7);
    };

    Vec8q(Vec512b const & a)
    {
        zmm = a;
    };

    // Constructor to convert from type _m512i used in intrinsics:
    // NOT SUPPORTED BY HW
    Vec8q(__m512i const & x) {
        zmm = x;
    };

    // Assignment operator to convert from type _m512i used in intrinsics:
    Vec8q & operator = (__m512i const & x) {
        zmm = x;
        return *this;
    };

    // Type cast operator to convert to _m512i used in intrinsics
    operator __m512i() const {
        return zmm;
    };

    // Member function to load from array (unaligned)
    Vec8q & load(void const * p) {
        // TODO: allignment check
        zmm = _mm512_load_si512(p);
        return *this;
    }

    // Member function to load from array, aligned by 64
    Vec8q & load_a(void const * p) {
        zmm = _mm512_load_si512(p);
        return *this;
    }
    
    // Partial load. Load n elements and set the rest to 0
    Vec8q & load_partial(int n, void const * p) {
        __m512i t0 = constant8q<0,0,0,0, 0,0,0,0>();
        zmm = _mm512_mask_load_epi64(t0, 
                                  (1 << n)-1,
                                  p);
        return *this;
    }

    // Partial store. Store n elements
    void store_partial(int64_t n, void * p) const {
        _mm512_mask_store_epi64(p, 
                             (1 << n) - 1, 
                             zmm);
    }

    // cut off vector to n elements. The last 8-n elements are set to zero
    Vec8q & cutoff(int n) {
        zmm = _mm512_mask_mov_epi64( _mm512_setzero_epi32(), _mm512_int2mask((1 << n) - 1), zmm);
        return *this;
    }

    // Member function to change a single element in vector
    // Note: This function is inefficient. Use load function if changing more than one element
    Vec8q const & insert(uint32_t index, int64_t value) {
        zmm = _mm512_mask_mov_epi64(zmm, 
                                 1 << index, 
                                 _mm512_set1_epi64(value));
        return *this;
    };

    // Member function extract a single element from vector
    int64_t extract(uint32_t index) const {
        __declspec(align(64)) int64_t x[8];
        store(x);
        return x[index & 7];
    }

    // Extract a single element. Use store function if extracting more than one element.
    // Operator [] can only read an element, not write.
    int64_t operator [] (uint32_t index) const {
        return extract(index);
    }
};

// Define operators for Vec8q

// vector operator + : add element by element
static inline Vec8q operator + (Vec8q const & a, Vec8q const & b) {
    return _mm512_add_epi64(a,b);
}

// vector operator += : add
static inline Vec8q & operator += (Vec8q & a, Vec8q const & b) {
    a = a + b;
    return a;
}

// postfix operator ++
static inline Vec8q operator ++ (Vec8q & a, int) {
    Vec8q a0 = a;
    a = a + 1;
    return a0;
}

// prefix operator ++
static inline Vec8q & operator ++ (Vec8q & a) {
    a = a + 1;
    return a;
}

// vector operator - : subtract element by element
static inline Vec8q operator - (Vec8q const & a, Vec8q const & b) {
    return _mm512_sub_epi64(a, b);
}

// vector operator - : unary minus
static inline Vec8q operator - (Vec8q const & a) {
    return _mm512_sub_epi64(_mm512_setzero_epi32(), a);
}

// vector operator -= : subtract
static inline Vec8q & operator -= (Vec8q & a, Vec8q const & b) {
    a = a - b;
    return a;
}

// postfix operator --
static inline Vec8q operator -- (Vec8q & a, int) {
    Vec8q a0 = a;
    a = a - 1;
    return a0;
}

// prefix operator --
static inline Vec8q & operator -- (Vec8q & a) {
    a = a - 1;
    return a;
}

// vector operator * : multiply element by element
static inline Vec8q operator * (Vec8q const & a, Vec8q const & b) {
    // TODO: 64b int operations have to be emulated
   /*// instruction does not exist. Split into 32-bit multiplies
    __m256i bswap   = _mm256_shuffle_epi32(b,0xB1);           // swap H<->L
    __m256i prodlh  = _mm256_mullo_epi32(a,bswap);            // 32 bit L*H products
    __m256i zero    = _mm256_setzero_si256();                 // 0
    __m256i prodlh2 = _mm256_hadd_epi32(prodlh,zero);         // a0Lb0H+a0Hb0L,a1Lb1H+a1Hb1L,0,0
    __m256i prodlh3 = _mm256_shuffle_epi32(prodlh2,0x73);     // 0, a0Lb0H+a0Hb0L, 0, a1Lb1H+a1Hb1L
    __m256i prodll  = _mm256_mul_epu32(a,b);                  // a0Lb0L,a1Lb1L, 64 bit unsigned products
    __m256i prod    = _mm256_add_epi64(prodll,prodlh3);       // a0Lb0L+(a0Lb0H+a0Hb0L)<<32, a1Lb1L+(a1Lb1H+a1Hb1L)<<32
    */
    // a: {a0L, a0H, a1L, a1H}_0 {a0L, a0H, a1L, a1H}_1 {a0L, a0H, a1L, a1H}_2 {a0L, a0H, a1L, a1H}_3
    // b: {b0L, b0H, b1L, b1H}_0 {b0L, b0H, b1L, b1H}_1 {b0L, b0H, b1L, b1H}_2 {b0L, b0H, b1L, b1H}_3

    // the same operation is performed on 32b elements ABCD of a 128b group
    __m512i bswap   = _mm512_shuffle_epi32(b, _MM_PERM_ABCD); // b0L,       b0H,        b1L,    b1H
    __m512i prodlh  = _mm512_mullo_epi32(a, bswap);           // 0,   a0L*b0H,           0,     a1L*b1H
    //__m512i prodlh3 = _mm512_shuffle_epi32(prodlh2, 



    return  0;
}

// vector operator *= : multiply
static inline Vec8q & operator *= (Vec8q & a, Vec8q const & b) {
    a = a * b;
    return a;
}

// vector operator / : divide all elements by same integer
// See bottom of file

// vector operator << : shift left
static inline Vec8q operator << (Vec8q const & a, int32_t b) {
    // TODO: verify that this is correct
    Vec16i t0 = _mm512_set1_epi32(b);        // Create shift vector
   /* I developed this part of the code as a WA for lack of _mm512_sllv_epi64() intrinsic. It turns out that its only missing in documentation.
    TODO: remove this whole commented area
    Vec16i t1 = Vec16i(a);

    Vec16i t2 = _mm512_srlv_epi32(t1, t0);   // shift all elements right - this will create masks out of higher bits of odd 
    Vec16i t3 = _mm512_sllv_epi32(t1, t0);   // shift all elements of the original vector 
    Vec16i t4 = _mm512_swizzle_epi32(t2, _MM_SWIZ_REG_CDAB); // This swizzle mask swaps pairs of elements: dcba -> cdab.
    Vec16i t5 = _mm512_mask_or_epi32(t3,     // this and mask will result in lower 32b of each number being copied without masking
                                     0xAAAA, // mask that covers only odd elements of the vector (higher 32b of quad)
                                     t3,     // original shifted vector
                                     t4); 
                                     
    return Vec8q(t5); // cast resultant vector back to quad
                                     */

    return _mm512_sllv_epi64(a, t0);
}

// vector operator <<= : shift left
static inline Vec8q & operator <<= (Vec8q & a, int32_t b) {
    a = a << b;
    return a;
}

// vector operator >> : shift right arithmetic
static inline Vec8q operator >> (Vec8q const & a, int32_t b) {
    return _mm512_srav_epi64(a, _mm512_set1_epi64(b));
}

// vector operator >>= : shift right arithmetic
static inline Vec8q & operator >>= (Vec8q & a, int32_t b) {
    a = a >> b;
    return a;
}

// vector operator == : returns true for elements for which a == b
static inline Vec8b operator == (Vec8q const & a, Vec8q const & b) {
    return _mm512_cmp_epi64_mask(a, b, _MM_CMPINT_EQ);
}

// vector operator != : returns true for elements for which a != b
static inline Vec8b operator != (Vec8q const & a, Vec8q const & b) {
    return _mm512_cmp_epi64_mask(a, b, _MM_CMPINT_NE);
}
  
// vector operator > : returns true for elements for which a > b
static inline Vec8b operator > (Vec8q const & a, Vec8q const & b) {
    return _mm512_cmp_epi64_mask(a, b, _MM_CMPINT_GT);
}

// vector operator < : returns true for elements for which a < b
static inline Vec8b operator < (Vec8q const & a, Vec8q const & b) {
    return _mm512_cmp_epi64_mask(a, b, _MM_CMPINT_LT);
}

// vector operator >= : returns true for elements for which a >= b (signed)
static inline Vec8b operator >= (Vec8q const & a, Vec8q const & b) {
    return _mm512_cmp_epi64_mask(a, b, _MM_CMPINT_GE);
}

// vector operator <= : returns true for elements for which a <= b (signed)
static inline Vec8b operator <= (Vec8q const & a, Vec8q const & b) {
    return _mm512_cmp_epi64_mask(a, b, _MM_CMPINT_LE);
}

// vector operator & : bitwise and
static inline Vec8q operator & (Vec8q const & a, Vec8q const & b) {
    return Vec8q(Vec512b(a) & Vec512b(b));
}
static inline Vec8q operator && (Vec8q const & a, Vec8q const & b) {
    return a & b;
}
// vector operator &= : bitwise and
static inline Vec8q & operator &= (Vec8q & a, Vec8q const & b) {
    a = a & b;
    return a;
}

// vector operator | : bitwise or
static inline Vec8q operator | (Vec8q const & a, Vec8q const & b) {
    return Vec8q(Vec512b(a) | Vec512b(b));
}
static inline Vec8q operator || (Vec8q const & a, Vec8q const & b) {
    return a | b;
}
// vector operator |= : bitwise or
static inline Vec8q & operator |= (Vec8q & a, Vec8q const & b) {
    a = a | b;
    return a;
}

// vector operator ^ : bitwise xor
static inline Vec8q operator ^ (Vec8q const & a, Vec8q const & b) {
    return Vec8q(Vec512b(a) ^ Vec512b(b));
}
// vector operator ^= : bitwise xor
static inline Vec8q & operator ^= (Vec8q & a, Vec8q const & b) {
    a = a ^ b;
    return a;
}

// vector operator ~ : bitwise not
static inline Vec8q operator ~ (Vec8q const & a) {
    return Vec8q( ~ Vec512b(a));
}

// vector operator ! : returns true for elements == 0
static inline Vec8b operator ! (Vec8q const & a) {
    return _mm512_cmp_epi64_mask(a, _mm512_setzero_epi32(), _MM_CMPINT_EQ);
}

// Functions for this class

// Select between two operands. Corresponds to this pseudocode:
// for (int i = 0; i < 8; i++) result[i] = s[i] ? a[i] : b[i];
// Each byte in s must be either 0 (false) or -1 (true). No other values are allowed.
// (s is signed)
static inline Vec8q select (Vec8b const & s, Vec8q const & a, Vec8q const & b) {
    assert(0);
    return 0;
    //return selectb(s,a,b);
}

// Conditional add: For all vector elements i: result[i] = f[i] ? (a[i] + b[i]) : a[i]
static inline Vec8q if_add (Vec8b const & f, Vec8q const & a, Vec8q const & b) {
    return a + (Vec8q(f) & b);
}

// Horizontal add: Calculates the sum of all vector elements.
// Overflow will wrap around
static inline int64_t horizontal_add (Vec8q const & a) {
    return _mm512_reduce_add_epi64(a);
}

// Horizontal add extended: Calculates the sum of all vector elements.
// Elements are sign extended before adding to avoid overflow
static inline int64_t horizontal_add_x (Vec8q const & a) {
    // TODO:
    assert(0);
//(#ifdef __XOP2__  // Possible future 256-bit XOP extension?)
    /*
    __m256i signs = _mm256_srai_epi32(a,31);                          // sign of all elements
    __m256i a01   = _mm256_unpacklo_epi32(a,signs);                   // sign-extended a0, a1, a4, a5
    __m256i a23   = _mm256_unpackhi_epi32(a,signs);                   // sign-extended a2, a3, a6, a7
    __m256i sum1  = _mm256_add_epi64(a01,a23);                        // add
    __m256i sum2  = _mm256_unpackhi_epi64(sum1,sum1);                 // odd qwords
    __m256i sum3  = _mm256_add_epi64(sum1,sum2);                      // add qwords
#if defined (_MSC_VER) && _MSC_VER <= 1700 && ! defined(__INTEL_COMPILER)
    __m128i sum4  = _mm256_extractf128_si256(sum3,1);                 // bug in MS compiler VS 11
#else
    __m128i sum4  = _mm256_extracti128_si256(sum3,1);                 // get high part
#endif
    __m128i sum5  = _mm_add_epi64(_mm256_castsi256_si128(sum3),sum4); // add low and high parts
#if defined (__x86_64__)
    return          _mm_cvtsi128_si64(sum5);                          // 64 bit mode
#else
    union {
        __m128i x;    // silly definition of _mm256_storel_epi64 requires __m128i
        int64_t i;
    } u;
    _mm_storel_epi64(&u.x,sum5);
    return u.i;
#endif*/
}

// function add_saturated: add element by element, signed with saturation
static inline Vec8q add_saturated(Vec8q const & a, Vec8q const & b) {
    assert(0);
    /*
    __m256i sum    = _mm256_add_epi32(a, b);                  // a + b
    __m256i axb    = _mm256_xor_si256(a, b);                  // check if a and b have different sign
    __m256i axs    = _mm256_xor_si256(a, sum);                // check if a and sum have different sign
    __m256i overf1 = _mm256_andnot_si256(axb,axs);            // check if sum has wrong sign    
    __m256i overf2 = _mm256_srai_epi32(overf1,31);            // -1 if overflow
    __m256i asign  = _mm256_srli_epi32(a,31);                 // 1  if a < 0
    __m256i sat1   = _mm256_srli_epi32(overf2,1);             // 7FFFFFFF if overflow
    __m256i sat2   = _mm256_add_epi32(sat1,asign);            // 7FFFFFFF if positive overflow 80000000 if negative overflow
    return  selectb(overf2,sat2,sum);                         // sum if not overflow, else sat2*/
}

// function sub_saturated: subtract element by element, signed with saturation
static inline Vec8q sub_saturated(Vec8q const & a, Vec8q const & b) {
    assert(0);
    /* TODO:
    __m256i diff   = _mm256_sub_epi32(a, b);                  // a + b
    __m256i axb    = _mm256_xor_si256(a, b);                  // check if a and b have different sign
    __m256i axs    = _mm256_xor_si256(a, diff);               // check if a and sum have different sign
    __m256i overf1 = _mm256_and_si256(axb,axs);               // check if sum has wrong sign
    __m256i overf2 = _mm256_srai_epi32(overf1,31);            // -1 if overflow
    __m256i asign  = _mm256_srli_epi32(a,31);                 // 1  if a < 0
    __m256i sat1   = _mm256_srli_epi32(overf2,1);             // 7FFFFFFF if overflow
    __m256i sat2   = _mm256_add_epi32(sat1,asign);            // 7FFFFFFF if positive overflow 80000000 if negative overflow
    return  selectb(overf2,sat2,diff);                        // diff if not overflow, else sat2 */
}

// function max: a > b ? a : b
static inline Vec8q max(Vec8q const & a, Vec8q const & b) {
    return _mm512_max_epi64(a, b);
}

// function min: a < b ? a : b
static inline Vec8q min(Vec8q const & a, Vec8q const & b) {
    return _mm512_min_epi64(a, b);
}

// function abs: a >= 0 ? a : -a
static inline Vec8q abs(Vec8q const & a) {
    __m512i mask = constant16i<0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 
                               0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF>();
    return _mm512_and_epi32(a, mask);
}

// function abs_saturated: same as abs, saturate if overflow
static inline Vec8q abs_saturated(Vec8q const & a) {
    assert(0);
    return 0;
/* TODO:
    __m256i absa   = abs(a);                                  // abs(a)
    __m256i overfl = _mm256_srai_epi32(absa,31);              // sign
    return           _mm256_add_epi32(absa,overfl);           // subtract 1 if 0x80000000 
    */
}
    
// function rotate_left all elements
// Use negative count to rotate right
static inline Vec8q rotate_left(Vec8q const & a, uint32_t b) {
    assert(0);
/* #ifdef __XOP2__  // Possible future 256-bit XOP extension ?
    return _mm256_rot_epi32(a,_mm_set1_epi32(b));
#else  // SSE2 instruction set
    __m256i left  = _mm256_sll_epi32(a,_mm_cvtsi32_si128(b & 0x1F));      // a << b 
    __m256i right = _mm256_srl_epi32(a,_mm_cvtsi32_si128((32-b) & 0x1F)); // a >> (32 - b)
    __m256i rot   = _mm256_or_si256(left,right);                          // or
    return  rot;
#endif*/
}

/*****************************************************************************
*
*          Vector of 8 64-bit unsigned integers
*
*****************************************************************************/

class Vec8uq : public Vec8q {
public:
    // Default constructor:
    Vec8uq() {
    };

    // Constructor to broadcast the same value into all elements:
    Vec8uq(uint64_t i) {
        zmm = _mm512_set1_epi64(i);
    };

    // Constructor to build from all elements:
    Vec8uq(uint64_t i0, uint64_t i1, uint64_t i2, uint64_t i3, uint64_t i4, uint64_t i5, uint64_t i6, uint64_t i7) {
        zmm = _mm512_setr_epi64(i0, i1, i2, i3, i4, i5, i6, i7);
    };
    // Constructor to build from two Vec4ui:
    // NOT SUPPORTED BY HW
    /*Vec16ui(Vec8ui const & a0, Vec8ui const & a1) {
        zmm = setr_m128ir(a0, a1);
    }*/
    // Constructor to convert from type __m512i used in intrinsics:
    Vec8uq(__m512i const & x) {
        zmm = x;
    };

    // Assignment operator to convert from type __m512i used in intrinsics:
    Vec8uq & operator = (__m512i const & x) {
        zmm = x;
        return *this;
    };

    // Member function to load from array (unaligned)
    Vec8uq & load(void const * p) {
        // TODO: allignment check
        zmm = _mm512_load_epi64(p);
        return *this;
    }

    // Member function to load from array, aligned by 64
    Vec8uq & load_a(void const * p) {
        zmm = _mm512_load_epi64(p);
        return *this;
    }

    // Member function to change a single element in vector
    // Note: This function is inefficient. Use load function if changing more than one element
    Vec8uq const & insert(uint32_t index, uint64_t value) {
        Vec8uq::insert(index, value);
        return *this;
    }

    // Member function extract a single element from vector
    uint32_t extract(uint32_t index) const {
        return Vec8q::extract(index);
    }

    // Extract a single element. Use store function if extracting more than one element.
    // Operator [] can only read an element, not write.
    uint32_t operator [] (uint32_t index) const {
        return extract(index);
    }
};

// Define operators for this class

// vector operator + : add
static inline Vec8uq operator + (Vec8uq const & a, Vec8uq const & b) {
    return Vec8uq(Vec8q(a) + Vec8q(b));
}

// vector operator - : subtract
static inline Vec8uq operator - (Vec8uq const & a, Vec8uq const & b) {
    return Vec8uq (Vec8q(a) - Vec8q(b));
}

// vector operator * : multiply
static inline Vec8uq operator * (Vec8uq const & a, Vec8uq const & b) {
    return Vec8uq (Vec8q(a) * Vec8q(b));
}

// vector operator / : divide
// See bottom of file

// vector operator >> : shift right logical all elements
static inline Vec8uq operator >> (Vec8uq const & a, uint32_t b) {
    return Vec8uq(_mm512_srli_epi64(a,b));
}

// vector operator >> : shift right logical all elements
static inline Vec8uq operator >> (Vec8uq const & a, int32_t b) {
    return a >> (uint32_t)b;
}

// vector operator >>= : shift right logical
static inline Vec8uq & operator >>= (Vec8uq & a, uint32_t b) {
    a = a >> b;
    return a;
} 

// vector operator << : shift left all elements
static inline Vec8uq operator << (Vec8uq const & a, uint32_t b) {
    return Vec8uq ((Vec8q)a << (int32_t)b);
}

// vector operator << : shift left all elements
static inline Vec8uq operator << (Vec8uq const & a, int32_t b) {
    return Vec8uq ((Vec8q)a << (int32_t)b);
}

// vector operator > : returns true for elements for which a > b (unsigned)
static inline Vec8b operator > (Vec8uq const & a, Vec8uq const & b) {
    return _mm512_cmp_epu64_mask(a, b, _MM_CMPINT_GT);
}

// vector operator < : returns true for elements for which a < b (unsigned)
static inline Vec8b operator < (Vec8uq const & a, Vec8uq const & b) {
    return _mm512_cmp_epu64_mask(a, b, _MM_CMPINT_LT);
}

// vector operator >= : returns true for elements for which a >= b (unsigned)
static inline Vec8b operator >= (Vec8uq const & a, Vec8uq const & b) {
    return _mm512_cmp_epu64_mask(a, b, _MM_CMPINT_GT);
}

// vector operator <= : returns true for elements for which a <= b (unsigned)
static inline Vec8b operator <= (Vec8uq const & a, Vec8uq const & b) {
    return _mm512_cmp_epu64_mask(a, b, _MM_CMPINT_GE);
}

// vector operator & : bitwise and
static inline Vec8uq operator & (Vec8uq const & a, Vec8uq const & b) {
    return Vec8uq(Vec512b(a) & Vec512b(b));
}
static inline Vec8uq operator && (Vec8uq const & a, Vec8uq const & b) {
    return a & b;
}

// vector operator | : bitwise or
static inline Vec8uq operator | (Vec8uq const & a, Vec8uq const & b) {
    return Vec8uq(Vec512b(a) | Vec512b(b));
}

static inline Vec8uq operator || (Vec8uq const & a, Vec8uq const & b) {
    return a | b;
}

// vector operator ^ : bitwise xor
static inline Vec8uq operator ^ (Vec8uq const & a, Vec8uq const & b) {
    return Vec8uq(Vec512b(a) ^ Vec512b(b));
}

// vector operator ~ : bitwise not
static inline Vec8uq operator ~ (Vec8uq const & a) {
    return Vec8uq( ~ Vec512b(a));
}

// Functions for this class

// Select between two operands. Corresponds to this pseudocode:
// for (int i = 0; i < 16; i++) result[i] = s[i] ? a[i] : b[i];
// Each word in s must be either 0 (false) or -1 (true). No other values are allowed.
// (s is signed)
static inline Vec8uq select (Vec8b const & s, Vec8uq const & a, Vec8uq const & b) {
    // TODO:
    assert(0);
    //return selectb(s,a,b);
    return 0;
}

// Conditional add: For all vector elements i: result[i] = f[i] ? (a[i] + b[i]) : a[i]
static inline Vec8uq if_add (Vec8b const & f, Vec8uq const & a, Vec8uq const & b) {
    return a + (Vec8uq(f) & b);
}

// Horizontal add: Calculates the sum of all vector elements.
// Overflow will wrap around
static inline uint64_t horizontal_add (Vec8uq const & a) {
    return horizontal_add((Vec8q)a);
}

// Horizontal add extended: Calculates the sum of all vector elements.
// Elements are zero extended before adding to avoid overflow
static inline uint64_t horizontal_add_x (Vec8uq const & a) {
    // TODO:
    assert(0);
//#ifdef __XOP2__  // Possible future 256-bit XOP extension ?
/*    __m256i zero  = _mm256_setzero_si256();                           // 0
    __m256i a01   = _mm256_unpacklo_epi32(a,zero);                    // zero-extended a0, a1
    __m256i a23   = _mm256_unpackhi_epi32(a,zero);                    // zero-extended a2, a3
    __m256i sum1  = _mm256_add_epi64(a01,a23);                        // add
    __m256i sum2  = _mm256_unpackhi_epi64(sum1,sum1);                 // high qword
    __m256i sum3  = _mm256_add_epi64(sum1,sum2);                      // add
#if defined (_MSC_VER) && _MSC_VER <= 1700 && ! defined(__INTEL_COMPILER)
    __m128i sum4  = _mm256_extractf128_si256(sum3, 1);                // bug in MS compiler VS 11
#else
    __m128i sum4  = _mm256_extracti128_si256(sum3, 1);                // get high part
#endif
    __m128i sum5  = _mm_add_epi64(_mm256_castsi256_si128(sum3),sum4); // add low and high parts
#ifdef __x86_64__
    return          _mm_cvtsi128_si64(sum5);                          // 64 bit mode
#else
    union {
        __m128i x;  // silly definition of _mm256_storel_epi64 requires __m256i
        uint64_t i;
    } u;
    _mm_storel_epi64(&u.x,sum5);
    return u.i;
#endif
    */
}

// function add_saturated: add element by element, unsigned with saturation
static inline Vec8uq add_saturated(Vec8uq const & a, Vec8uq const & b) {
    Vec8uq sum      = a + b;
    Vec8uq aorb     = Vec8uq(a | b);
    Vec8uq overflow = Vec8uq(sum < aorb);   // overflow if a + b < (a | b)
    return Vec8uq (sum | overflow);         // return 0xFFFFFFFF if overflow
}

// function sub_saturated: subtract element by element, unsigned with saturation
static inline Vec8uq sub_saturated(Vec8uq const & a, Vec8uq const & b) {
    assert(0);
   /* TODO:
    Vec16ui diff      = a - b;
    Vec16ui underflow = Vec16ui(diff > a);                   // underflow if a - b > a
    return _mm256_andnot_si256(underflow,diff);            // return 0 if underflow*/
}

// function max: a > b ? a : b
static inline Vec8uq max(Vec8uq const & a, Vec8uq const & b) {
    return _mm512_max_epu64(a, b);
}

// function min: a < b ? a : b
static inline Vec8uq min(Vec8uq const & a, Vec8uq const & b) {
    return _mm512_min_epu64(a, b);
}


/*****************************************************************************
*
*          Vector permute functions
*
******************************************************************************
*
* These permute functions can reorder the elements of a vector and optionally
* set some elements to zero. 
*
* The indexes are inserted as template parameters in <>. These indexes must be
* constants. Each template parameter is an index to the element you want to select.
* An index of -1 will generate zero. An index of -256 means don't care.
*
* Example:
* Vec8i a(10,11,12,13,14,15,16,17);      // a is (10,11,12,13,14,15,16,17)
* Vec8i b;
* b = permute8i<0,2,7,7,-1,-1,1,1>(a);   // b is (10,12,17,17, 0, 0,11,11)
*
* A lot of the code here is metaprogramming aiming to find the instructions
* that best fit the template parameters and instruction set. The metacode
* will be reduced out to leave only a few vector instructions in release
* mode with optimization on.
*****************************************************************************/

// Permute vector of 16 32-bit integers.
// Index -1 gives 0, index -256 means don't care.
template <int32_t i0, int32_t i1, int32_t i2,  int32_t i3,  int32_t i4,  int32_t i5,  int32_t i6,  int32_t i7,
          int32_t i8, int32_t i9, int32_t i10, int32_t i11, int32_t i12, int32_t i13, int32_t i14, int32_t i15>
static inline Vec16i permute16i(Vec16i const & a) {
    assert(0);
    /* TODO:
    // Combine indexes into a single bitfield, with 4 bits for each
    const int m1 = (i0&7)     | (i1&7)<<4  | (i2&7)<<8  | (i3&7)<<12
                 | (i4&7)<<16 | (i5&7)<<20 | (i6&7)<<24 | (i7&7)<<28;

    // Mask to zero out negative indexes
    const int mz = (i0<0?0:0xF) | (i1<0?0:0xF)<<4 | (i2<0?0:0xF)<<8 | (i3<0?0:0xF)<<12
        | (i4<0?0:0xF)<<16 | (i5<0?0:0xF)<<20 | (i6<0?0:0xF)<<24 | (i7<0?0:0xF)<<28;

    // zeroing needed
    const bool dozero = ((i0|i1|i2|i3|i4|i5|i6|i7) & 0x80) != 0;

    __m256i t1, mask;

    if (((m1 ^ 0x76543210) & mz) == 0) {
        // no shuffling
        if (dozero) {
            // zero some elements
            mask = constant8i <
                i0 < 0 ? 0 : -1, i1 < 0 ? 0 : -1, i2 < 0 ? 0 : -1, i3 < 0 ? 0 : -1, 
                i4 < 0 ? 0 : -1, i5 < 0 ? 0 : -1, i6 < 0 ? 0 : -1, i7 < 0 ? 0 : -1 > ();                    
            return _mm256_and_si256(a, mask);
        }
        return a;                                 // do nothing
    }

    // Check if we can use 64-bit permute. Even numbered indexes must be even and odd numbered
    // indexes must be equal to the preceding index + 1, except for negative indexes.
    if (((m1 ^ 0x10101010) & 0x11111111 & mz) == 0 && ((m1 ^ m1 >> 4) & 0x0E0E0E0E & mz & mz >> 4) == 0) {

        const bool partialzero = int((i0^i1)|(i2^i3)|(i4^i5)|(i6^i7)) < 0; // part of a 64-bit block is zeroed
        const int blank1 = partialzero ? -0x100 : -1;  // ignore or zero
        const int n0 = i0 > 0 ? i0 /2 : i1 > 0 ? i1 /2 : blank1;  // indexes for 64 bit blend
        const int n1 = i2 > 0 ? i2 /2 : i3 > 0 ? i3 /2 : blank1;
        const int n2 = i4 > 0 ? i4 /2 : i5 > 0 ? i5 /2 : blank1;
        const int n3 = i6 > 0 ? i6 /2 : i7 > 0 ? i7 /2 : blank1;
        // do 64-bit permute
        t1 = permute4q<n0,n1,n2,n3> (Vec4q(a));
        if (blank1 == -1 || !dozero) {    
            return  t1;
        }
        // need more zeroing
        mask = constant8i <
            i0 < 0 ? 0 : -1, i1 < 0 ? 0 : -1, i2 < 0 ? 0 : -1, i3 < 0 ? 0 : -1, 
            i4 < 0 ? 0 : -1, i5 < 0 ? 0 : -1, i6 < 0 ? 0 : -1, i7 < 0 ? 0 : -1 > ();                    
        return _mm256_and_si256(t1, mask);
    }

    if (((m1 ^ 0x44440000) & 0x44444444 & mz) == 0) {
        // no exchange of data between low and high half

        if (((m1 ^ (m1 >> 16)) & 0x3333 & mz & (mz >> 16)) == 0 && !dozero) {
            // same pattern in low and high half. use VPSHUFD
            const int sd = ((i0>=0)?(i0&3):(i4&3)) | ((i1>=0)?(i1&3):(i5&3)) << 2 |
                ((i2>=0)?(i2&3):(i6&3)) << 4 | ((i3>=0)?(i3&3):(i7&3)) << 6;
            return _mm256_shuffle_epi32(a, sd);
        }

        // use VPSHUFB
        mask = constant8i <
            i0 < 0 ? -1 : (i0 & 3) * 0x04040404 + 0x03020100,
            i1 < 0 ? -1 : (i1 & 3) * 0x04040404 + 0x03020100,
            i2 < 0 ? -1 : (i2 & 3) * 0x04040404 + 0x03020100,
            i3 < 0 ? -1 : (i3 & 3) * 0x04040404 + 0x03020100,
            i4 < 0 ? -1 : (i4 & 3) * 0x04040404 + 0x03020100,
            i5 < 0 ? -1 : (i5 & 3) * 0x04040404 + 0x03020100,
            i6 < 0 ? -1 : (i6 & 3) * 0x04040404 + 0x03020100,
            i7 < 0 ? -1 : (i7 & 3) * 0x04040404 + 0x03020100 > ();
        return _mm256_shuffle_epi8(a, mask);
    }

    // general case. Use VPERMD
    mask = constant8i <
        i0 < 0 ? -1 : (i0 & 7), i1 < 0 ? -1 : (i1 & 7),
        i2 < 0 ? -1 : (i2 & 7), i3 < 0 ? -1 : (i3 & 7),
        i4 < 0 ? -1 : (i4 & 7), i5 < 0 ? -1 : (i5 & 7),
        i6 < 0 ? -1 : (i6 & 7), i7 < 0 ? -1 : (i7 & 7) > ();
#if defined (_MSC_VER) && _MSC_VER < 1700 && ! defined(__INTEL_COMPILER)
    // bug in MS VS 11 beta: operands in wrong order. fixed in v. 11.0
    t1 = _mm256_permutevar8x32_epi32(mask, a);   // ms
#elif defined (GCC_VERSION) && GCC_VERSION <= 40700 && !defined(__INTEL_COMPILER) && !defined(__clang__)
    // Gcc 4.7.0 also has operands in wrong order. fixed in version 4.7.1
    t1 = _mm256_permutevar8x32_epi32(mask, a);   // GCC
#else
    t1 = _mm256_permutevar8x32_epi32(a, mask);   // no-bug version
#endif

    if (dozero) {
        // zero some elements
        mask = constant8i <
            i0 < 0 ? 0 : -1, i1 < 0 ? 0 : -1, i2 < 0 ? 0 : -1, i3 < 0 ? 0 : -1, 
            i4 < 0 ? 0 : -1, i5 < 0 ? 0 : -1, i6 < 0 ? 0 : -1, i7 < 0 ? 0 : -1 > ();                    
        return _mm256_and_si256(t1, mask);
    }
    return t1; */
}

template <int i0, int i1, int i2,  int i3,  int i4,  int i5,  int i6,  int i7, 
          int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15>
static inline Vec16ui permute16ui(Vec16ui const & a) {
    return Vec16ui (permute16i<i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15>(a));
}

/*****************************************************************************
*
*          Vector blend functions
*
******************************************************************************
*
* These blend functions can mix elements from two different vectors and
* optionally set some elements to zero. 
*
* The indexes are inserted as template parameters in <>. These indexes must be
* constants. Each template parameter is an index to the element you want to 
* select, where higher indexes indicate an element from the second source
* vector. For example, if each vector has 8 elements, then indexes 0 - 7
* will select an element from the first vector and indexes 8 - 15 will select 
* an element from the second vector. A negative index will generate zero.
*
* Example:
* Vec8i a(100,101,102,103,104,105,106,107); // a is (100, 101, 102, 103, 104, 105, 106, 107)
* Vec8i b(200,201,202,203,204,205,206,207); // b is (200, 201, 202, 203, 204, 205, 206, 207)
* Vec8i c;
* c = blend8i<1,0,9,8,7,-1,15,15> (a,b);    // c is (101, 100, 201, 200, 107,   0, 207, 207)
*
* A lot of the code here is metaprogramming aiming to find the instructions
* that best fit the template parameters and instruction set. The metacode
* will be reduced out to leave only a few vector instructions in release
* mode with optimization on.
*****************************************************************************/

template <int i0, int i1, int i2,  int i3,  int i4,  int i5,  int i6,  int i7,
          int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15> 
static inline Vec16i blend16i(Vec16i const & a, Vec16i const & b) {  
    assert(0);
    /* TODO:
    const int ior = i0 | i1 | i2 | i3 | i4 | i5 | i6 | i7;  // OR indexes

    // is zeroing needed
    const bool do_zero  = ior < 0 && (ior & 0x80); // at least one index is negative, and not -0x100

    // Combine all the indexes into a single bitfield, with 4 bits for each
    const int m1 = (i0&0xF) | (i1&0xF)<<4 | (i2&0xF)<<8 | (i3&0xF)<<12 | (i4&0xF)<<16 | (i5&0xF)<<20 | (i6&0xF)<<24 | (i7&0xF)<<28;

    // Mask to zero out negative indexes
    const int mz = (i0<0?0:0xF) | (i1<0?0:0xF)<<4 | (i2<0?0:0xF)<<8 | (i3<0?0:0xF)<<12 | (i4<0?0:0xF)<<16 | (i5<0?0:0xF)<<20 | (i6<0?0:0xF)<<24 | (i7<0?0:0xF)<<28;

    __m256i t1, mask;

    if (mz == 0) return _mm256_setzero_si256();  // all zero

    // special case: 64 bit blend/permute
    if (((m1 ^ 0x10101010) & 0x11111111 & mz) == 0 && ((m1 ^ (m1 >> 4)) & 0x0E0E0E0E & mz & mz >> 4) == 0) {
        // check if part of a 64-bit block is zeroed
        const bool partialzero = int((i0^i1) | (i2^i3) | (i4^i5) | (i6^i7)) < 0; 
        const int blank1 = partialzero ? -0x100 : -1;  // ignore if zeroing later anyway
        // indexes for 64 bit blend
        const int j0 = i0 >= 0 ? i0 / 2 : i1 >= 0 ? i1 / 2 : blank1;
        const int j1 = i2 >= 0 ? i2 / 2 : i3 >= 0 ? i3 / 2 : blank1;
        const int j2 = i4 >= 0 ? i4 / 2 : i5 >= 0 ? i5 / 2 : blank1;
        const int j3 = i6 >= 0 ? i6 / 2 : i7 >= 0 ? i7 / 2 : blank1;
        // 64-bit blend and permute
        t1 = blend4q<j0,j1,j2,j3>(Vec4q(a), Vec4q(b));
        if (partialzero && do_zero) {
            // zero some elements
            mask = constant8i< i0 < 0 ? 0 : -1, i1 < 0 ? 0 : -1, i2 < 0 ? 0 : -1, i3 < 0 ? 0 : -1, 
                i4 < 0 ? 0 : -1, i5 < 0 ? 0 : -1, i6 < 0 ? 0 : -1, i7 < 0 ? 0 : -1 > ();
            return _mm256_and_si256(t1, mask);
        }
        return t1;
    }

    if ((m1 & 0x88888888 & mz) == 0) {
        // all from a
        return permute8i<i0, i1, i2, i3, i4, i5, i6, i7> (a);
    }

    if (((m1 ^ 0x88888888) & 0x88888888 & mz) == 0) {
        // all from b
        return permute8i<i0&~8, i1&~8, i2&~8, i3&~8, i4&~8, i5&~8, i6&~8, i7&~8> (b);
    }

    if ((((m1 & 0x77777777) ^ 0x76543210) & mz) == 0) {
        // blend and zero, no permute
        mask = constant8i<(i0&8)?0:-1, (i1&8)?0:-1, (i2&8)?0:-1, (i3&8)?0:-1, (i4&8)?0:-1, (i5&8)?0:-1, (i6&8)?0:-1, (i7&8)?0:-1> ();
        t1   = select(mask, a, b);
        if (!do_zero) return t1;
        // zero some elements
        mask = constant8i< (i0<0&&(i0&8)) ? 0 : -1, (i1<0&&(i1&8)) ? 0 : -1, (i2<0&&(i2&8)) ? 0 : -1, (i3<0&&(i3&8)) ? 0 : -1, 
            (i4<0&&(i4&8)) ? 0 : -1, (i5<0&&(i5&8)) ? 0 : -1, (i6<0&&(i6&8)) ? 0 : -1, (i7<0&&(i7&8)) ? 0 : -1 > ();
        return _mm256_and_si256(t1, mask);
    }

    // special case: shift left
    if (i0 > 0 && i0 < 8 && mz == -1 && (m1 ^ ((i0 & 7) * 0x11111111u + 0x76543210u)) == 0) {
        t1 = _mm256_permute2x128_si256(a, b, 0x21);
        if (i0 < 4) return _mm256_alignr_epi8(t1, a, (i0 & 3) * 4);
        else        return _mm256_alignr_epi8(b, t1, (i0 & 3) * 4);
    }
    // special case: shift right
    if (i0 > 8 && i0 < 16 && mz == -1 && (m1 ^ 0x88888888 ^ ((i0 & 7) * 0x11111111u + 0x76543210u)) == 0) {
        t1 = _mm256_permute2x128_si256(b, a, 0x21);
        if (i0 < 12) return _mm256_alignr_epi8(t1, b, (i0 & 3) * 4);
        else         return _mm256_alignr_epi8(a, t1, (i0 & 3) * 4);
    }

    // general case: permute and blend and possible zero
    const int blank = do_zero ? -1 : -0x100;  // ignore or zero

    Vec8i ta = permute8i <
        (uint32_t)i0 < 8 ? i0 : blank,
        (uint32_t)i1 < 8 ? i1 : blank,
        (uint32_t)i2 < 8 ? i2 : blank,
        (uint32_t)i3 < 8 ? i3 : blank,
        (uint32_t)i4 < 8 ? i4 : blank,
        (uint32_t)i5 < 8 ? i5 : blank,
        (uint32_t)i6 < 8 ? i6 : blank,
        (uint32_t)i7 < 8 ? i7 : blank > (a);
    Vec8i tb = permute8i <
        (uint32_t)(i0^8) < 8 ? (i0^8) : blank,
        (uint32_t)(i1^8) < 8 ? (i1^8) : blank,
        (uint32_t)(i2^8) < 8 ? (i2^8) : blank,
        (uint32_t)(i3^8) < 8 ? (i3^8) : blank,
        (uint32_t)(i4^8) < 8 ? (i4^8) : blank,
        (uint32_t)(i5^8) < 8 ? (i5^8) : blank,
        (uint32_t)(i6^8) < 8 ? (i6^8) : blank,
        (uint32_t)(i7^8) < 8 ? (i7^8) : blank > (b);
    if (blank == -1) {    
        return  _mm256_or_si256(ta, tb); 
    }
    // no zeroing, need to blend
    const int maskb = ((i0 >> 3) & 1) | ((i1 >> 2) & 2) | ((i2 >> 1) & 4) | (i3 & 8) | 
        ((i4 << 1) & 0x10) | ((i5 << 2) & 0x20) | ((i6 << 3) & 0x40) | ((i7 << 4) & 0x80);
    return _mm256_blend_epi32(ta, tb, maskb);  // blend */
}

template <int i0, int i1, int i2,  int i3,  int i4,  int i5,  int i6,  int i7,
          int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15> 
static inline Vec16ui blend16ui(Vec16ui const & a, Vec16ui const & b) {
    return Vec16ui( blend16i<i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15> (a,b));
}

/*****************************************************************************
*
*          Vector lookup functions
*
******************************************************************************
*
* These functions use vector elements as indexes into a table.
* The table is given as one or more vectors or as an array.
*
* This can be used for several purposes:
*  - table lookup
*  - permute or blend with variable indexes
*  - blend from more than two sources
*  - gather non-contiguous data
*
* An index out of range may produce any value - the actual value produced is
* implementation dependent and may be different for different instruction
* sets. An index out of range does not produce an error message or exception.
*
* Example:
* Vec8i a(2,0,0,6,4,3,5,0);                 // index a is (  2,   0,   0,   6,   4,   3,   5,   0)
* Vec8i b(100,101,102,103,104,105,106,107); // table b is (100, 101, 102, 103, 104, 105, 106, 107)
* Vec8i c;
* c = lookup8 (a,b);                        // c is       (102, 100, 100, 106, 104, 103, 105, 100)
*
*****************************************************************************/

static inline Vec16i lookup16(Vec16i const & index, Vec16i const & table) {
    assert(0);
    // TODO :
    /*Vec32c index1 = Vec32c(index * 0x04040404 + 0x03020100);
    Vec32c f0 = constant8i<0,0,0,0,0x10101010,0x10101010,0x10101010,0x10101010>();
    Vec32c f1 = constant8i<0x10101010,0x10101010,0x10101010,0x10101010,0,0,0,0>();
    Vec32c tablef = _mm256_permute4x64_epi64(table, 0x4E);   // low and high parts swapped
    Vec32c r0 = _mm256_shuffle_epi8(table,  (index1 ^ f0) + 0x70);
    Vec32c r1 = _mm256_shuffle_epi8(tablef, (index1 ^ f1) + 0x70);
    return Vec8i(r0 | r1);*/
}

template <int n>
static inline Vec16i lookup(Vec16i const & index, void const * table) {
    assert(0);
    /* TODO:
    if (n <= 0) return 0;
    if (n <= 4) {
        Vec4i table1 = Vec4i().load(table);        
        return Vec8i(       
            lookup4 (index.get_low(),  table1),
            lookup4 (index.get_high(), table1));
    }
    // n > 4. Limit index
    Vec8ui index1;
    if ((n & (n-1)) == 0) {
        // n is a power of 2, make index modulo n
        index1 = Vec8ui(index) & (n-1);
    }
    else {
        // n is not a power of 2, limit to n-1
        index1 = min(Vec8ui(index), n-1);
    }
    return _mm256_i32gather_epi32((const int *)table, index1, 4); */
}

/*****************************************************************************
*
*          Gather functions with fixed indexes
*
*****************************************************************************/
// Load elements from array a with indices i0, i1, i2, i3, i4, i5, i6, i7
template <int i0, int i1, int i2,  int i3,  int i4,  int i5,  int i6,  int i7, 
          int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15>
static inline Vec16i gather16i(void const * a) {
    assert(0);
    /* TODO:
    Static_error_check<(i0|i1|i2|i3|i4|i5|i6|i7)>=0> Negative_array_index;  // Error message if index is negative
    const int i01min = i0 < i1 ? i0 : i1;
    const int i23min = i2 < i3 ? i2 : i3;
    const int i45min = i4 < i5 ? i4 : i5;
    const int i67min = i6 < i7 ? i6 : i7;
    const int i0123min = i01min < i23min ? i01min : i23min;
    const int i4567min = i45min < i67min ? i45min : i67min;
    const int imin = i0123min < i4567min ? i0123min : i4567min;
    const int i01max = i0 > i1 ? i0 : i1;
    const int i23max = i2 > i3 ? i2 : i3;
    const int i45max = i4 > i5 ? i4 : i5;
    const int i67max = i6 > i7 ? i6 : i7;
    const int i0123max = i01max > i23max ? i01max : i23max;
    const int i4567max = i45max > i67max ? i45max : i67max;
    const int imax = i0123max > i4567max ? i0123max : i4567max;

    if (imax - imin <= 7) {
        // load one contiguous block and permute
        if (imax > 7) {
            // make sure we don't read past the end of the array
            Vec8i b = Vec8i().load((int32_t const *)a + imax-7);
            return permute8i<i0-imax+7, i1-imax+7, i2-imax+7, i3-imax+7, i4-imax+7, i5-imax+7, i6-imax+7, i7-imax+7>(b);
        }
        else {
            Vec8i b = Vec8i().load((int32_t const *)a + imin);
            return permute8i<i0-imin, i1-imin, i2-imin, i3-imin, i4-imin, i5-imin, i6-imin, i7-imin>(b);
        }
    }
    if ((i0<imin+8 || i0>imax-8) && (i1<imin+8 || i1>imax-8) && (i2<imin+8 || i2>imax-8) && (i3<imin+8 || i3>imax-8)
    &&  (i4<imin+8 || i4>imax-8) && (i5<imin+8 || i5>imax-8) && (i6<imin+8 || i6>imax-8) && (i7<imin+8 || i7>imax-8)) {
        // load two contiguous blocks and blend
        Vec8i b = Vec8i().load((int32_t const *)a + imin);
        Vec8i c = Vec8i().load((int32_t const *)a + imax-7);
        const int j0 = i0<imin+8 ? i0-imin : 15-imax+i0;
        const int j1 = i1<imin+8 ? i1-imin : 15-imax+i1;
        const int j2 = i2<imin+8 ? i2-imin : 15-imax+i2;
        const int j3 = i3<imin+8 ? i3-imin : 15-imax+i3;
        const int j4 = i4<imin+8 ? i4-imin : 15-imax+i4;
        const int j5 = i5<imin+8 ? i5-imin : 15-imax+i5;
        const int j6 = i6<imin+8 ? i6-imin : 15-imax+i6;
        const int j7 = i7<imin+8 ? i7-imin : 15-imax+i7;
        return blend8i<j0, j1, j2, j3, j4, j5, j6, j7>(b, c);
    }
    // use AVX2 gather
    return _mm256_i32gather_epi32((const int *)a, Vec8i(i0,i1,i2,i3,i4,i5,i6,i7), 4);*/
}

/*****************************************************************************
*
*          Functions for conversion between integer sizes
*
*****************************************************************************/

// Extend 32-bit integers to 64-bit integers, signed and unsigned
// Function extend_low : extends the low 8 elements to 64 bits with sign extension
static inline Vec8q extend_low (Vec16i const & a) {
    // NOTE: no pack/unpack instructions for MIC
    Vec16i t0 = _mm512_mask_permutevar_epi32(constant16i<0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0>(), 
                                            0xFFFF,  
                                            constant16i<0,8,1,9,2,10,3,11, 4,12,5,13,6,14,7,15>(), // pack values into odd indices
                                            a);
    Vec16i t1 = _mm512_and_epi32(constant16i<0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,
                                             0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF>(),
                                 t0); // absolute value
    Vec16i t2 = _mm512_and_epi32(constant16i<0x80000000,0x80000000,0x80000000,0x80000000,0x80000000,0x80000000,0x80000000,0x80000000,
                                             0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000>(),
                                 t0); // sign
    Vec16i t3 = _mm512_mask_permutevar_epi32(constant16i<0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0>(),        // pack sign into even indices
                                            0xFFFF,
                                            constant16i<8,0,9,1,10,2,11,3, 12,4,13,5,14,6,15,7>(),
                                            t2);
    return Vec8q(_mm512_or_epi32(t3, t1));
}

// Function extend_high : extends the high 8 elements to 32 bits with sign extension
static inline Vec8q extend_high (Vec16i const & a) {
    // NOTE: no pack/unpack instructions for MIC
    Vec16i t0 = _mm512_mask_permutevar_epi32(constant16i<0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0>(), 
                                            0xFFFF,  
                                            constant16i<8,0,9,1,10,2,11,3, 12,4,13,5,14,6,15,7>(), // pack values into odd indices
                                            a);
    Vec16i t1 = _mm512_and_epi32(constant16i<0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,
                                             0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF,0x7FFFFFFF>(),
                                 t0); // absolute value
    Vec16i t2 = _mm512_and_epi32(constant16i<0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,
                                             0x80000000,0x80000000,0x80000000,0x80000000,0x80000000,0x80000000,0x80000000,0x80000000>(),
                                 t0); // sign
    Vec16i t3 = _mm512_mask_permutevar_epi32(constant16i<0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0>(),        // pack sign into even indices
                                            0xFFFF,
                                            constant16i<0,8,1,9,2,10,3,11, 4,12,5,13,6,14,7,15>(),
                                            t2);
    return Vec8q(_mm512_or_epi32(t3, t1));
}

// Function extend_low : extends the low 8 elements to 32 bits with zero extension
static inline Vec8uq extend_low (Vec16ui const & a) {
    assert(0);
    /*
    __m256i a2 = permute4q<0,-256,1,-256>(Vec4q(a));             // get bits 64-127 to position 128-191
    return    _mm256_unpacklo_epi16(a2, _mm256_setzero_si256()); // interleave with zero extensions*/
}

// Function extend_high : extends the high 8 elements to 32 bits with zero extension
static inline Vec8uq extend_high (Vec16ui const & a) {
    assert(0);
    /*
    __m256i a2 = permute4q<-256,2,-256,3>(Vec4q(a));             // get bits 128-191 to position 64-127
    return  _mm256_unpackhi_epi16(a2, _mm256_setzero_si256());   // interleave with zero extensions */
}

// Compress 64-bit integers to 32-bit integers, signed and unsigned, with and without saturation

// Function compress : packs two vectors of 64-bit integers into one vector of 32-bit integers
// Overflow wraps around
static inline Vec16i compress (Vec8q const & low, Vec8q const & high) {
    assert(0);
    /*
    __m256i low2  = _mm256_shuffle_epi32(low,0xD8);           // low dwords of low  to pos. 0 and 32
    __m256i high2 = _mm256_shuffle_epi32(high,0xD8);          // low dwords of high to pos. 0 and 32
    __m256i pk    = _mm256_unpacklo_epi64(low2,high2);        // interleave
    return          _mm256_permute4x64_epi64(pk, 0xD8);       // put in right place */
}

// Function compress : packs two vectors of 64-bit integers into one vector of 32-bit integers
// Signed, with saturation
static inline Vec16i compress_saturated (Vec8q const & low, Vec8q const & high) {
    assert(0);
    /*
    Vec4q maxval = constant8i<0,0x7FFFFFFF,0,0x7FFFFFFF,0,0x7FFFFFFF,0,0x7FFFFFFF>();
    Vec4q minval = constant8i<80000000,0xFFFFFFFF,80000000,0xFFFFFFFF,80000000,0xFFFFFFFF,80000000,0xFFFFFFFF>();
    Vec4q low1   = min(low,maxval);
    Vec4q high1  = min(high,maxval);
    Vec4q low2   = max(low1,minval);
    Vec4q high2  = max(high1,minval);
    return compress(low2,high2); */
}

// Function compress : packs two vectors of 32-bit integers into one vector of 16-bit integers
// Overflow wraps around
static inline Vec16ui compress (Vec8uq const & low, Vec8uq const & high) {
    assert(0);
    /*
    return Vec8ui (compress((Vec4q)low, (Vec4q)high)); */
}

// Function compress : packs two vectors of 64-bit integers into one vector of 32-bit integers
// Unsigned, with saturation
static inline Vec16ui compress_saturated (Vec8uq const & low, Vec8uq const & high) {
    assert(0);
    /*
    __m256i zero     = _mm256_setzero_si256();                // 0
    __m256i lowzero  = _mm256_cmpeq_epi32(low,zero);          // for each dword is zero
    __m256i highzero = _mm256_cmpeq_epi32(high,zero);         // for each dword is zero
    __m256i mone     = _mm256_set1_epi32(-1);                 // FFFFFFFF
    __m256i lownz    = _mm256_xor_si256(lowzero,mone);        // for each dword is nonzero
    __m256i highnz   = _mm256_xor_si256(highzero,mone);       // for each dword is nonzero
    __m256i lownz2   = _mm256_srli_epi64(lownz,32);           // shift down to low dword
    __m256i highnz2  = _mm256_srli_epi64(highnz,32);          // shift down to low dword
    __m256i lowsatur = _mm256_or_si256(low,lownz2);           // low, saturated
    __m256i hisatur  = _mm256_or_si256(high,highnz2);         // high, saturated
    return  Vec8ui (compress(Vec4q(lowsatur), Vec4q(hisatur))); */
}

/*****************************************************************************
*
*          Integer division operators
*
*          Please see the file vectori128.h for explanation.
*
*****************************************************************************/

/*****************************************************************************
*
*          Integer division operators
*
******************************************************************************
*
* The instruction set does not support integer vector division. Instead, we
* are using a method for fast integer division based on multiplication and
* shift operations. This method is faster than simple integer division if the
* same divisor is used multiple times.
*
* All elements in a vector are divided by the same divisor. It is not possible
* to divide different elements of the same vector by different divisors.
*
* The parameters used for fast division are stored in an object of a 
* Divisor class. This object can be created implicitly, for example in:
*        Vec4i a, b; int c;
*        a = b / c;
* or explicitly as:
*        a = b / Divisor_i(c);
*
* It takes more time to compute the parameters used for fast division than to
* do the division. Therefore, it is advantageous to use the same divisor object
* multiple times. For example, to divide 80 unsigned short integers by 10:
*
*        uint16_t dividends[80], quotients[80];         // numbers to work with
*        Divisor_us div10(10);                          // make divisor object for dividing by 10
*        Vec8us temp;                                   // temporary vector
*        for (int i = 0; i < 80; i += 8) {              // loop for 4 elements per iteration
*            temp.load(dividends+i);                    // load 4 elements
*            temp /= div10;                             // divide each element by 10
*            temp.store(quotients+i);                   // store 4 elements
*        }
* 
* The parameters for fast division can also be computed at compile time. This is
* an advantage if the divisor is known at compile time. Use the const_int or const_uint
* macro to do this. For example, for signed integers:
*        Vec8s a, b;
*        a = b / const_int(10);
* Or, for unsigned integers:
*        Vec8us a, b;
*        a = b / const_uint(10);
*
* The division of a vector of 16-bit integers is faster than division of a vector 
* of other integer sizes.
*
* 
* Mathematical formula, used for signed division with fixed or variable divisor:
* (From T. Granlund and P. L. Montgomery: Division by Invariant Integers Using Multiplication,
* Proceedings of the SIGPLAN 1994 Conference on Programming Language Design and Implementation.
* http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.1.2556 )
* x = dividend
* d = abs(divisor)
* w = integer word size, bits
* L = ceil(log2(d)) = bit_scan_reverse(d-1)+1
* L = max(L,1)
* m = 1 + 2^(w+L-1)/d - 2^w                      [division should overflow to 0 if d = 1]
* sh1 = L-1
* q = x + (m*x >> w)                             [high part of signed multiplication with 2w bits]
* q = (q >> sh1) - (x<0 ? -1 : 0)
* if (divisor < 0) q = -q 
* result trunc(x/d) = q
*
* Mathematical formula, used for unsigned division with variable divisor:
* (Also from T. Granlund and P. L. Montgomery)
* x = dividend
* d = divisor
* w = integer word size, bits
* L = ceil(log2(d)) = bit_scan_reverse(d-1)+1
* m = 1 + 2^w * (2^L-d) / d                      [2^L should overflow to 0 if L = w]
* sh1 = min(L,1)
* sh2 = max(L-1,0)
* t = m*x >> w                                   [high part of unsigned multiplication with 2w bits]
* result floor(x/d) = (((x-t) >> sh1) + t) >> sh2
*
* Mathematical formula, used for unsigned division with fixed divisor:
* (From Terje Mathisen, unpublished)
* x = dividend
* d = divisor
* w = integer word size, bits
* b = floor(log2(d)) = bit_scan_reverse(d)
* f = 2^(w+b) / d                                [exact division]
* If f is an integer then d is a power of 2 then go to case A
* If the fractional part of f is < 0.5 then go to case B
* If the fractional part of f is > 0.5 then go to case C
* Case A:  [shift only]
* result = x >> b
* Case B:  [round down f and compensate by adding one to x]
* result = ((x+1)*floor(f)) >> (w+b)             [high part of unsigned multiplication with 2w bits]
* Case C:  [round up f, no compensation for rounding error]
* result = (x*ceil(f)) >> (w+b)                  [high part of unsigned multiplication with 2w bits]
*
*
*****************************************************************************/

// encapsulate parameters for fast division on vector of 4 32-bit signed integers
class Divisor_i {
protected:
    __m512i multiplier;                                    // multiplier used in fast division
    __m512i shift1;                                        // shift count used in fast division
    __m512i sign;                                          // sign of divisor
public:
    Divisor_i() {};                                        // Default constructor
    Divisor_i(int32_t d) {                                 // Constructor with divisor
        set(d);
    }
    Divisor_i(int m, int s1, int sgn) {                    // Constructor with precalculated multiplier, shift and sign
    assert(0);
      /* TODO:
        multiplier = _mm_set1_epi32(m);
        shift1     = _mm_cvtsi32_si128(s1);
        sign       = _mm_set1_epi32(sgn); */
    }
    void set(int32_t d) {                                  // Set or change divisor, calculate parameters
    assert(0);
        /* TODO:
        const int32_t d1 = abs(d);
        int32_t sh, m;
        if (d1 > 1) {
            sh = bit_scan_reverse(d1-1);                   // shift count = ceil(log2(d1))-1 = (bit_scan_reverse(d1-1)+1)-1
            m = int32_t((int64_t(1) << (32+sh)) / d1 - ((int64_t(1) << 32) - 1)); // calculate multiplier
        }
        else {
            m  = 1;                                        // for d1 = 1
            sh = 0;
            if (d == 0) m /= d;                            // provoke error here if d = 0
            if (uint32_t(d) == 0x80000000u) {              // fix overflow for this special case
                m  = 0x80000001;
                sh = 30;
            }
        }
        multiplier = _mm_set1_epi32(m);                    // broadcast multiplier
        shift1     = _mm_setr_epi32(sh, 0, 0, 0);          // shift count
        sign       = _mm_set1_epi32(d < 0 ? -1 : 0);       // sign of divisor*/
    }
    __m512i getm() const {                                 // get multiplier
        return multiplier;
    }
    __m512i gets1() const {                                // get shift count
        return shift1;
    }
    __m512i getsign() const {                              // get sign of divisor
        return sign;
    }
};

// encapsulate parameters for fast division on vector of 4 32-bit unsigned integers
class Divisor_ui {
protected:
    __m512i multiplier;                                    // multiplier used in fast division
    __m512i shift1;                                        // shift count 1 used in fast division
    __m512i shift2;                                        // shift count 2 used in fast division
public:
    Divisor_ui() {};                                       // Default constructor
    Divisor_ui(uint32_t d) {                               // Constructor with divisor
        set(d);
    }
    Divisor_ui(uint32_t m, int s1, int s2) {               // Constructor with precalculated multiplier and shifts
    assert(0);
        /* TODO:
        multiplier = _mm_set1_epi32(m);
        shift1     = _mm_setr_epi32(s1, 0, 0, 0);
        shift2     = _mm_setr_epi32(s2, 0, 0, 0); */
    }
    void set(uint32_t d) {                                 // Set or change divisor, calculate parameters
    assert(0);
       /* TODO:
        uint32_t L, L2, sh1, sh2, m;
        switch (d) {
        case 0:
            m = sh1 = sh2 = 1 / d;                         // provoke error for d = 0
            break;
        case 1:
            m = 1; sh1 = sh2 = 0;                          // parameters for d = 1
            break;
        case 2:
            m = 1; sh1 = 1; sh2 = 0;                       // parameters for d = 2
            break;
        default:                                           // general case for d > 2
            L  = bit_scan_reverse(d-1)+1;                  // ceil(log2(d))
            L2 = L < 32 ? 1 << L : 0;                      // 2^L, overflow to 0 if L = 32
            m  = 1 + uint32_t((uint64_t(L2 - d) << 32) / d); // multiplier
            sh1 = 1;  sh2 = L - 1;                         // shift counts
        }
        multiplier = _mm_set1_epi32(m);
        shift1     = _mm_setr_epi32(sh1, 0, 0, 0);
        shift2     = _mm_setr_epi32(sh2, 0, 0, 0); */
    }
    __m512i getm() const {                                 // get multiplier
        return multiplier;
    }
    __m512i gets1() const {                                // get shift count 1
        return shift1;
    }
    __m512i gets2() const {                                // get shift count 2
        return shift2;
    }
};

// vector operator / : divide each element by divisor

// vector of 16 32-bit signed integers
static inline Vec16i operator / (Vec16i const & a, Divisor_i const & d) {
    assert(0);
    /* TODO:
    __m256i m   = _mm256_broadcastq_epi64(d.getm());       // broadcast multiplier
    __m256i sgn = _mm256_broadcastq_epi64(d.getsign());    // broadcast sign of d
    __m256i t1  = _mm256_mul_epi32(a,m);                   // 32x32->64 bit signed multiplication of even elements of a
    __m256i t2  = _mm256_srli_epi64(t1,32);                // high dword of even numbered results
    __m256i t3  = _mm256_srli_epi64(a,32);                 // get odd elements of a into position for multiplication
    __m256i t4  = _mm256_mul_epi32(t3,m);                  // 32x32->64 bit signed multiplication of odd elements
    __m256i t5  = constant8i<0,-1,0,-1,0,-1,0,-1> ();      // mask for odd elements
    __m256i t7  = _mm256_blendv_epi8(t2,t4,t5);            // blend two results
    __m256i t8  = _mm256_add_epi32(t7,a);                  // add
    __m256i t9  = _mm256_sra_epi32(t8,d.gets1());          // shift right artihmetic
    __m256i t10 = _mm256_srai_epi32(a,31);                 // sign of a
    __m256i t11 = _mm256_sub_epi32(t10,sgn);               // sign of a - sign of d
    __m256i t12 = _mm256_sub_epi32(t9,t11);                // + 1 if a < 0, -1 if d < 0
    return        _mm256_xor_si256(t12,sgn);               // change sign if divisor negative*/
}

// vector of 16 32-bit unsigned integers
static inline Vec16ui operator / (Vec16ui const & a, Divisor_ui const & d) {
    assert(0);
   /* TODO: 
    __m256i m   = _mm256_broadcastq_epi64(d.getm());       // broadcast multiplier
    __m256i t1  = _mm256_mul_epu32(a,m);                   // 32x32->64 bit unsigned multiplication of even elements of a
    __m256i t2  = _mm256_srli_epi64(t1,32);                // high dword of even numbered results
    __m256i t3  = _mm256_srli_epi64(a,32);                 // get odd elements of a into position for multiplication
    __m256i t4  = _mm256_mul_epu32(t3,m);                  // 32x32->64 bit unsigned multiplication of odd elements
    __m256i t5  = constant8i<0,-1,0,-1,0,-1,0,-1> ();      // mask for odd elements
    __m256i t7  = _mm256_blendv_epi8(t2,t4,t5);            // blend two results
    __m256i t8  = _mm256_sub_epi32(a,t7);                  // subtract
    __m256i t9  = _mm256_srl_epi32(t8,d.gets1());          // shift right logical
    __m256i t10 = _mm256_add_epi32(t7,t9);                 // add
    return        _mm256_srl_epi32(t10,d.gets2());         // shift right logical */
}

// vector operator /= : divide
static inline Vec16i & operator /= (Vec16i & a, Divisor_i const & d) {
    a = a / d;
    return a;
}

// vector operator /= : divide
static inline Vec16ui & operator /= (Vec16ui & a, Divisor_ui const & d) {
    a = a / d;
    return a;
}

/*****************************************************************************
*
*          Integer division 2: divisor is a compile-time constant
*
*****************************************************************************/

// Divide Vec16i by compile-time constant
template <int32_t d>
static inline Vec16i divide_by_i(Vec16i const & x) {
    assert(0);
    /* TODO: 
    Static_error_check<(d!=0)> Dividing_by_zero;                     // Error message if dividing by zero
    if (d ==  1) return  x;
    if (d == -1) return -x;
    if (uint32_t(d) == 0x80000000u) return (x == Vec8i(0x80000000)) & 1; // prevent overflow when changing sign
    const uint32_t d1 = d > 0 ? uint32_t(d) : -uint32_t(d);          // compile-time abs(d). (force GCC compiler to treat d as 32 bits, not 64 bits)
    if ((d1 & (d1-1)) == 0) {
        // d1 is a power of 2. use shift
        const int k = bit_scan_reverse_const(d1);
        __m256i sign;
        if (k > 1) sign = _mm256_srai_epi32(x, k-1); else sign = x;  // k copies of sign bit
        __m256i bias    = _mm256_srli_epi32(sign, 32-k);             // bias = x >= 0 ? 0 : k-1
        __m256i xpbias  = _mm256_add_epi32 (x, bias);                // x + bias
        __m256i q       = _mm256_srai_epi32(xpbias, k);              // (x + bias) >> k
        if (d > 0)      return q;                                    // d > 0: return  q
        return _mm256_sub_epi32(_mm256_setzero_si256(), q);          // d < 0: return -q
    }
    // general case
    const int32_t sh = bit_scan_reverse_const(uint32_t(d1)-1);       // ceil(log2(d1)) - 1. (d1 < 2 handled by power of 2 case)
    const int32_t mult = int(1 + (uint64_t(1) << (32+sh)) / uint32_t(d1) - (int64_t(1) << 32));   // multiplier
    const Divisor_i div(mult, sh, d < 0 ? -1 : 0);
    return x / div; */
}

// define Vec16i a / const_int(d)
template <int32_t d>
static inline Vec16i operator / (Vec16i const & a, Const_int_t<d>) {
    return divide_by_i<d>(a);
}

// define Vec16i a / const_uint(d)
template <uint32_t d>
static inline Vec16i operator / (Vec16i const & a, Const_uint_t<d>) {
    Static_error_check< (d<0x80000000u) > Error_overflow_dividing_signed_by_unsigned; // Error: dividing signed by overflowing unsigned
    return divide_by_i<int32_t(d)>(a);                               // signed divide
}

// vector operator /= : divide
template <int32_t d>
static inline Vec16i & operator /= (Vec16i & a, Const_int_t<d> b) {
    a = a / b;
    return a;
}

// vector operator /= : divide
template <uint32_t d>
static inline Vec16i & operator /= (Vec16i & a, Const_uint_t<d> b) {
    a = a / b;
    return a;
}


// Divide Vec16ui by compile-time constant
template <uint32_t d>
static inline Vec16ui divide_by_ui(Vec16ui const & x) {
    assert(0);
    /*TODO: Static_error_check<(d!=0)> Dividing_by_zero;                     // Error message if dividing by zero
    if (d == 1) return x;                                            // divide by 1
    const int b = bit_scan_reverse_const(d);                         // floor(log2(d))
    if ((uint32_t(d) & (uint32_t(d)-1)) == 0) {
        // d is a power of 2. use shift
        return  _mm256_srli_epi32(x, b);                             // x >> b
    }
    // general case (d > 2)
    uint32_t mult = uint32_t((uint64_t(1) << (b+32)) / d);           // multiplier = 2^(32+b) / d
    const uint64_t rem = (uint64_t(1) << (b+32)) - uint64_t(d)*mult; // remainder 2^(32+b) % d
    const bool round_down = (2*rem < d);                             // check if fraction is less than 0.5
    if (!round_down) {
        mult = mult + 1;                                             // round up mult
    }
    // do 32*32->64 bit unsigned multiplication and get high part of result
    const __m256i multv = _mm256_set_epi32(0,mult,0,mult,0,mult,0,mult);// zero-extend mult and broadcast
    __m256i t1 = _mm256_mul_epu32(x,multv);                          // 32x32->64 bit unsigned multiplication of x[0] and x[2]
    if (round_down) {
        t1      = _mm256_add_epi64(t1,multv);                        // compensate for rounding error. (x+1)*m replaced by x*m+m to avoid overflow
    }
    __m256i t2 = _mm256_srli_epi64(t1,32);                           // high dword of result 0 and 2
    __m256i t3 = _mm256_srli_epi64(x,32);                            // get x[1] and x[3] into position for multiplication
    __m256i t4 = _mm256_mul_epu32(t3,multv);                         // 32x32->64 bit unsigned multiplication of x[1] and x[3]
    if (round_down) {
        t4      = _mm256_add_epi64(t4,multv);                        // compensate for rounding error. (x+1)*m replaced by x*m+m to avoid overflow
    }
    __m256i t5 = _mm256_set_epi32(-1,0,-1,0,-1,0,-1,0);              // mask of dword 1 and 3
    __m256i t7 = _mm256_blendv_epi8(t2,t4,t5);                       // blend two results
    Vec8ui  q  = _mm256_srli_epi32(t7, b);                           // shift right by b
    return q;                                                        // no overflow possible
    */
}

// define Vec16ui a / const_uint(d)
template <uint32_t d>
static inline Vec16ui operator / (Vec16ui const & a, Const_uint_t<d>) {
    return divide_by_ui<d>(a);
}

// define Vec16ui a / const_int(d)
template <int32_t d>
static inline Vec16ui operator / (Vec16ui const & a, Const_int_t<d>) {
    Static_error_check< (d>=0) > Error_dividing_unsigned_by_negative;// Error: dividing unsigned by negative is ambiguous
    return divide_by_ui<d>(a);                                       // unsigned divide
}

// vector operator /= : divide
template <uint32_t d>
static inline Vec16ui & operator /= (Vec16ui & a, Const_uint_t<d> b) {
    a = a / b;
    return a;
}

// vector operator /= : divide
template <int32_t d>
static inline Vec16ui & operator /= (Vec16ui & a, Const_int_t<d> b) {
    a = a / b;
    return a;
}

/*****************************************************************************
*
*          Horizontal scan functions
*
*****************************************************************************/

static inline int horizontal_find_first(Vec16b const & x) {
    assert(0);
    // TODO: return horizontal_find_first(Vec32cb(x)) >> 2;
}

static inline uint32_t horizontal_count(Vec16b const & x) {
    assert(0);
   //TODO return horizontal_count(Vec32cb(x)) >> 2;
}

#endif // VECTORI128_H
