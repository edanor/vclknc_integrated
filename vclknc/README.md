## Description ##
VCLKNC is a version of Agner Fog's Vector Class Library (VCL) for Intel Knight's Corner coprocessor. The original library is available at: [VCL](http://www.agner.org/optimize/#vectorclass).
 
This extension does not provide the full functionality of original VCL library. This is only implementation for Xeon Phi KNC implementing Initial Many Core Instructions (IMCI) extension. Implementation of templated functions varies a little from the original library but should still have the same functionality.

## Authorship ##
This piece of code has been developed by Przemysław Karpiński (CERN) during the course of Intel-CERN European Doctorate Industrial Program (ICE-DIP).

*This research project has been supported by a Marie Curie European Industrial Doctorates Fellowship of the European Community’s Seventh Framework Programme [contract number PITN-GA-2012-316596-ICE-DIP].
*

## License ##
The code is available under GPL license. 

Agner Fog offers proprietary license as an alternative for the original library GPL license. Since this is a derived, public domain licensed piece of code, any proprietary, non-public use and derived work should be consulted with both Mr. Fog and CERN.
